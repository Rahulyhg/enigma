program fpcunitproject;

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, FormattedPositions, GraphicsHelpers, GraphicsHelpersImpl,
  ChartMetricsImpl, ChartsHandlers, DbUpdaterImpl, SharedXchgDomain,
  SharedEndpoints, XChartsDomain, XChartsDomainImpl, UiDomain,
  UiChartsDataVaultImpl, GuiTestRunner, TestCaseDomainModel, TestCaseSeFrontend,
  TestCaseEndpoints, TestCaseXchgDomain, testcasexchartsdomain,
  FakesXChartsDomain, TestCaseUiDomain, TestCaseSharedXchgDomain,
  TestCaseSharedEndpoints, TestCaseChartsDomain, TestCaseSharedHandlers,
  TestCaseBeChartsHandlers, TestCaseChartsEndpoints, FakesChartsDomain,
  FakesXchgDomain, FakesSharedXchgDomain, TestCaseDbUpdater, TestCaseDto,
  FakesDto, DbTestUtils, TestCaseDao, FakesDao, TestCaseDatabaseHandlers,
  FakesDatabaseHandlers, TestCaseDataEndpoints, FakesDataEndpoints,
  TestCaseChartMetrics, FakesChartMetrics, TestCaseGraphicsHelpers,
  TestCaseUiUtil, TestCaseFormattedPositions, TestCaseUiChartsDataVault;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

