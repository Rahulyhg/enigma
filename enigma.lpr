{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
program enigma;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, lazcontrols, runtimetypeinfocontrols, UiSharedDashboard, UiChartsInput,
  UiChartsMain, SelectHouseSystem, SelectAyanamsha, SelectLov, UiChartsDrawing;


{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TFormDashboard, FormDashboard);
  Application.CreateForm(TFormChartsMain, FormChartsMain);
  Application.CreateForm(TFormChartsInput, FormChartsInput);
  Application.CreateForm(TFormSelectHouseSystem, FormSelectHouseSystem);
  Application.CreateForm(TFormSelectAyanamsha, FormSelectAyanamsha);
  Application.CreateForm(TFormSelectLov, FormSelectLov);
  Application.CreateForm(TFormChartsDrawing, FormChartsDrawing);
  Application.Run;
end.

