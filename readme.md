# Enigma - software for research into astrology

## Current version 0.4

Enigma is written in Free Pascal to explore and test the possibilities of astrology.
The current version 0.4 is still in a preliminary state, it only performs calculations of planetary positions and handles database access.

Please note that you need to install both a dll and datafiles from the Swiss Ephemeris which are not in my GitLab project.

To create the database you need to run the test project FPCUnitProject.

You can find information about this in the technical documentation at http://radixpro.org/documentation/

For more information check the site http://radixpro.org
