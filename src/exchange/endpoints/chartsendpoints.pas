{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit ChartsEndpoints;
{< Endpoints for requests that are specific for Charts. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeChartsHandlers, XChartsDomain, SharedXchgDomain;

type

{ Endpoint for the calculation of a full chart. }
TFullChartEndpoint = class
  strict private
    Request: IFullChartRequest;
    Handler: IFullChartHandler;
  public
    constructor Create;
    destructor Destroy; override;
    function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
end;

implementation

uses BeChartsHandlersImpl;

{ TFullChartEndpoint}
constructor TFullChartEndpoint.Create;
begin
  Handler := TFullChartHandler.Create;
end;

destructor TFullChartEndpoint.Destroy;
begin
  FreeAndNil(Handler);
  inherited;
end;

{ Endpoint for the calculation of a full chart. }
function TFullChartEndpoint.HandleRequest(PFullChartRequest: IFullChartRequest) : IFullChartResponse;
begin
  Result:= Handler.HandleRequest(PFullChartRequest);
end;


end.

