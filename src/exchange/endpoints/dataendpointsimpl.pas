{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit DataEndpointsImpl;
{< Implementations for DataEndpoints }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DataEndpoints, Dao, Dto;

type

  THouseSystemDataEndpoint = class(TInterfacedObject, IHouseSystemDataEndpoint)
    strict private
      Dao: IHouseSystemsDao;
    public
      Constructor Create(PDao: IHouseSystemsDao);
      function GetSystem(PId: Integer): IHouseSystemDto;
      function GetAllSystems: THouseSystemDtoArray;
  end;

  TAyanamshaDataEndpoint = class(TInterfacedObject, IAyanamshaDataEndpoint)
    strict private
      Dao: IAyanamshasDao;
    public
      Constructor Create(PDao: IAyanamshasDao);
      function GetAyanamsha(PId: Integer): IAyanamshaDto;
      function GetAllAyanamshas: TAyanamshaDtoArray;
  end;

  TCoordinateSystemDataEndpoint = class(TInterfacedObject,ICoordinateSystemDataEndpoint)
    strict private
      Dao: ILookUpValueDao;
    public
      constructor Create(PDao: ILookUpValueDao);
      function GetName(PId: Integer): ILookUpValueDto;
      function GetAllItems():TLookUpValueDtoArray;
      property AllItems: TLookUpValueDtoArray read GetAllItems;
    end;

  TLookupValueDataEndpoint = class(TInterfacedObject,ILookupValueDataEndpoint)
    strict private
      Dao: ILookUpValueDao;
    public
      constructor Create(PDao: ILookUpValueDao);
      function GetName(PId: Integer): ILookUpValueDto;
      function GetAllItems():TLookUpValueDtoArray;
      property AllItems: TLookUpValueDtoArray read GetAllItems;
    end;

  //TGlyphsConfigurationDataEndpoint = class(TInterfacedObject, IGlyphsConfigurationEndpoint)
  //  strict private
  //    Dao: IGlyphsConfigurationDao;
  //  public
  //    constructor Create(PDao: IGlyphsConfigurationDao);
  //    function GetItemsForConfigAndCategory(PConfig: Integer; PCategory: String):TGlyphsConfigurationDtoArray;
  //    property ItemsForConfigAndCategory: TGlyphsConfigurationDtoArray read GetItemsForConfigAndCategory;
  //  end;



  { Factory for HouseSystemDataEndpoint. @br
  - Created: 2019-01-12 @br
  - Last update: 2019-01-12  }
  TFactoryHouseSystemDataEndpoint = Class
    function GetInstance: IHouseSystemDataEndpoint;
  end;

  { Factory for AyanamshaDataEndpoint. @br
  - Created: 2019-01-20 @br
  - Last update: 2019-01-20  }
  TFactoryAyanamshaDataEndpoint = Class
    function GetInstance: IAyanamshaDataEndpoint;
  end;

  { Factory for CoordinateSystemDataEndpoint. @br
  - Created: 2019-01-21 @br
  - Last update: 2019-01-21  }
  TFactoryCoordinateSystemDataEndpoint = Class
    function GetInstance: ICoordinateSystemDataEndpoint;
  end;

  { Factory for LookupValueDataEndpoint. @br
  - Created: 2019-01-23 @br
  - Last update: 2019-01-23  }
  TFactoryLookupValueDataEndpoint = Class
    strict private
      TableName: String;
    public
      constructor Create(PTableName: String);
      function GetInstance: ILookupValueDataEndpoint;
  end;

  //{ Factory for GlyphsConfigurationDataEndpoint. @br
  //- Created: 2019-02-13 @br
  //- Last update: 2019-02-13  }
  //TFactoryGlyphsConfigurationDataEndpoint = Class
  //  strict private
  //    TableName: String;
  //  public
  //    constructor Create(PTableName: String);
  //    function GetInstance: IGlyphsConfigurationDataEndpoint;
  //end;


implementation

  { THouseSystemDataEndpoint }

  Constructor THouseSystemDataEndpoint.Create(PDao: IHouseSystemsDao);
    begin
      Dao:= PDao;
    end;

  function THouseSystemDataEndpoint.GetSystem(PId: Integer): IHouseSystemDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function THouseSystemDataEndpoint.GetAllSystems: THouseSystemDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;

  { TAyanamshaDataEndpoint }
  Constructor TAyanamshaDataEndpoint.Create(PDao: IAyanamshasDao);
    begin
      Dao:= PDao;
    end;

  function TAyanamshaDataEndpoint.GetAyanamsha(PId: Integer): IAyanamshaDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TAyanamshaDataEndpoint.GetAllAyanamshas: TAyanamshaDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;


  //{ TGlyphsConfigurationDataEndpoint }
  //constructor TGlyphsConfigurationDataEndpoint.Create(PDao: IGlyphsConfigurationDao);
  //  begin
  //    Dao:= PDao;
  //  end;
  //
  //function TGlyphsConfigurationDataEndpoint.GetItemsForConfigAndCategory(PConfig: Integer;
  //              PCategory: String):TGlyphsConfigurationDtoArray;
  //  begin
  //    Result:= Dao.ReadAll(ItemId, 'O');   { TODO : uitbreiden met indicatie van config }
  //  end;

  { TFactoryHouseSystemDataEndpoint }
  function TFactoryHouseSystemDataEndpoint.GetInstance: IHouseSystemDataEndpoint;
    begin
      Result:= THouseSystemDataEndpoint.Create(TFactoryHouseSystemsDao.Create.GetInstance);
    end;

  { TFactoryAyanamshaDataEndpoint }
  function TFactoryAyanamshaDataEndpoint.GetInstance: IAyanamshaDataEndpoint;
    begin
      Result:= TAyanamshaDataEndpoint.Create(TFactoryAyanamshasDao.Create.GetInstance);
    end;


  { TCoordinateSystemDataEndpoint }
  constructor TCoordinateSystemDataEndpoint.Create(PDao: ILookUpValueDao);
    begin
      Dao:= PDao;
    end;

  function TCoordinateSystemDataEndpoint.GetName(PId: Integer): ILookUpValueDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TCoordinateSystemDataEndpoint.GetAllItems():TLookUpValueDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;

  { TLookupValueDataEndpoint }
  constructor TLookupValueDataEndpoint.Create(PDao: ILookUpValueDao);
    begin
      Dao:= PDao;
    end;

  function TLookupValueDataEndpoint.GetName(PId: Integer): ILookUpValueDto;
    begin
      Result:= Dao.Read(PId)[0];
    end;

  function TLookupValueDataEndpoint.GetAllItems():TLookUpValueDtoArray;
    begin
      Result:= Dao.ReadAll('Name');
    end;


  { TFactoryCoordinateSystemDataEndpoint }
  function TFactoryCoordinateSystemDataEndpoint.GetInstance: ICoordinateSystemDataEndpoint;
    var
      Dao: ILookUpValueDao;
      Factory: TFactoryLookUpValueDao;
    begin
      Factory:= TFactoryLookUpValueDao.create('CoordinateSystems');
      Dao:=Factory.GetInstance;
      Result:= TCoordinateSystemDataEndpoint.Create(Dao);
    end;

  { TFactoryLookupValueDataEndpoint }
  constructor TFactoryLookupValueDataEndpoint.Create(PTableName: String);
    begin
      TableName:= PTableName;
    end;

  function TFactoryLookupValueDataEndpoint.GetInstance: ILookupValueDataEndpoint;
    var
      Dao: ILookUpValueDao;
      Factory: TFactoryLookUpValueDao;
    begin
      Factory:= TFactoryLookUpValueDao.create(TableName);
      Dao:= Factory.GetInstance;
      Result:= TLookupValueDataEndpoint.Create(Dao);
    end;




end.

