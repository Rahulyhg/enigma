{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit SharedEndpoints;
{< Endpoints that are shared between different parts of Enigma. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SharedXchgDomain, SharedHandlers, SeFrontend;

type

{ Endpoint for geographic latitude. Validates and converts strings with sexagesimal values to a double. @br
- Created: 2018 @br
- Last update: 2018 }
IValidatedLatitudeEndpoint = interface ['{BFEFBE3C-75A4-4E20-882C-4BD6345982A6}']
  function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
end;

{ Endpoint for geographic longitude. Validates and converts input to a double.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedLongitudeEndpoint = interface ['{39B76C28-A780-44A3-8A9D-33936F0E3561}']
  function HandleInput(PDegrees, PMinutes, PSeconds, PDirection: String): IValidatedDouble;
end;

{ Endpoint for clock time. Converts strings to a TTime object.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedTimeEndpoint = interface ['{DC360C68-B58D-498B-B0D2-9768BBF8F628}']
  function HandleInput(PHour, PMinute, PSecond: String): IValidatedTime;
end;

{ Endpoint for calendar date. Converts strings to a TDate object.@br
- Created: 2018 @br
- Last update: 2018 }
IValidatedDateEndpoint = interface ['{D6BC2834-970E-4AF2-BEE1-6B5FD2EABCB5}']
  function HandleInput(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
end;

{ Factory for ValidatedLatitudeEndpoint. @br
- Created: 2018-11-25 @br
- Last update: 2018-11-25 }
FactoryValidatedLatitudeEndpoint = class
  public
    class function GetInstance: IValidatedLatitudeEndpoint;
  end;

{ Factory for ValidatedLongitudeEndpoint. @br
- Created: 2018-11-25 @br
- Last update: 2018-11-25 }
FactoryValidatedLongitudeEndpoint = class
  public
    class function GetInstance: IValidatedLongitudeEndpoint;
  end;

{ Factory for ValidatedDateEndpoint. @br
- Created: 2018-11-25 @br
- Last update: 2018-11-25 }
FactoryValidatedDateEndpoint = class
  public
    class function GetInstance: IValidatedDateEndpoint;
  end;

{ Factory for ValidatedTimeEndpoint. @br
- Created: 2018-11-25 @br
- Last update: 2018-11-25 }
FactoryValidatedTimeEndpoint = class
  public
    class function GetInstance: IValidatedTimeEndpoint;
  end;

implementation

uses SharedEndpointsImpl, SharedHandlersImpl;

{ FactoryValidatedLatitudeEndpoint. }
class function FactoryValidatedLatitudeEndpoint.GetInstance: IValidatedLatitudeEndpoint;
begin
  Result:= TValidatedLatitudeEndpoint.Create(TLatitudeValidationHandler.Create);
end;

{ FactoryValidatedLongitudeEndpoint. }
class function FactoryValidatedLongitudeEndpoint.GetInstance: IValidatedLongitudeEndpoint;
begin
  Result:= TValidatedLongitudeEndpoint.Create(TLongitudeValidationHandler.Create);
end;

{ FactoryValidatedDateEndpoint. }
class function FactoryValidatedDateEndpoint.GetInstance: IValidatedDateEndpoint;
begin
  Result:= TValidatedDateEndpoint.Create(TDateValidationHandler.Create(TSeFrontend.Create));
end;

{ FactoryValidatedTimeEndpoint. }
class function FactoryValidatedTimeEndpoint.GetInstance: IValidatedTimeEndpoint;
begin
  Result:= TValidatedTimeEndpoint.Create(TTimeValidationHandler.Create);
end;

end.

