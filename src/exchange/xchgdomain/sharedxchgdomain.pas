{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit SharedXchgDomain;
{< Shared objects that are used to communicate between frontend and backend.
Typically generic data as date, time, location.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

{ DTO with name and coördinates for location. @br
The constructor accepts values as given, range checks for longitude and latitude should have been done earlier. @br
- Factory: none @br
- Fake: FakeValidatedLocation @br
- Created: 2018 @br
- Last update: 2018-11-23  }
IValidatedLocation = interface ['{68F8C9BF-7080-4BB9-B9A0-D1E57EA2192C}']
  function GetName: String;
  function GetLongitude: Double;
  function GetLatitude: Double;
  property Name: String read GetName;
  property Longitude: Double read GetLongitude;
  property Latitude: Double read GetLatitude;
end;


{ DTO for calendar date and an indication if the input was valid. @br
Will be used to create a DateTime. br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-23 }
IValidatedDate = interface ['{F5F2E324-CC29-4580-9A20-8B8D59D17F10}']
  function GetYear: Integer;
  function GetMonth: Integer;
  function GetDay: Integer;
  function GetCalendar: String;
  function IsValid: Boolean;
  property Year: Integer read GetYear;
  property Month: Integer read GetMonth;
  property Day: Integer read GetDay;
  property Calendar: String read GetCalendar;
  property Valid: Boolean read IsValid;
end;

{ DTO for clock time and an indication if the input was valid. @br
Time is in UT. @br
Will be used to create a DateTime. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-23 }
IValidatedTime = interface ['{5768FCBA-0E86-4AD1-9A4B-E1009F19D87B}']
  function GetDecimalTime: Double;
  function IsValid: Boolean;
  property DecimalTime: Double read GetDecimalTime;
  property Valid: Boolean read IsValid;
end;


{ DTO with a single double value and an indication if the input was valid. @br
If Valid is false, value will contain 0.0. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-23 }
IValidatedDouble = interface ['{73878EDB-F088-4E4D-95F2-6AFB41E29273}']
  function GetValue:Double;
  function IsValid:Boolean;
  property Value: Real read GetValue;
  property Valid: Boolean read IsValid;
end;


{
 }
{ DTO for frame of reference, checks if equatorial, topocentric, heliocentric and/or sidereal positions should be used. @br
Calculates a part of the value for the SE flags. @br
- Factory: none @br
- Fake: FakedReferenceFrame @br
- Created: 2018 @br
- Last update: 2018-11-23}
IReferenceFrame = interface ['{914FCE2D-9C53-44BB-A8E6-A399B1D67C04}']
  function GetFlags: LongInt;
  property Flags: LongInt read GetFlags;
end;



implementation

end.

