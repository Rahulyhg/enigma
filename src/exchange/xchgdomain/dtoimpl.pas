{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit DtoImpl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dto;

type

  TLookUpValueDto = class(TInterfacedObject, ILookUpValueDto)
  strict private
    LId: Integer;
    LName, LDescription: String;
  public
    constructor Create(PId: Integer; PName, PDescription: String);
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
  end;

  TVersionDto = class(TInterfacedObject, IVersionDto)
    strict private
      Id, Major, Minor, Micro: Integer;
      Date: String;
    public
      constructor Create(PId: Integer; PMajor, PMinor, PMicro: Integer; PDate: String);
      function GetId: Integer;
      function GetMajor: Integer;
      function GetMinor: Integer;
      function GetMicro: Integer;
      function GetDate: String;
  end;

  THouseSystemDto = class(TInterfacedObject, IHouseSystemDto)
    strict private
      Id, NrOfHouses: Integer;
      Name, Description: String;
      CounterClockWise, QuadrantSystem, CuspIsStart: Boolean;
    public
      constructor Create(PId: Integer; PName, PDescription: String;
                  PNrOfHouses, PCounterClockWise, PQuadrantSystem, PCuspIsStart: Integer);
      function GetId: Integer;
      function GetName: String;
      function GetDescription: String;
      function GetNrOfHouses: Integer;
      function IsCounterClockWise: Boolean;
      function IsQuadrantSystem: Boolean;
      function IsCuspIsStart: Boolean;
  end;

  TAyanamshaDto = class(TInterfacedObject, IAyanamshaDto)
    strict private
      Id: Integer;
      Name, Description: String;
      Offset2000: Double;
    public
      constructor Create(Pid: Integer; PName, PDescription: String; POffset2000: Double);
      function GetId: Integer;
      function GetName: String;
      function GetDescription: String;
      function GetOffset2000: Double;
  end;

  TBodyDto = class(TInterfacedObject, IBodyDto)
    strict private
      Id, BodyCategory: Integer;
      Name: String;
    public
      constructor Create(PId: Integer; PName: String; PBodyCategory: Integer);
      function GetId: Integer;
      function GetName: String;
      function GetBodyCategory: Integer;
  end;

  TAspectDto = class(TInterfacedObject, IAspectDto)
    strict private
      Id, AspectCategory: Integer;
      Name: String;
      Angle: Double;
    public
      constructor Create(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer);
      function GetId: Integer;
      function GetName: String;
      function GetAngle: Double;
      function GetAspectCategory: Integer;
  end;

  TConfigurationDto = class(TInterfacedObject, IConfigurationDto)
    strict private
      Id, CoordinateSystem, Ayanamsha, HouseSystem, ObserverPosition, ProjectionType: Integer;
      Name, Description: String;
      BaseOrbAspects, BaseOrbMidpoints: Double;
    public
      constructor Create(Pid: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                         PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPOsition, PProjectionType: Integer);
      function GetId: Integer;
      function GetName: String;
      function GetDescription: String;
      function GetBaseOrbAspects: Double;
      function GetBaseOrbMidpoints: Double;
      function GetCoordinateSystem: Integer;
      function GetAyanamsha: Integer;
      function GetHouseSystem: integer;
      function GetObserverPosition: Integer;
      function GetProjectionType: Integer;
  end;

  TGlyphsBodiesDto = class(TInterfacedObject, IGlyphsBodiesDto)
    strict private
      BodyId: Integer;
      Glyph: String;
    public
      Constructor Create(PBodyId: Integer; PGlyph: String);
      function GetId: LongInt;
      function GetGlyph: String;
    end;

  TGlyphsConfigurationDto = class(TInterfacedObject, IGlyphsConfigurationDto)
    strict private
      ConfigId, ItemId: LongInt;
      Category, Glyph: String;
    public
      Constructor Create(PConfigId, PItemId: LongInt; PCategory, PGlyph: String);
      function GetConfigId: LongInt;
      function GetItemId: Integer;
      function GetCategory: String;
      function GetGlyph: String;
  end;


implementation

  { TLookUpValueDto }
  constructor TLookUpValueDto.Create(PId: Integer; PName, PDescription: String);
    begin
      LId:= PId;
      LName:= PName;
      LDescription:= PDescription;
    end;

  function TLookUpValueDto.GetId: Integer;
    begin
      Result:= LId;
    end;

  function TLookUpValueDto.GetName: String;
    begin
      Result:= LName;
    end;

  function TLookUpValueDto.GetDescription: String;
    begin
      Result:= LDescription;
    end;

  { TVersionDto }
  constructor TVersionDto.Create(PId: Integer; PMajor, PMinor, PMicro: Integer; PDate: String);
    begin
      Id:= PId;
      Major:= PMajor;
      Minor:= PMinor;
      Micro:= PMicro;
      Date:= PDate;
    end;

  function TVersionDto.GetId: INteger;
    begin
      Result:= Id;
    end;

  function TVersionDto.GetMajor: Integer;
    begin
      Result:= Major;
    end;

  function TVersionDto.GetMinor: Integer;
    begin
      Result:= Minor;
    end;

  function TVersionDto.GetMicro: Integer;
    begin
      Result:= Micro;
    end;

  function TVersionDto.GetDate: String;
    begin
      Result:= Date;
    end;

  { THouseSystemDto }

  constructor THouseSystemDto.Create(PId: Integer; PName, PDescription: String;
              PNrOfHouses, PCounterClockWise, PQuadrantSystem, PCuspIsStart: Integer);
    begin
      Id:= PId;
      Name:= PName;
      Description:= PDescription;
      NrOfHouses:= PNrOfHouses;
      CounterClockWise:= Not(PCounterClockWise = 0);
      QuadrantSystem:= Not(PQuadrantSystem = 0);
      CuspIsStart:= Not(PCuspIsStart = 0);
    end;

  function THouseSystemDto.GetId: Integer;
    begin
      Result:= Id;
    end;

  function THouseSystemDto.GetName: String;
    begin
      Result:= Name;
    end;

  function THouseSystemDto.GetDescription: String;
    begin
      Result:= Description;
    end;

  function THouseSystemDto.GetNrOfHouses: Integer;
    begin
      Result:= NrOfHouses;
    end;

  function THouseSystemDto.IsCounterClockWise: Boolean;
    begin
      Result:= CounterClockWise;
    end;

  function THouseSystemDto.IsQuadrantSystem: Boolean;
    begin
      Result:= QuadrantSystem;
    end;

  function THouseSystemDto.IsCuspIsStart: Boolean;
    begin
      Result:= CuspIsStart;
    end;


  { TAyanamshaDto }

  constructor TAyanamshaDto.Create(Pid: Integer; PName, PDescription: String; POffset2000: Double);
    begin
      Id:= PId;
      Name:= PName;
      Description:= PDescription;
      Offset2000:= POffset2000;
    end;

  function TAyanamshaDto.GetId: Integer;
    begin
      Result:= Id;
    end;

  function TAyanamshaDto.GetName: String;
    begin
      Result:= Name;
    end;

  function TAyanamshaDto.GetDescription: String;
    begin
      Result:= Description;
    end;

  function TAyanamshaDto.GetOffset2000: Double;
    begin
      Result:= Offset2000;
    end;

  { TBodyDto }

  constructor TBodyDto.Create(PId: Integer; PName: String; PBodyCategory: Integer);
    begin
      Id:= Pid;
      Name:= PName;
      BodyCategory:= PBodyCategory;
    end;

  function TBodyDto.GetId: Integer;
    begin
      Result:= Id;
    end;

  function TBodyDto.GetName: String;
    begin
      Result:= Name;
    end;

  function TBodyDto.GetBodyCategory: Integer;
    begin
      Result:= BodyCategory;
    end;


  { TAspectDto }

  constructor TAspectDto.Create(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer);
    begin
      Id:= Pid;
      Name:= PName;
      Angle:= PAngle;
      AspectCategory:= PAspectCategory;
    end;

  function TAspectDto.GetId: Integer;
    begin
      Result:= Id;
    end;

  function TAspectDto.GetName: String;
    begin
      Result:= Name;
    end;

  function TAspectDto.GetAngle: Double;
    begin
      Result:= Angle;
    end;


  function TAspectDto.GetAspectCategory: Integer;
    begin
      Result:= AspectCategory;
    end;

  { TConfigurationDto }
  constructor TConfigurationDto.Create(Pid: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
                     PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer);
    begin
      Id:= PId;
      Name:= PName;
      Description:= PDescription;
      BaseOrbAspects:= PBaseOrbAspects;
      BaseOrbMidpoints:= PBaseOrbMidpoints;
      CoordinateSystem:= PCoordinateSystem;
      Ayanamsha:= PAYanamsha;
      HouseSystem:= PHouseSystem;
      ObserverPosition:= PObserverPosition;
      ProjectionType:= PProjectionType;
    end;

  function TConfigurationDto.GetId: Integer;
    begin
      Result:= Id;
    end;

  function TConfigurationDto.GetName: String;
    begin
      Result:= Name;
    end;

  function TConfigurationDto.GetDescription: String;
    begin
      Result:= Description;
    end;

  function TConfigurationDto.GetBaseOrbAspects: Double;
    begin
      Result:= BaseOrbAspects;
    end;

  function TConfigurationDto.GetBaseOrbMidpoints: Double;
    begin
      Result:= BaseOrbMidpoints;
    end;

  function TConfigurationDto.GetCoordinateSystem: Integer;
    begin
      Result:= CoordinateSystem;
    end;

  function TConfigurationDto.GetAyanamsha: Integer;
    begin
      Result:= Ayanamsha;
    end;

  function TConfigurationDto.GetHouseSystem: integer;
   begin
     Result:= HouseSystem;
   end;

  function TConfigurationDto.GetObserverPosition: Integer;
    begin
      Result:= ObserverPosition;
    end;

  function TConfigurationDto.GetProjectionType: Integer;
    begin
      Result:= ProjectionType;
    end;

  { TGlyphsBodiesDto }
  constructor TGlyphsBodiesDto.Create(PBodyId: Integer; PGlyph: String);
    begin
      BodyId:= PBodyId;
      Glyph:= PGlyph;
    end;

  function TGlyphsBodiesDto.GetId: LongInt;
    begin
      Result:= BodyId;
    end;

  function TGlyphsBodiesDto.GetGlyph: String;
    begin
      Result:= Glyph;
    end;

  { TGlyphsConfigurationDto }
  Constructor TGlyphsConfigurationDto.Create(PConfigId, PItemId: LongInt; PCategory, PGlyph: String);
    begin
      ConfigId:= PConfigId;
      ItemId:= PItemId;
      Category:= PCategory;
      Glyph:= PGlyph;
    end;

  function TGlyphsConfigurationDto.GetConfigId: LongInt;
    begin
      Result:= ConfigId;
    end;

  function TGlyphsConfigurationDto.GetItemId: Integer;
    begin
      Result:= ItemId;
    end;

  function TGlyphsConfigurationDto.GetCategory: String;
    begin
      Result:= category;
    end;

  function TGlyphsConfigurationDto.GetGlyph: String;
    begin
      Result:= Glyph;
    end;



end.

