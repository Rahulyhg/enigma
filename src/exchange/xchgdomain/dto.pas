{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit Dto;
{< Interfaces for dto's that can be used to communicate with a dao. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
type

  IDto = Interface;

  ILookUpValueDto = Interface;
  IVersionDto = Interface;
  IHouseSystemDto = Interface;
  IAyanamshaDto = Interface;
  IBodyDto = Interface;
  IAspectDto = Interface;
  IConfigurationDto = Interface;
  IGlyphsBodiesDto = Interface;
  IGlyphsConfigurationDto = Interface;

  TLookUpValueDtoArray = Array of ILookUpValueDto;
  TVersionDtoArray = Array of IVersionDto;
  THouseSystemDtoArray = Array of IHouseSystemDto;
  TAyanamshaDtoArray = Array of IAyanamshaDto;
  TBodyDtoArray = Array of IBodyDto;
  TAspectDtoArray = Array of IAspectDto;
  TConfigurationDtoArray = Array of IConfigurationDto;
  TGlyphsBodiesDtoArray = Array of IGlyphsBodiesDto;
  TGlyphsConfigurationDtoArray = Array of IGlyphsConfigurationDto;

  TDtoArray = Array of IDto;


  { Parent for all Dto's.  }        { TODO : remove parent }
  IDto = interface ['{99CE8B6A-5024-4214-BB94-280C47458497}']
    function GetId: Integer;
  end;

  { Dto for simple lookup values that consists of only id, name and description. @b
  - Factory: none @br
  - Fake: FakeLookUpValueDto and FakeLookUpValueDto2 @br
  - Created: 2018 @br
  - Last update: 2018-12-31  }
  ILookUpValueDto = Interface(IDto) ['{EA52ABB3-FB28-4A0A-BCED-171495F2BB4A}']
    function GetName: String;
    function GetDescription: String;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
  end;

  { Dto for versions, both for database and for application. @b
  - Factory: none @br
  - Fake: FakeVersionDto @br
  - Created: 2018-12-30 @br
  - Last update: 2018-12-30  }
  IVersionDto = Interface(IDto) ['{DEA9345E-EEB4-4A81-A84E-A19C6466E953}']
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
    property Id: Integer read GetId;
    property Major: Integer read GetMajor;
    property Minor: Integer read GetMinor;
    property Micro: Integer read GetMicro;
    property Date: String read GetDate;
  end;

  { Dto for house systems. @b
  - Factory: none @br
  - Fake: FakeHouseSystemDto and FakeHouseSystemDto2 @br
  - Created: 2018-12-31 @br
  - Last update: 2018-12-31  }
  IHouseSystemDto = Interface(IDto) ['{31DA7D77-217B-4656-87EE-856B8BD3E834}']
    function GetName: String;
    function GetDescription: String;
    function GetNrOfHouses: Integer;
    function IsCounterClockWise: Boolean;
    function IsQuadrantSystem: Boolean;
    function IsCuspIsStart: Boolean;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property NrOfHouses: Integer read GetNrOfHouses;
    property CounterClockWise: Boolean read IsCounterClockWise;
    property QuadrantSystem: Boolean read IsQuadrantSystem;
    property CuspIsStart: Boolean read IsCuspIsStart;
  end;

  { Dto for ayanamsha's. @b
  - Factory: none @br
  - Fake: FakeAyanamshaDto and FakeAyanamshaDto2 @br
  - Created: 2018-12-31 @br
  - Last update: 2019-01-01  }
  IAyanamshaDto = Interface(IDto) ['{D53A8AA9-7A60-4FA9-B2F4-D9FEF449899B}']
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Double;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property Offset2000: Double read GetOffset2000;
  end;

  { Dto for celestial bodies. @b
  - Factory: none @br
  - Fake: FakeBodyDto and FakeBodyDto2 @br
  - Created: 2019-01-02 @br
  - Last update: 2019-01-02  }
  IBodyDto = Interface(IDto) ['{88F0164D-F983-4315-BD3C-C5B60560D59A}']
    function GetName: String;
    function GetBodyCategory: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property BodyCategory: INteger read GetBodyCategory;
  end;

  { Dto for aspects. @b
  - Factory: none @br
  - Fake: FakeAspectDto and FakeAspectDto2 @br
  - Created: 2019-01-07 @br
  - Last update: 2019-01-07  }
  IAspectDto = Interface(IDto) ['{7BF8D7ED-BB57-418E-8F82-58D6CA375507}']
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Angle: Double read GetAngle;
    property AspectCategory: Integer read GetAspectCategory;
  end;


  { Dto for configurations. @b
  - Factory: none @br
  - Fake: FakeConfigurationDto and FakeConfigurationDto2 @br
  - Created: 2019-01-09 @br
  - Last update: 2019-01-09  }
  IConfigurationDto = Interface(IDto) ['{423EECAD-8F51-4B24-9764-771FC2B5234F}']
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
    property Id: Integer read GetId;
    property Name: String read GetName;
    property Description: String read GetDescription;
    property BaseOrbAspects: Double read GetBaseOrbAspects;
    property BaseOrbMidpoints: Double read GetBaseOrbMidpoints;
    property CoordinateSystem: Integer read GetCoordinateSystem;
    property Ayanamsha: Integer read GetAyanamsha;
    property HouseSystem: Integer read GetHouseSystem;
    property ObserverPosition: Integer read GetObserverPosition;
    property ProjectionType: Integer read GetProjectionType;
  end;


  { Dto for supported glyph-body combinations. @b
  - Factory: none @br
  - Fake: FakeGlyphsBodiesDto and FakeGlyphsBodiesDto2 @br
  - Created: 2019-02-08 @br
  - Last update: 2019-02-10  }
  IGlyphsBodiesDto = Interface(IDto) ['{261F420A-E11B-48DC-A51F-E5FE93321874}']
    function GetId: LongInt;
    function GetGlyph: String;
    property BodyId: Integer read GetId;
    property Glyph: String read GetGlyph;
  end;

  { Dto for actual glyphs as used in a configuration. @br
    The variable Category discriminates between celestial object/sign/aspect/diverse and has, in the same sequence,
    the following possivle values: O, S, A, D.
  - Factory: none @br
  - Fake: FakeGlyphsConfigurationDto and FakeGlyphsConfigurationDto2 @br
  - Created: 2019-02-11 @br
  - Last update: 2019-02-13  }
  IGlyphsConfigurationDto = Interface ['{A286634A-5A78-4868-B326-37DBA93B549F}']
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
    property ConfigId: Integer read GetConfigId;
    property ItemId: Integer read GetItemId;
    property Category: String read GetCategory;
    property Glyph: String read GetGlyph;
  end;

implementation

end.

