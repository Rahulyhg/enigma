{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XChartsDomainImpl;
{< Implementations for XChartsDomain. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain, SharedXchgDomain;

type

  THousePositionFullArray = Array of IHousePositionFull;

  THouseSystemSpec = class(TInterfacedObject, IHouseSystemSpec)
    strict private
      LName, LSeId, LDescription: String;
      LId, LNumberOfHouses: Integer;
      LQuadrantSystem, LCuspIsStart: Boolean;
    public
      constructor Create(PName, PSeID, PDescription: String; PId, PNumberOfHouses: Integer;
                         PQuadrantSystem, PCuspIsStart: Boolean);
      function GetName: String;
      function GetSeId: String;
      function GetDescription: String;
      function GetId: Integer;
      function GetNumberOfHouses: Integer;
      function IsQuadrantSystem: Boolean;
      function IsCuspIsStart: Boolean;
  end;

  THousePositionFull = class(TInterfacedObject, IHousePositionFull)
    strict private
      LLongitude, LRightAscension, LDeclination, LAzimuth, LAltitude: Double;
      function GetLongitude: Double;
      function GetRightAscension: Double;
      function GetDeclination: Double;
      function GetAzimuth: Double;
      function GetAltitude: Double;
    public
      constructor Create(PLongitude, PRightAscension, PDeclination, PAzimuth, PAltitude: Double);
  end;

  TCelestialObjectSimple = class(TInterfacedObject, ICelestialObjectSimple)
    strict private
      LMainPos, LDeviationPos, LDistancePos, LMainSpeed, LDeviationSpeed, LDistanceSpeed: Double;
      function GetMainPos: Double;
      function GetDeviationPos: Double;
      function GetDistancePos: Double;
      function GetMainSpeed: Double;
      function GetDeviationSpeed: Double;
      function GetDistanceSpeed: Double;
    public
      constructor Create(CalculatedValues: Array of Double);
  end;

  TCelestialObjectFull = class(TInterfacedObject, ICelestialObjectFull)
    strict private
      LObjectId: Integer;
      LEclipticalPos, LEquatorialPos, LHorizontalPos: ICelestialObjectSimple;
      function GetObjectId: Integer;
      function GetEclipticalPos: ICelestialObjectSimple;
      function GetEquatorialPos: ICelestialObjectSimple;
      function GetHorizontalPos: ICelestialObjectSimple;
    public
      constructor Create(PObjectId: Integer; PEclipticalPos, PEquatorialPos, PHorizontalPos: ICelestialObjectSimple);
  end;

  THousesResponse = class(TInterfacedObject, IHousesResponse)
    strict private
      LCusps: THousePositionFullArray;
      LMc, LAsc: IHousePositionFull;
      LHouseSystemSpec: IHouseSystemSpec;
      function GetMc: IHousePositionFull;
      function GetAsc: IHousePositionFull;
      function GetCusps: THousePositionFullArray;
      function GetHouseSystemSpec: IHouseSystemSpec;
    public
      constructor Create(PMc, PAsc: IHousePositionFull; PCusps: THousePositionFullArray; PHouseSystemSpec: IHouseSystemSpec);
  end;

  TCalculationSettings = class(TInterfacedObject, ICalculationSettings)
    strict private
      LPosition, LAyanamsha: Integer;
      LObjects: TIntArray;
      LHouseSystemSpec: IHouseSystemSpec;   { TODO : R_0.5 replace housesystem with integer }
      LReferenceFrame: IReferenceFrame;     { TODO : R_0.5 replace reference frame with integer }
    public
      constructor Create(PPosition, PAyanamsha: Integer;
                         PObjects: TIntArray;
                         PReferenceFrame: IReferenceFrame;
                         PHouseSystemSpec: IHouseSystemSpec);
      function GetPosition: Integer;
      function GetAyanamsha: Integer;
      function GetHouseSystemSpec: IHouseSystemSpec;
      function GetObjects: TIntArray;
      function GetReferenceFrame: IReferenceFrame;
  end;

  TFullChartRequest = class(TInterfacedObject, IFullChartRequest)
    strict private
      LName: String;
      LLocation: IValidatedLocation;
      LDate: IValidatedDate;
      LTime: IValidatedTime;
      LCalculationSettings: ICalculationSettings;
    public
      constructor Create(PName: String; PLocation: IValidatedLocation; PDate: IValidatedDate; PTime: IValidatedTime;
        PCalculationSettings: ICalculationSettings);
      function GetName: String;
      function GetLocation: IValidatedLocation;
      function GetDate: IValidatedDate;
      function GetTime: IValidatedTime;
      function GetCalculationSettings: ICalculationSettings;
  end;

  TFullChartResponse = class(TInterfacedObject, IFullChartResponse)
    strict private
      LName: String;
      LAllObjects: TCelestialObjectFullArray;
      LHouses: IHousesResponse;
      LFullChartRequest: IFullChartRequest;   { TODO: R_0.5 Use only relevant parts of FullChartRequest }
    public
      constructor Create(PName: String;
                         PAllObjects: TCelestialObjectFullArray;
                         PHouses: IHousesResponse;
                         PFullChartRequest: IFullChartRequest);
      function GetName: String;
      function GetAllObjects: TCelestialObjectFullArray;
      function GetHouses: IHousesResponse;
      function GetFullChartRequest: IFullChartRequest;
  end;


implementation

{ THouseSystemSpec }
  constructor THouseSystemSpec.Create(PName, PSeID, PDescription: String; PId, PNumberOFHouses: Integer;
                     PQuadrantSystem, PCuspISStart: Boolean);
    begin
      self.LName:= PName;
      self.LSeId:= PSeID;
      self.LDescription:= PDescription;
      self.LId:= PId;
      self.LNumberOfHouses:= PNumberOFHouses;
      self.LQuadrantSystem:= PQuadrantSystem;
      self.LCuspIsStart:= PCuspISStart;
    end;

  function THouseSystemSpec.GetName: String;
    begin
      Result:= LName;
    end;

  function THouseSystemSpec.GetSeId: String;
    begin
      Result:= LSeId;
    end;

  function THouseSystemSpec.GetDescription: String;
    begin
      Result:= LDescription;
    end;

  function THouseSystemSpec.GetId: Integer;
    begin
      Result:= LId;
    end;

  function THouseSystemSpec.GetNumberOfHouses: Integer;
    begin
      Result:= LNumberOfHouses;
    end;

  function THouseSystemSpec.IsQuadrantSystem: Boolean;
    begin
      Result:= LQuadrantSystem;
    end;

  function THouseSystemSpec.IsCuspIsStart: Boolean;
    begin
      Result:= LCuspIsStart;
    end;


{ THousePositionFull }

  constructor THousePositionFull.Create(PLongitude, PRightAscension, PDeclination, PAzimuth, PAltitude: Double);
    begin
      LLongitude:= PLongitude;
      LRightAscension:= PRightAscension;
      LDeclination:= PDeclination;
      LAzimuth:= PAzimuth;
      LAltitude:= PAltitude;
    end;

  function THousePositionFull.GetLongitude: Double;
    begin
      Result:= LLongitude;
    end;

  function THousePositionFull.GetRightAscension: Double;
    begin
      Result:= LRightAscension;
    end;

  function THousePositionFull.GetDeclination: Double;
    begin
      Result:= LDeclination;
    end;

  function THousePositionFull.GetAzimuth: Double;
    begin
      Result:= LAzimuth;
    end;

  function THousePositionFull.GetAltitude: Double;
    begin
      Result:= LAltitude;
    end;


{ TCelestialObjectSimple }

  constructor TCelestialObjectSimple.Create(CalculatedValues: Array of Double);
    begin
      LMainPos:= CalculatedValues[0];
      LDeviationPos:= CalculatedValues[1];
      LDistancePos:= CalculatedValues[2];
      LMainSpeed:= CalculatedValues[3];
      LDeviationSpeed:= CalculatedValues[4];
      LDistanceSpeed:= CalculatedValues[5];
    end;


  function TCelestialObjectSimple.GetMainPos: Double;
    begin
      Result:= LMainPos;
    end;

  function TCelestialObjectSimple.GetDeviationPos: Double;
    begin
      Result:= LDeviationPos;
    end;

  function TCelestialObjectSimple.GetDistancePos: Double;
    begin
      Result:= LDistancePos;
    end;

  function TCelestialObjectSimple.GetMainSpeed: Double;
    begin
      Result:= LMainSpeed;
    end;

  function TCelestialObjectSimple.GetDeviationSpeed: Double;
    begin
      Result:= LDeviationSpeed;
    end;

  function TCelestialObjectSimple.GetDistanceSpeed: Double;
    begin
      Result:= LDistanceSpeed;
    end;



{ TCelestialObjectFull }

  constructor TCelestialObjectFull.Create(PObjectId: Integer;
                     PEclipticalPos, PEquatorialPos, PHorizontalPos: ICelestialObjectSimple);
    begin
      LObjectId:= PObjectId;
      LEclipticalPos:= PEclipticalPos;
      LEquatorialPos:= PEquatorialPos;
      LHorizontalPos:= PHorizontalPos;
    end;

  function TCelestialObjectFull.GetObjectId: Integer;
    begin
      Result:= LObjectId;
    end;

  function TCelestialObjectFull.GetEclipticalPos: ICelestialObjectSimple;
    begin
      Result:= LEclipticalPos;
    end;

  function TCelestialObjectFull.GetEquatorialPos: ICelestialObjectSimple;
    begin
      Result:= LEquatorialPos;
    end;

  function TCelestialObjectFull.GetHorizontalPOs: ICelestialObjectSimple;
    begin
      Result:= LHorizontalPos;
    end;


{ THouseResponse }
  constructor THousesResponse.Create(PMc, PAsc: IHousePositionFull;
                                     PCusps: THousePositionFullArray;
                                     PHouseSystemSpec: IHouseSystemSpec);
    begin
      LMc:= PMc;
      LAsc:= PAsc;
      LCusps:= PCusps;
      LHouseSystemSpec:= PHouseSystemSpec;
    end;

  function THousesResponse.GetMc: IHousePositionFull;
    begin
      Result:= LMc;
    end;

  function THousesResponse.GetASc: IHousePositionFull;
    begin
      Result:= LAsc;
    end;

  function THousesResponse.GetCusps: THousePositionFullArray;
    begin
      Result:= LCusps;
    end;

  function THousesResponse.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= LHouseSystemSpec;
    end;



{ TCalculationSettings }
  constructor TCalculationSettings.Create(PPosition, PAyanamsha: Integer;
     PObjects: TIntArray; PReferenceFrame: IReferenceFrame; PHouseSystemSpec: IHouseSystemSpec);
    begin
     LPosition:= PPosition;
     LAyanamsha:= PAyanamsha;
     LHouseSystemSpec:= PHouseSystemSpec;
     LObjects:=PObjects;
     LReferenceFrame:= PReferenceFrame;
    end;

  function TCalculationSettings.GetPosition: Integer;
    begin
      Result:=LPosition;
    end;

  function TCalculationSettings.GetAyanamsha: Integer;
    begin
      Result:=LAyanamsha;
    end;

  function TCalculationSettings.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= LHouseSystemSpec;
    end;

  function TCalculationSettings.GetObjects: TIntArray;
    begin
      Result:=LObjects;
    end;

  function TCalculationSettings.GetReferenceFrame: IReferenceFrame;
    begin
      Result:= LReferenceFrame;
    end;


 { TFullChartRequest }

   constructor TFullChartRequest.Create(PName: String;
                                        PLocation: IValidatedLocation;
                                        PDate: IValidatedDate;
                                        PTime: IValidatedTime;
                                        PCalculationSettings: ICalculationSettings);
     begin
       LName:= PName;
       LLocation:= PLocation;
       LDate:= PDate;
       LTime:= PTime;
       LCalculationSettings:=PCalculationSettings;
     end;

  function TFullChartRequest.GetName: String;
    begin
      Result := LName;
    end;

  function TFullChartRequest.GetLocation: IValidatedLocation;
    begin
      Result := LLocation;
    end;

  function TFullChartRequest.GetDate: IValidatedDate;
    begin
      Result := LDate;
    end;

  function TFullChartRequest.GetTime: IValidatedTime;
    begin
      Result:= LTime;
    end;

  function TFullChartRequest.GetCalculationSettings: ICalculationSettings;
    begin
      Result:= LCalculationSettings;
    end;


{ TFullChartResponse }
  constructor TFullChartResponse.Create(PName: String; PAllObjects: TCelestialObjectFullArray; PHouses: IHousesResponse;
                                        PFullChartRequest: IFullChartRequest);
    begin
      LName:= PName;
      LAllObjects:= PAllObjects;
      LHouses:= PHouses;
      LFullChartRequest:= PFullChartRequest;
    end;

  function TFullChartResponse.GetName: String;
    begin
      Result:= LName;
    end;

  function TFullChartResponse.GetAllObjects: TCelestialObjectFullArray;
    begin
      Result:= LAllObjects;
    end;

  function TFullChartResponse.GetHouses: IHousesResponse;
    begin
      Result:= LHouses;
    end;

  function TFullChartResponse.GetFullChartRequest: IFullChartRequest;
    begin
      Result:= LFullChartRequest;
    end;




end.

