{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit XChartsDomain;
{< Objects for charts that are used to communicate between frontend and backend. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SharedXchgDomain;

type

IHousePositionFull = interface;
ICelestialObjectFull = interface;
THousePositionFullArray = Array of IHousePositionFull;
TCelestialObjectFullArray = Array of ICelestialObjectFull;
TIntArray = Array of Integer;

{ Specifications for a housesystem. @br
- Factory: none @br
- Fake: FakeHouseSystemSpec @br
- Created: 2018 @br
- Last update: 2018-12-09
}
IHouseSystemSpec = interface ['{DC13CF48-133C-47DC-8485-EB84986E9A96}']     { TODO R_0.5 : refactor, retrieve id for SE from new mapping class }
  function GetName: String;
  function GetSeId: String;
  function GetDescription: String;
  function GetId: Integer;
  function GetNumberOfHouses: Integer;
  function IsQuadrantSystem: Boolean;
  function IsCuspIsStart: Boolean;
  property Name:String read GetName;
  { Id as used by the Swiss Ephemeris. }
  property SeId: String read GetSeId;
  property Description: String read GetDescription;
  { Id as used by Enigma. }
  property Id: Integer read GetId;
  property NumberOfHouses: Integer read GetNumberOfHouses;
  property QuadrantSystem: Boolean read IsQuadrantSystem;
  property CuspIsStart: Boolean read IsCuspIsStart;
end;

{ Positions for a house cusp in ecliptic, equatorial and horizontal coördinates. No latitude, as this is always zero.@br
- Factory: none @br
- Fake: FakeHousePositionFull @br
- Created: 2018 @br
- Last update: 2018-12-10}
IHousePositionFull = interface ['{3FD7D6FC-B650-4E7E-8AFC-E12F186A3C5A}']
  function GetLongitude: Double;
  function GetRightAscension: Double;
  function GetDeclination: Double;
  function GetAzimuth: Double;
  function GetAltitude: Double;
  property Longitude: Double read GetLongitude;
  property RightAscension: Double read GetRightAscension;
  property Declination: Double read GetDeclination;
  property Azimuth: Double read GetAzimuth;
  property Altitude: Double read GetAltitude;
end;

{ DTO for a position of a celestial object, defines three elements of a coördinatesystem and also the speed of these
elements. @br
DeviationPos and DeviationSpeed define the deviation from the mainplain in degrees, resp. latitude, declination or
altitude.!@br
DistancePos and DistanceSpeed define the distance from the observationpoint in AU and is the same for ecliptical,
equatorial and horizontal coördinates. @br
The constructor expects an array of 6 doubles as calculated by the SE and respectively containing the vlaues for
MainPos, DeviationPos, DistancePos, MainSpeed, DeviationSpeed and DistanceSpeed in that sequence.@br
- Factory: none @br
- Fake: FakeCelestialObjectSimple @br
- Created: 2018 @br
- Last update: 2018-12-10}
ICelestialObjectSimple = interface ['{7B44D69C-14B4-4A73-9F35-644831390CED}']
  function GetMainPos: Double;
  function GetDeviationPos: Double;
  function GetDistancePos: Double;
  function GetMainSpeed: Double;
  function GetDeviationSpeed: Double;
  function GetDistanceSpeed: Double;
  property MainPos: Double read GetMainPos;
  property DeviationPos: Double read GetDeviationPos;
  property DistancePos: Double read GetDistancePos;
  property MainSpeed: Double read GetMainSpeed;
  property DeviationSpeed: Double read GetDeviationSpeed;
  property DistanceSpeed: Double read GetDistanceSpeed;
end;

{ DTO for a fully defined position of a celestial object. @br
- Factory: none @br
- Fake: FakeCelestialObjectFull @br
- Created: 2018 @br
- Last update: 2018-12-10}
ICelestialObjectFull = interface ['{AB456E97-EB4A-4F9A-8680-11FCF90BA98F}']
  function GetObjectId: Integer;
  function GetEclipticalPos: ICelestialObjectSimple;
  function GetEquatorialPos: ICelestialObjectSimple;
  function GetHorizontalPos: ICelestialObjectSimple;
  property ObjectId: Integer read GetObjectId;
  property EclipticalPos: ICelestialObjectSimple read GetEclipticalPos;
  property EquatorialPos: ICelestialObjectSimple read GetEquatorialPos;
  property HorizontalPos: ICelestialObjectSimple read GetHorizontalPos;
end;

{ Response to a request to calculate houses. For convenience, cusps start with index 1. @br
- Factory: none @br
- Fake: FakeHousesResponse @br
- Created: 2018 @br
- Last update: 2018-12-13}
IHousesResponse = interface ['{81CC71EB-1828-403C-B7F8-F9BED644BD9B}']
  function GetMc: IHousePositionFull;
  function GetAsc: IHousePositionFull;
  function GetCusps: THousePositionFullArray;
  function GetHouseSystemSpec: IHouseSystemSpec;
  property Mc: IHousePositionFull read GetMc;
  property Asc: IHousePositionFull read GetAsc;
  property HouseSystemSpec: IHouseSystemSpec read GetHouseSystemSpec;
  property Cusps: THousePositionFullArray read GetCusps;
end;

{ DTO for the settings for a calculation. Calculates the value for the SE flags.@br
- Factory: none @br
- Fake: FakeCalculationSettings @br
- Created: 2018 @br
- Last update: 2018-12-09  }
ICalculationSettings = interface ['{5085546F-A11B-4A31-BE4F-3FDC621A464B}']
  function GetPosition: Integer;
  function GetAyanamsha: Integer;
  function GetHouseSystemSpec: IHouseSystemSpec;
  function GetObjects: TIntArray;
  function GetReferenceFrame: IReferenceFrame;
  property Position: Integer read GetPosition;
  property Ayanamsha: Integer read GetAyanamsha;
  property HouseSystemSpec: IHouseSystemSpec read GetHouseSystemSpec;
  property Objects: TIntArray read GetObjects;
  property ReferenceFrame: IReferenceFrame read GetReferenceFrame;
end;

{ Request for the calculation of a chart. @br
- Factory: none @br
- Fake: FakeFullChartRequest @br
- Created: 2018 @br
- Last update: 2018-12-12  }
IFullChartRequest = interface ['{4D15622F-B0BA-4404-9056-4FF1A8E7D596}']
    function GetName: String;
    function GetLocation: IValidatedLocation;
    function GetDate: IValidatedDate;
    function GetTime: IValidatedTime;
    function GetCalculationSettings: ICalculationSettings;
    property Name: String read GetName;
    property Location: IValidatedLocation read GetLocation;
    property Date: IValidatedDate read GetDate;
    property Time: IValidatedTime read GetTime;
    property CalculationSettings: ICalculationSettings read GetCalculationSettings;
end;

{ DTO containing all data for a calculated chart. @br
- Factory: none @br
- Fake: FakeFullChartResponse @br
- Created: 2018 @br
- Last update: 2018-12-13  }
IFullChartResponse = interface ['{35789E0A-E284-4E95-A21C-8A10820B1EA7}']
  function GetName: String;
  function GetAllObjects: TCelestialObjectFullArray;
  function GetHouses: IHousesResponse;
  function GetFullChartRequest: IFullChartRequest;
  property Name: String read GetName;
  property AllObjects: TCelestialObjectFullArray read GetAllObjects;
  property Houses: IHousesResponse read GetHouses;
  property FullChartRequest: IFullChartRequest read GetFullChartRequest;
end;



implementation

end.

