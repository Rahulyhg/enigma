{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit Dao;
{< Interfaces for dao's. }

{$mode objfpc}{$H+}
interface

uses
  Classes, SysUtils, db, sqldb, sqlite3conn, Dto;

type

  { Creates and frees database connections. @br
  - Factory: none @br
  - Fake: none. @br
  - Created: 2018-12-29 @br
  - Last update: 2018-12-29  }
  IDatabaseInstance = interface ['{0FBC334B-C966-49FE-9FD8-376C8B875694}']
    function GetConnection: TSQLite3Connection;
    procedure ReleaseConnection;
    property Connection: TSQLite3Connection read GetConnection;
  end;

  { Helper class for selections via DAO's. Do not use this class otherwise. @br
  - Factory: TFactoryDaoSelectHelper @br
  - Fake: ...TODO... @br
  - Created: 2018-12-24 @br
  - Last update: 2019-01-06  }
  IDaoSelectHelper = interface ['{BDDA0C4A-02A5-495B-9F42-FD9B5C35A627}']
    function InitializeSelect(PQueryText: String): TSQLQuery;
    procedure Release;
  end;

  { Helper class for DML actions (Create, Updpate, Delete) via DAO's. Do not use this class otherwise. @br
  - Factory: TFactoryDaoDMLCudHelper @br
  - Fake: ...TODO... @br
  - Created: 2018-12-24 @br
  - Last update: 2019-01-06  }
  IDaoDMLCudHelper = interface ['{74440013-9305-458F-84BB-1167E16C794F}']
    procedure RunDmlCUD(PQueryText: String);
    function RunDml(PQueryText: String): String;
  end;

  { Generic DAO for lookup values. @br
  - Factory: TFactoryLookUpValueDao @br
  - Fake: FakeLookUpValueDao @br
  - Created: 2018-12-17 @br
  - Last update: 2019-01-06 }
  ILookUpValueDao = interface ['{0DA8CA02-148C-4627-9559-10B25B6E159A}']
    function Read(PId: Integer): TLookUpValueDtoArray;
    function ReadAll(SortColumn: String): TLookUpValueDtoArray;
    function Insert(PId: Integer; PName, PDescription: String): String;
    function Update(PId: Integer; PName, PDescription: String): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for ApplicationVersions and DatabaseVersions. @br
  This data changes only once per new release and will not be deleted, therefore only read and insert is suppported.
  - Factory: TFactoryVersionsDaonone @br
  - Fake: FakeVersionDao @br
  - Created: 2018-12-30 @br
  - Last update: 2019-01-06 }
  IVersionsDao = interface ['{9936E890-3D2A-488C-B525-55D0F5A35F4C}']
    function Read(PId: Integer): TVersionDtoArray;
    function ReadAll(PSortColumn: String): TVersionDtoArray;
    function Insert(PId, PMajor, PMinor, PMicro: Integer; PDate: String): String;
  end;

  { DAO for Housesystems. @br
  - Factory: TFactoryHouseSystemsDao @br
  - Fake: FakeHouseSystemsDao @br
  - Created: 2018-12-30 @br
  - Last update:  2019-01-06 }
  IHouseSystemsDao = interface ['{DEDC78A6-605B-4B9A-AB0F-2607989093EA}']
    function Read(PId: Integer): THouseSystemDtoArray;
    function ReadAll(PSortColumn: String): THouseSystemDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                    PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    function Update(PId: Integer; PName, PDescription: String; PNrOfHouses: Integer;
                    PCounterClockWise, PQuadrantSystem, PCuspIsStart: Boolean): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for Ayanamsha's. @br
  - Factory: TFactoryAyanamshasDao @br
  - Fake: FakeAyanamshaDao @br
  - Created: 2019-01-01 @br
  - Last update: 2019-01-06 }
  IAyanamshasDao = interface ['{AB0CEE00-01D4-4B81-9747-328542F42965}']
    function Read(PId: Integer): TAyanamshaDtoArray;
    function ReadAll(PSortColumn: String): TAyanamshaDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    function Update(PId: Integer; PName, PDescription: String; POffset2000: Double): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for celestial bodies. @br
  - Factory: TFactoryBodiesDao @br
  - Fake: FakeBodiesDao @br
  - Created: 2019-01-02 @br
  - Last update: 2019-01-06 }
  IBodiesDao = interface ['{91ED57C3-B3BE-4CB4-BE92-A1D147B36865}']
    function Read(PId: Integer): TBodyDtoArray;
    function ReadAll(PSortColumn: String): TBodyDtoArray;
    function Insert(PId: Integer; PName: String; PBodyCategory: Integer): String;
    function Update(PId: Integer; PName: String; PBodyCategory: Integer): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for aspects. @br
  - Factory: TFactoryAspectsDao @br
  - Fake: FakeAspectsDao @br
  - Created: 2019-01-07 @br
  - Last update: 2019-01-07 }
  IAspectsDao = interface ['{D45F9907-E51A-49DC-B8EE-6225672C1EB4}']
    function Read(PId: Integer): TAspectDtoArray;
    function ReadAll(PSortColumn: String): TAspectDtoArray;
    function Insert(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
    function Update(PId: Integer; PName: String; PAngle: Double; PAspectCategory: Integer): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for configurations. @br
  - Factory: TFactoryConfigurationsDao @br
  - Fake: FakeConfigurationsDao @br
  - Created: 2019-01-09 @br
  - Last update: 2019-01-11 }
  IConfigurationsDao = interface ['{4480C728-C4BC-49F3-93E0-E97C452687DA}']
    function Read(PId: Integer): TConfigurationDtoArray;
    function ReadAll(PSortColumn: String): TConfigurationDtoArray;
    function Insert(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
        PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    function Update(PId: Integer; PName, PDescription: String; PBaseOrbAspects, PBaseOrbMidpoints: Double;
        PCoordinateSystem, PAyanamsha, PHouseSystem, PObserverPosition, PProjectionType: Integer): String;
    function Delete(PId: Integer): String;
  end;

  { DAO for GlyphsBodies. @br
  This data changes only once per new release and will not be deleted, therefore only read and insert is supported.
  - Factory: TFactoryGlyphsBodiesDao @br
  - Fake: FakeGlyphsBodiesDao @br
  - Created: 2019-02-08 @br
  - Last update: 2019-02-10 }
  IGlyphsBodiesDao = interface ['{A6A12FFB-F1D0-4200-A1DB-689A501DDD8D}']
    function Read(PBodyId: Integer): TGlyphsBodiesDtoArray;
    function ReadAll(PSortColumn: String): TGlyphsBodiesDtoArray;
    function Insert(PBodyId: Integer; PGlyph: String): String;
  end;

  { DAO for GlyphsConfiguration: Glyphs for items in a specific configuration. @br
  This data changes only once per new release and will not be deleted, therefore only read and insert is supported.
  - Factory: TGlyphsConfigurationDao @br
  - Fake: FakeGlyphsConfigurationDao @br
  - Created: 2019-02-11 @br
  - Last update: 2019-02-13 }
  IGlyphsConfigurationDao = interface ['{85CF7616-E238-4077-BC79-8C8E2EACC02F}']
    function Read(PConfigId, PBodyId: Integer; Category: String): TGlyphsConfigurationDtoArray;
    function ReadAll(PSortColumn: String; Category: String): TGlyphsConfigurationDtoArray;
    function Insert(PConfigId, PBodyId: Integer; PCategory, PGlyph: String): String;
  end;

  { Factory for DaoSelectHelper. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryDaoSelectHelper = class
    strict private
      DatabaseInstance: IDatabaseInstance;
    public
      function GetInstance: IDaoSelectHelper;
  end;

  { Factory for DMLCudHelper. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryDaoDMLCudHelper = class
    strict private
      DatabaseInstance: IDatabaseInstance;
    public
      function GetInstance: IDaoDMLCudHelper;
  end;

  { Factory for LookUpValueDao. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-21  }
  TFactoryLookUpValueDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
      TableName: String;
    public
      Constructor create(PTableName: String);
      function GetInstance: ILookUpValueDao;
  end;

  { Factory for VersionsDao. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryVersionsDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
      TableName: String;
    public
      function GetInstance(PTableName: String): IVersionsDao;
  end;

  { Factory for HouseSystems. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryHouseSystemsDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
    public
      function GetInstance: IHouseSystemsDao;
  end;

  { Factory for Ayanamshas. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryAyanamshasDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
    public
      function GetInstance: IAyanamshasDao;
  end;

  { Factory for Bodies. @br
  - Created: 2019-01-06 @br
  - Last update: 2019-01-06  }
  TFactoryBodiesDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
    public
      function GetInstance: IBodiesDao;
  end;

  { Factory for Aspects. @br
  - Created: 2019-01-07 @br
  - Last update: 2019-01-07  }
  TFactoryAspectsDao  = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
    public
      function GetInstance: IAspectsDao;
  end;

  { Factory for Configurations. @br
  - Created: 2019-01-11 @br
  - Last update: 2019-01-11  }
  TFactoryConfigurationsDao = class
    strict private
      DaoselectHelper: IDaoselectHelper;
      DaoDMLCudHelper: IDaoDMLCudHelper;
    public
      function GetInstance: IConfigurationsDao;
  end;

  { Factory for GlyphsBodies (supported combinations for glyphs and bodies). @br
  - Created: 2019-02-10 @br
  - Last update: 2019-02-10  }
  TFactoryGlyphsBodiesDao = class
  strict private
    DaoselectHelper: IDaoselectHelper;
    DaoDMLCudHelper: IDaoDMLCudHelper;
  public
    function GetInstance: IGlyphsBodiesDao;
  end;

  { Factory for GlyphsConfiguration (selected combinations for glyphs and items in a specific configuration). @br
  - Created: 2019-02-13 @br
  - Last update: 2019-02-13  }
  TFactoryGlyphsConfigurationDao = class
  strict private
    DaoselectHelper: IDaoselectHelper;
    DaoDMLCudHelper: IDaoDMLCudHelper;
  public
    function GetInstance: IGlyphsConfigurationDao;
  end;

implementation
   uses
     DaoImpl;

  { TFactoryDaoSelectHelper }
  function TFactoryDaoSelectHelper.GetInstance: IDaoSelectHelper;
    begin
      Result:= TDaoSelectHelper.Create(TDatabaseInstance.Create);
    end;

  { TFactoryDaoDMLCudHelper }
  function TFactoryDaoDMLCudHelper.GetInstance: IDaoDMLCudHelper;
    begin
      Result:= TDaoDMLCudHelper.Create(TDatabaseInstance.Create);
    end;

  { TFactoryLookUpValueDao }
  constructor TFactoryLookUpValueDao.Create(PTableName: String);
    begin
      TableName:= PTableName;;
    end;

  function TFactoryLookUpValueDao.GetInstance: ILookUpValueDao;
    begin
      Result:= TLookUpValueDao.Create(TFactoryDaoSelectHelper.Create.GetInstance,
                                      TFactoryDaoDMLCudHelper.Create.Getinstance, TableName);
    end;

  { TFactoryVersionsDao }
  function TFactoryVersionsDao.GetInstance(PTableName: String): IVersionsDao;
    begin
      Result:= TVersionsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance,
                                   TFactoryDaoDMLCudHelper.Create.Getinstance, PTableName);
    end;

  { TFactoryHouseSystemsDao }
  function TFactoryHouseSystemsDao.GetInstance: IHouseSystemsDao;
    begin
      Result:= THouseSystemsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryAyanamshasDao }
  function TFactoryAyanamshasDao.GetInstance: IAyanamshasDao;
    begin
      Result:= TAyanamshasDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryBodiesDao }
  function TFactoryBodiesDao.GetInstance: IBodiesDao;
    begin
      Result:= TBodiesDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryBodiesDao }
  function TFactoryAspectsDao.GetInstance: IAspectsDao;
    begin
      Result:= TAspectsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryConfigurationsDao }
  function TFactoryConfigurationsDao.GetInstance: IConfigurationsDao;
    begin
      Result:= TConfigurationsDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryGlyphsBodiesDao }
  function TFactoryGlyphsBodiesDao.GetInstance: IGlyphsBodiesDao;
    begin
      Result:= TGlyphsBodiesDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;

  { TFactoryGlyphsConfigurationDao }
  function TFactoryGlyphsConfigurationDao.GetInstance: IGlyphsConfigurationDao;
    begin
      Result:= TGlyphsConfigurationDao.Create(TFactoryDaoSelectHelper.Create.GetInstance, TFactoryDaoDMLCudHelper.Create.Getinstance);
    end;



end.

