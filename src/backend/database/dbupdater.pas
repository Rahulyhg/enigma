{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit DbUpdater;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  { Checks and handles updating the database to the current version. Should be called once at start of the program. @br
  - Factory: none @br
  - Fake: none @br
  - Created: 2018 @br
  - Last update: 2018-02-13 }
  IDatabaseUpdater = interface ['{1703951E-EBD7-4C10-85D3-4C20EFEDD86C}']
    { Checks if database exists, creates one if not. performs ddl and dml to update database.}
    procedure CreateDbVersion;
  end;

implementation

end.

