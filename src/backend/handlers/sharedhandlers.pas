{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit SharedHandlers;
{< Handlers that are shareable between the different functional parts of Enigma. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SharedXchgDomain, SharedXchgDomainImpl, SeFrontend;

type

{ Handles sexagesimal input and converts it to a IValidatedDouble. @br
Expects a lower and upper limit during construction, respectively MinDHValue and MaxDHValue, which contain the
limits for Degrees or Hours. The upper and lower limits of minutes and seconds are automatically checked.@br
If the conversion was valid: Value contains the converted value and Valid is true.@br
If the conversions was invalid: Value contains 0.0 and Valid is false. @br
- Factory: FactorySexagesimalValidationHandler @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-23 }
ISexagesimalValidationHandler = interface ['{1EAFED0E-D2FE-4A2D-940B-67235684AFD7}']
  function CalculateValue(DHText, MinutesText, SecondsText: String; NegativeSign: Boolean): IValidatedDouble;
end;

{ Handles input for a calendar date and converts it to a IValidatedDate.@br
The calendar should be 'g' for Gregorian and 'j' for Julian.@br
If the conversion was valid: Value contains the converted value and Valid is true.@br
If the conversions was invalid: Value contains 0.0 and Valid is false. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-24 }
IDateValidationHandler = interface ['{1BFBBE59-9CE9-4A80-B4B2-66DE4EFE4331}']
  function CalculateValue(PYear, PMonth, PDay, PCalendar: String): IValidatedDate;
end;

{ Handles input for clock time and converts it to a IValidatedTime.@br
Time is assumed to be always UT.@br
If the conversion was valid: Value contains the converted value and Valid is true.@br
If the conversions was invalid: Value contains 0.0 and Valid is false. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-24}
ITimeValidationHandler = interface ['{B2F56A1F-1029-4AA0-844C-0135B25C9537}']
  function CalculateValue(PHour, PMinute, PSecond: String): IValidatedTime;
end;

{ Handles input for geographic longitude and converts it to a IValidatedDouble. @br
The Direction should be 'E' for East and 'W' for West.@br
If the conversion was valid: Value contains the converted value and Valid is true.@br
If the conversions was invalid: Value contains 0.0 and Valid is false. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-24}
ILongitudeValidationHandler = interface ['{6E10182C-B090-45E5-BFD4-7204838CEB6D}']
  function CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
end;

{ Handles input for geographic latitude and converts it to a IValidatedDouble.@br
The Direction should be 'N' for North and 'S' for South.@br
If the conversion was valid: Value contains the converted value and Valid is true.@br
If the conversions was invalid: Value contains 0.0 and Valid is false. @br
- Factory: none @br
- Fake: none @br
- Created: 2018 @br
- Last update: 2018-11-24}
ILatitudeValidationHandler = interface ['{3000AB13-3AE0-498D-878B-4B8F737E03CC}']
  function CalculateValue(Degrees, Minutes, Seconds, Direction: String): IValidatedDouble;
end;

{ Factory for SexagesimalValidationHandler. @br
- Created: 2018-11-23 @br
- Last update: 2018-11-23 }
FactorySexagesimalValidationHandler = class
  public
    class function GetInstance(PMinDH, PMaxDH: Integer): ISexagesimalValidationHandler;
end;



implementation
uses SharedHandlersImpl;

{ FactorySexagesimalValidationHandler }
class function FactorySexagesimalValidationHandler.GetInstance(PMinDH, PMaxDH: Integer): ISexagesimalValidationHandler;
begin
  Result:= TSexagesimalValidationHandler.Create(PMinDH, PMaxDH);
end;




end.
