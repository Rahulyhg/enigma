{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit DatabaseHandlers;
{< Handlers for database access.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dto;

//type

  //{ Handles retrieving housesystems.@br
  //  This is standing data, no insert, update or delete actions from the UI.@br
  //- Factory: FactoryHouseSystemDataHandler @br
  //- Fake: FakeHouseSystemDataHandler @br
  //- Created: 201-01-11 @br
  //- Last update: 2019-01-11 }
  //IHouseSystemDataHandler = interface ['{2C04763E-7C8B-492A-BD5C-057EDDC3E6E9}']
  //  function GetHouseSystem(PId: Integer): IHouseSystemDto;
  //  function GetAllHouseSystems: THouseSystemDtoArray;
  //  property AllHouseSystems: THouseSystemDtoArray read GetAllHouseSystems;
  //end;
  //
  //{ Factory for HouseSystemDataHandler. @br
  //- Created: 2019-01-11 @br
  //- Last update: 2019-01-11  }
  //TFactoryHouseSystemDataHandler = class
  //  strict private
  //    Dao: IHouseSystemDto;
  //  public
  //    function GetInstance: IHouseSystemDataHandler;
  //end;


implementation

uses
  DatabaseHandlersImpl, Dao;

  //{ TFactoryHouseSystemDataHandler }
  //function TFactoryHouseSystemDataHandler.GetInstance: IHouseSystemDataHandler;
  //  begin
  //    Result:= THouseSystemDataHandler.Create(TFactoryHouseSystemsDao.Create.GetInstance);
  //  end;



end.

