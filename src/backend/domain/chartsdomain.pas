{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit ChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BeSwissDelphi;

type

{ DTO for horizontal coördinates. Can be used for celestial objects and for house cusps.}
IHorizontalPosition = interface ['{E9C0CB2E-DF7F-4528-81F9-621AB06AC76B}']
  function GetAltitude: Double;
  function GetAzimuth: Double;
  property Altitude: Double read GetAltitude;
  property Azimuth: Double read GetAzimuth;
end;

implementation

end.

