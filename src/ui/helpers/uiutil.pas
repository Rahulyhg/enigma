{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiUtil;
{< Utility classes that support both the UI and graphics. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;


type
  { Calculation functions for the user interface.@br
  - Factory: none @br
  - Fake: none @br
  - Created: 2019-03-10 @br
  - Last update: 2019-03-10 }
  IUiCalculations = Interface ['{1BF0E546-045D-4765-B9B8-858AAB5A1962}']
    { If Pvalue is even, returns PValue. If Pvalue is odd, returns the even value of Pvalue + 1 }
    function MakeEven(PValue: Integer): Integer;
    { Returns a value in the range of 0-360 by adding or subtracting 360 from the original double value.}
    function Range360(PInputValue: Double): Double;
    { Returns a value in the range of 0-360 by adding or subtracting 360 from the original integer value.}
    function Range360(PInputValue: Integer): Integer;
  end;

implementation

end.

