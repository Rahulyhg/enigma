{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit GraphicsHelpersImpl;
{< Implementations for interfaces in unit GraphicsHelpers }
{$mode objfpc}{$H+}

interface

  uses
    Classes, SysUtils, GraphicsHelpers;

  { TODO : Create unit tests for GraphicsHelpers }
  type TRectTriangle = Class(TInterfacedObject, IRectTriangle)
    strict private
      CenterX, CenterY, Hypothenusa: Integer;
      Angle: Double;
      CalculatedPoint: TPoint;
      function DefinePoint: TPoint;
    public
      constructor Create(PCenterX, PCenterY, PHypothenusa: Integer; PAngle: Double);
      function GetXYPoint: TPoint;
  end;



implementation

  uses Math;

  { TRectTriangle }
  constructor TRectTriangle.Create(PCenterX, PCenterY, PHypothenusa: Integer; PAngle: Double);
    begin
      CenterX:= PCenterX;
      CenterY:= PCenterY;
      Hypothenusa:= PHypothenusa;
      Angle:= PAngle;
      CalculatedPoint:= DefinePoint;
    end;

  function TRectTriangle.GetXYPoint: TPoint;
    begin
      Result:= CalculatedPoint;
    end;

  function TRectTriangle.DefinePoint: TPoint;
    var
      XSize, YSize: LongInt;
    begin
      YSize:= CenterY - Floor(Sin(DegToRad(Angle)) * Hypothenusa);
      XSize:= CenterX - Floor(Cos(DegToRad(Angle)) * Hypothenusa);
      Result:= TPoint.Create(XSize, YSize);
    end;


end.


