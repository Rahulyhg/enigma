{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit GraphicsHelpers;
{< Interfaces for classes that support using graphics. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  { Rectangular triangle.@br
    Used to calculate a position based on simple goniometric relation in a rectangular triangle.@br
  - Factory: none @br
  - Fake: none @br
  - Created: 2019-02-24 @br
  - Last update: 2019-02-24 }
  IRectTriangle = Interface ['{09071611-6ED8-437A-A395-7E0374C487FE}']
    { X and Y coordinates of calculated point }
    function GetXYPoint: TPoint;
    property XYPoint:Tpoint read GetXYPoint;
  end;



implementation

end.

