{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit FormattedPositionsImpl;
{< Implementations for interfaces in FormattedPositions.}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FormattedPositions;

type
  TDegreeMinuteSign = class(TInterfacedObject, IDegreeMinuteSign)
    strict private
      Position: Double;
      Degrees, Minutes, Sign: String;
      PositionInDegrees: Double;
      DegreesNr, DegreesNrWithSigns, MinutesNr, SignIndex: Integer;

      procedure DefineTheParts;
    public
      constructor Create(PPosition: Double);
      function GetDegrees: String;
      function GetMinutes: String;
      function GetDegreesMinutes: String;
      function GetSign: String;
  end;

implementation

const
  ERROR = '-Error-';
  SPACE = ' ';
  ZERO = '0';
  DEGREES_IN_SIGN = 30;
  MINUTES_IN_DEGREE = 60;
  SECONDS_IN_MINUTE = 60;
  DEGREE_SIGN = '°';     { TODO : Replace degreesign with unicode U+00B0 }
  MINUTE_SIGN = '''';
  SECOND_SIGN = '"';

  { TDegreeMinuteSign }

    constructor TDegreeMinuteSign.Create(PPosition: Double);
      begin
        Position:= PPosition;
        DefineTheParts;
      end;

    function TDegreeMinuteSign.GetDegrees: String;
      begin
        Result:= Degrees;
      end;

    function TDegreeMinuteSign.GetMinutes: String;
      begin
        Result:= Minutes;
      end;

    function TDegreeMinuteSign.GetDegreesMinutes: String;
      begin
        Result:= Degrees + Minutes;
      end;

    function TDegreeMinuteSign.GetSign: String;
     begin
       Result:= Sign;
     end;

    procedure TDegreeMinuteSign.DefineTheParts;
    var
      TempAmount: Double;
    begin
      DegreesNr:= Trunc(Position);
      TempAmount:= Frac(Position) * MINUTES_IN_DEGREE;
      MinutesNr:= Trunc(TempAmount);
      SignIndex:= Trunc(DegreesNr DIV DEGREES_IN_SIGN) + 1;
      DegreesNrWithSigns:= DegreesNr - ((SignIndex - 1) * DEGREES_IN_SIGN);
      Degrees:= IntToStr(DegreesNrWithSigns) + DEGREE_SIGN;
      Minutes:= IntToStr(MinutesNr) + MINUTE_SIGN;
      Sign:= 'x';   { TODO : Define real value for glyph }
    end;

end.

