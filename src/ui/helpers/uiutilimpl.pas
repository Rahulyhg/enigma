{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiUtilImpl;
{< Implementations for interfaces in UiUtil. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiUtil;

type

  TUiCalculations = class(TInterfacedObject, IUiCalculations)
    function MakeEven(PValue: Integer): Integer;
    function Range360(PInputValue: Double): Double;
    function Range360(PInputValue: Integer): Integer;
  end;

implementation

{ TUiCalculations }
  function TUiCalculations.MakeEven(PValue: Integer): Integer;
    begin
      Result:= ((PValue + 1) DIV 2) * 2;
    end;

  function TUiCalculations.Range360(PInputValue: Double): Double;
    var
      TempValue: Real;
    begin
      TempValue:= PInputValue;
      while TempValue > 0.0 do begin
        TempValue:= TempValue - 360.0;
      end;
      while TempValue < 0.0 do begin
        TempValue:= TempValue + 360.0;
      end;
      Result:= TempValue;
    end;

  function TUiCalculations.Range360(PInputValue: Integer): Integer;
    var
      TempValue: Integer;
    begin
      TempValue:= PInputValue;
      while TempValue > 0 do begin
        TempValue:= TempValue - 360;
      end;
      while TempValue < 0 do begin
        TempValue:= TempValue + 360;
      end;
      Result:= TempValue;
    end;

end.

