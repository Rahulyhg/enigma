{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit FormattedPositions;
{< Classes that format several types of output.}
{$mode objfpc}{$H+}

{ TODO : Create interfaces and move implementations to FormattedPositionsImpl }

interface

uses
  Classes, SysUtils;

const
  ERROR = '-Error-';
  SPACE = ' ';
  ZERO = '0';
  DEGREES_IN_SIGN = 30;
  MINUTES_IN_DEGREE = 60;
  SECONDS_IN_MINUTE = 60;
  DEGREE_SIGN = '°';     { TODO : Replace degreesign with unicode U+00B0 }
  MINUTE_SIGN = '''';
  SECOND_SIGN = '"';

type

{ Position text splitted in degrees, minutes and sign.@br
  Used for showing positions in a graphic chart..@br
- Factory: none @br
- Fake: none @br
- Created: 2019-03-11 @br
- Last update: 2019-03-11 }
IDegreeMinuteSign = Interface ['{99F7D15C-6A3F-4F66-97B3-039D55B5F2C8}']
  { Degrees, no leading zero added. Followed by degree sign.}
  function GetDegrees: String;
  { Minutes, no leading zero added. Followed by minute sign.}
  function GetMinutes: String;
  { Get degree + degree sign plus minute and minute sign}
  function GetDegreesMinutes: String;
  { Character for glyph in font EnigmaAstrology.}
  function GetSign: String;
  property Degrees: String read GetDegrees;
  property Minutes: String read GetMinutes;
  property DegreesMinutes: String read GetDegreesMinutes;
  property Sign: String read GetSign;
 end;

  { Formats to sexagesimal (degrees, minutes, seconds) positions.
  Optionally handles signs but uses for now only abbreviations to indicate the signs. }
  TSexagesimalPositions = class
    strict private
      Signs: array[1..12] of String;
      PositionInDegrees: Double;
      Degrees, DegreesWithSigns, Minutes, Seconds, SignIndex: Integer;
      procedure DefineElementsForPosition;
      procedure DefineSigns;
      function CreateStringForDegrees(UseSigns: Boolean): String;
      function CreateStringForSigns: String;
    public
      constructor Create(DecimalDegrees: Double);
      function SexagesimalDegrees: String;
      function SexagesimalSigns: String;
  end;


implementation
{ SexagesimalPositions }

constructor TSexagesimalPositions.Create(DecimalDegrees: Double);
begin
  PositionInDegrees:= DecimalDegrees;
  DefineSigns;
  DefineElementsForPosition;
end;

procedure TSexagesimalPositions.DefineSigns;
begin
  Signs[1]:='AR';
  Signs[2]:='TA';
  Signs[3]:='GE';
  Signs[4]:='CN';
  Signs[5]:='LE';
  Signs[6]:='VI';
  Signs[7]:='LI';
  Signs[8]:='SC';
  Signs[9]:='SA';
  Signs[10]:='CP';
  Signs[11]:='AQ';
  Signs[12]:='PI';
end;

procedure TSexagesimalPositions.DefineElementsForPosition;
var
  TempAmount: Double;
begin
  Degrees:= Trunc(PositionInDegrees);
  TempAmount:= Frac(PositionInDegrees) * MINUTES_IN_DEGREE;
  Minutes:= Trunc(TempAmount);
  Seconds:= Trunc(frac(TempAmount) * SECONDS_IN_MINUTE);
  SignIndex:= Trunc(Degrees DIV DEGREES_IN_SIGN) + 1;
  DegreesWithSigns:= Degrees - ((SignIndex - 1) * DEGREES_IN_SIGN);
end;

function TSexagesimalPositions.CreateStringForDegrees(UseSigns: Boolean): String;
var
  Deg, Min, Sec, FormattedText: String;
  CurrentDegrees: Integer;
begin
  if (UseSigns) then CurrentDegrees := DegreesWithSigns
  else CurrentDegrees := Degrees;
  Deg := IntToStr(CurrentDegrees);
  Min := IntToStr(Minutes);
  Sec := IntToStr(Seconds);
  if ((CurrentDegrees < 100) and (UseSigns = false)) then Deg:= Concat(SPACE, Deg);
  if (CurrentDegrees < 10) then Deg:= Concat(SPACE, Deg);
  if (Minutes < 10) then Min:= Concat(ZERO, Min);
  if (Seconds < 10) then Sec:= Concat(ZERO, Sec);
  FormattedText:= Concat(Deg, DEGREE_SIGN, Min, MINUTE_SIGN, Sec, SECOND_SIGN);
  Result:= FormattedText;
end;

function TSexagesimalPositions.CreateStringForSigns: String;
var
  FormattedText: String;
begin
  FormattedText:= CreateStringForDegrees(true);
  Result:= Concat(FormattedText, SPACE, Signs[SignIndex]);
end;

function TSexagesimalPositions.SexagesimalDegrees: String;
begin
  Result:= CreateStringForDegrees(false);
end;

function TSexagesimalPositions.SexagesimalSigns: String;
begin
  Result:= CreateStringForSigns;
end;

end.

