{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiDomainImpl;
{< Implementations for interfaces in unit UiDomain }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UiDomain;
type
  { TODO : Create unit tests for UiDomain }
  TGraphicBodyPosition = class(TInterfacedObject, IGraphicBodyPosition)
    strict private
      BodyIndex: Integer;
      EclipticPosition, MundanePosition, PlotPosition: Double;
    public
      constructor Create(PBodyIndex: Integer; PEclipticPosition, PMundanePosition: Double);
      function GetBodyIndex: Integer;
      function GetEclipticPosition: Double;
      function GetMundanePosition: Double;
      procedure SetMundanePosition(PMundanePosition: Double);
      procedure SetPlotPosition(PPlotPosition: Double);
      function GetPlotPosition: Double;
  end;

  TSortedGraphicBodyPositions = class(TInterfacedObject, ISortedGraphicBodyPositions)
    strict private
      MinDistance: Double;
      AllPositions: TArrayGraphicBodyPositions;
      procedure SortOnPlotPosition;
      procedure FixOverlaps;
    public
      constructor Create(PArrayGraphicBodyPositions: TArrayGraphicBodyPositions; PMinDistance: Double);
      function GetPositions: TArrayGraphicBodyPositions;
  end;

implementation

  { TGraphicBodyPosition }
    constructor TGraphicBodyPosition.Create(PBodyIndex: Integer; PEclipticPosition, PMundanePosition: Double);
      begin
        BodyIndex:= PBodyIndex;
        EclipticPosition:= PEclipticPosition;
        MundanePosition:= PMundanePosition;
        PlotPosition:= MundanePosition;
      end;

    function TGraphicBodyPosition.GetBodyIndex: Integer;
      begin
        Result:= BodyIndex;
      end;

    function TGraphicBodyPosition.GetEclipticPosition: Double;
      begin
        Result:= EclipticPosition;
      end;

    procedure TGraphicBodyPosition.SetPlotPosition(PPlotPosition: Double);
      begin
        PlotPosition:= PPlotPosition;
      end;

    function TGraphicBodyPosition.GetMundanePosition: Double;
      begin
        Result:= MundanePosition;
      end;

    procedure TGraphicBodyPosition.SetMundanePosition(PMundanePosition: Double);
      begin
        MundanePosition:= PMundanePosition;
      end;

    function TGraphicBodyPosition.GetPlotPosition: Double;
      begin
        Result:= Plotposition;
      end;

 { TSortedGraphicBodyPositions }
     constructor TSortedGraphicBodyPositions.Create(PArrayGraphicBodyPositions: TArrayGraphicBodyPositions; PMinDistance: Double);
       begin
         MinDistance:= PMinDistance;
         AllPositions := PArrayGraphicBodyPositions;
         SortOnPlotPosition;
         FixOverlaps;
       end;

     function TSortedGraphicBodyPositions.GetPositions: TArrayGraphicBodyPositions;
       begin
         Result:= AllPositions;
       end;

     procedure TSortedGraphicBodyPositions.SortOnPlotPosition;
       var
         i, j, size: Integer;
         tmp: IGraphicBodyPosition;
       begin
         size:= Length(AllPositions);
         for i := size - 1 downto 0 do begin
           for j := 0 to i-1 do begin
             if (AllPositions[j].PlotPosition > AllPositions[j+1].PlotPosition) then
               begin
                 tmp := AllPositions[j];
                 AllPositions[j] := AllPositions[j+1];
                 AllPositions[j+1] := tmp;
               end;
           end;
         end;
       end;

     procedure TSortedGraphicBodyPositions.FixOverlaps;  { TODO : check for overlap at end of ecliptic }
       var
         i, size: Integer;
         pos1, pos2: IGraphicBodyPosition;
         plotPos1, plotPos2: Double;
       begin
         size:= Length(AllPositions);
         for i := 0 to size-2 do begin
           pos1:= AllPositions[i];
           pos2:= AllPositions[i+1];
           plotPos1:= pos1.PlotPosition;
           plotPos2:= pos2.PlotPosition;
           if ((plotPos2 - plotPos1 ) < MinDistance) then begin
             AllPositions[i+1].PlotPosition:= AllPositions[i].PlotPosition + MinDistance;
           end;
         end;
       end;

end.

