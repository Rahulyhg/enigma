{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiDomain;


{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { Position for drawing a glyph of a body on the chart. @br
    Eclipticpositions is the standard longitude. @br
    MundanePosition is the angle from the ascendant (counterclockwise). @br
    Plotposition is the angle used for drawing the glyph.
    Often this is the same as the mundaneposition but it could change if several celestial objects are near each other. @br
  - Factory: none @br
  - Fake: none. @br
  - Created: 2019-03-03 @br
  - Last update: 2019-03-09 }
  IGraphicBodyPosition = Interface ['{00936E5A-EAA8-4F5D-AF02-9028ADD9148F}']
    function GetBodyIndex: Integer;
    function GetEclipticPosition: Double;
    function GetMundanePosition: Double;
    procedure SetMundanePosition(PMundanePosition: Double);
    function GetPlotPosition: Double;
    procedure SetPlotPosition(PPlotPosition: Double);
    property BodyIndex: Integer read GetBodyIndex;
    property EclipticPosition: Double read GetEclipticPosition;
    property MundanePosition: Double read GetMundanePosition write SetMundanePosition;
    property PlotPosition: Double read GetPlotPosition write SetPlotPosition;
  end;

  { Array for BodyPositions, to be used in drawing a chart. }
  TArrayGraphicBodyPositions = Array of IGraphicBodyPosition;

  { All positions of bodies to be drawn on the chart. @br
    The positions are sorted (ascending) on PlotPosition and possible overlaps are corrected. @br
    If positions are tooc klose the highest position is moved upwards (higher value for plotposition).  @br
  - Factory: none @br
  - Fake: none. @br
  - Created: 2019-03-09 @br
  - Last update: 2019-03-09 }
  ISortedGraphicBodyPositions = Interface ['{F9422E93-AA35-40A8-9F75-C7611D55FCB9}']
    function GetPositions: TArrayGraphicBodyPositions;
  end;


  { Parent for presentable values }   { TODO : Create interface and move class to UiDomainImpl }
  TPresentableValue = class
  private
    Text: String;
    function ConvertInput(InputValue: Double): String; virtual; abstract;
  public
      Constructor Create(PInputValue: Double);
      function GetText: String;
  end;


  { Array of presentable values,
  for ecliptical values: 0..4 is longitude, latitude, longitude speed, latitude speed;
  for equatorial values: 0..4 is right ascension, declination, right ascension speed, declination speed
  for distance values: 0..1 is radius vector, radv speed
  for horizontal values: 0.11 is azimuth, altitude }
  TPresentableValueArray = Array of TPresentableValue;



  { Presentable instance for a decimal value, using decimal presentation and 8 positions for the fraction.
    If necessary the fraction is postfixed with zero's to arrive at 8 positions. }
  TDecimalValue = class(TPresentableValue)           { TODO : Create interface and move class to UiDomainImpl }
    private
      function ConvertInput(InputValue: Double): String; override;
  end;

  { Presentable instance for a sexagesimal value, using degrees, minutes and seconds but no fraction.
    If necessary the minutes and seconds are prefixed with zero's to arrive at 2 positions. }
  TSexagesimalValue = class(TPresentableValue)                             { TODO : Create interface and move class to UiDomainImpl }
    private
      function ConvertInput(InputValue: Double): String; override;
  end;

 { Presentable instance for a sexagesimal value, using signs, degrees, minutes and seconds but no fraction.@br
   If necessary the minutes and seconds are prefixed with zero's to arrive at 2 positions. @br
   - Created: 2018 @br
   - Last update: 2019-02-15  }
  TSignDMSValue = class(TPresentableValue)                { TODO : Create interface and move class to UiDomainImpl }
  private
    Signs: array[1..12] of String;                        { TODO : check if the parent TPresentableValue can be removed }
    Glyphs: array[1..12] of String;
    SignIndex: Integer;
    procedure DefineSigns;
    procedure DefineGlyphs;
    function ConvertInput(InputValue: Double): String; override;
  public
    function GetGlyph:String;
    function GetAbbreviation: String;
  end;

  { Container for all presentablevalues for a celestial body }
  TFullCelestialObjectValues = class                                    { TODO : Create interface and move class to UiDomainImpl }
      strict private
        EclipticValues: TPresentableValueArray;
        EquatorialValues: TPresentableValueArray;
        DistanceValues: TPresentableValueArray;
        HorizontalValues: TPresentableValueArray;
      public
        Constructor Create(PEclipticValues, PEquatorialValues, PDistanceValues,
                           PHorizontalValues: TPresentableValueArray);
        function GetEclipticalValues: TPresentableValueArray;
        function GetEquatorialValues: TPresentableValueArray;
        function GetDistanceValues: TPresentableValueArray;
        function GetHorizontalValues: TPresentableValueArray;
  end;

    { Contains an array with TFullCelestialObjectValues that descripe an object for each item in the array }
  TAllPresentableValues = Array of TFullCelestialObjectValues;


  { Container for all presentableValues for houses. }
  TFullHouseValues = class                                      { TODO : Create interface and move class to UiDomainImpl }
    strict private
      Cusps: TPresentableValueArray;
      Specials: TPresentableValueArray;
    public
      Constructor Create(PCusps, PSpecials: TPresentableValueArray);
      function GetCusps: TPresentableValueArray;
      function GetSpecials: TPresentableValueArray;
  end;

  { Container for presentable metainformation about chart (inputted data). }
  TMetaInfoValues = class                                 { TODO : Create interface and move class to UiDomainImpl }
     strict private
       Name, Location, DateTime, Settings: String;
     public
       Constructor Create(PName, PLocation, PDateTime, PSettings: String);
       function GetName: String;
       function GetLocation: String;
       function GetDateTime: String;
       function GetSettings: String;
  end;

  { Container with all presentation data for a full chart. }
  TFullChartValues = class                                       { TODO : Create interface and move class to UiDomainImpl }
    strict private
      FullHouseValues: TFullHouseValues;
      FullCelestialObjectValues: TFullCelestialObjectValues;
      MetaInfoValues: TMetaInfoValues;
    public
      Constructor Create(PFullHouseValues: TFullHouseValues;
                         PFullCelestialObjectValues: TFullCelestialObjectValues;
                         PMetaInfoValues: TMetaInfoValues);
      function GetFullHouseValues: TFullHouseValues;
      function GetFullCelestialObjectValues: TFullCelestialObjectValues;
      function GetMetaINfoValues: TMetaInfoValues;
  end;


implementation

const
  DECIMAL_DOT = '.';
  ERROR = '-Error-';
  SPACE = ' ';
  ZERO = '0';
  DEGREES_IN_SIGN = 30;
  MINUTES_IN_DEGREE = 60;
  SECONDS_IN_MINUTE = 60;
  DEGREE_SIGN = '°';     { TODO : Replace degreesign with unicode U+00B0 }
  MINUTE_SIGN = '''';
  SECOND_SIGN = '"';

{ TPresentableValue }

Constructor TPresentableValue.Create(PInputValue: Double);
begin
  Text:= ConvertInput(PInputValue);
end;

function TPresentableValue.GetText: String;
begin
  Result:= Text;
end;

{ TDecimalValue }
function TDecimalValue.ConvertInput(InputValue: Double): String;
begin
   Result:= Format('%3.8f',[InputValue]);
end;



{ TSexagesimalValue }

function TSexagesimalValue.ConvertInput(InputValue: Double): String;
var
  DegTxt, MinTxt, SecTxt: String;
  Degrees, Minutes, Seconds: Integer;
  TempAmount, AbsInputValue: Double;
  IsNegative: Boolean;
begin
  IsNegative:= (InputValue < 0.0);
  AbsInputValue:= Abs(InputValue);
  Degrees:= Trunc(AbsInputValue);
  TempAmount:= Frac(AbsInputValue) * MINUTES_IN_DEGREE;
  Minutes:= Trunc(TempAmount);
  Seconds:= Trunc(frac(TempAmount) * SECONDS_IN_MINUTE);
  DegTxt := Format('%3d',[Degrees]);
  MinTxt := Format('%2d',[Minutes]);
  SecTxt := Format('%2d',[Seconds]);
  if IsNegative then DegTxt:= '-' + TrimLeft(DegTxt);
  Result:= Concat(DegTxt, DEGREE_SIGN, MinTxt, MINUTE_SIGN, SecTxt, SECOND_SIGN);
end;


  { TSignDMSValue }

  function TSignDMSValue.GetGlyph:String;
    begin
      Result:= Glyphs[SignIndex];
    end;

  function TSignDMSValue.GetAbbreviation:String;
    begin
      Result:= Signs[SignIndex];
    end;

  procedure TSignDMSValue.DefineSigns;
    begin
      Signs[1]:='AR';
      Signs[2]:='TA';
      Signs[3]:='GE';
      Signs[4]:='CN';
      Signs[5]:='LE';
      Signs[6]:='VI';
      Signs[7]:='LI';
      Signs[8]:='SC';
      Signs[9]:='SA';
      Signs[10]:='CP';
      Signs[11]:='AQ';
      Signs[12]:='PI';
    end;

  procedure TSignDMSValue.DefineGlyphs;        { TODO : R_0.6 Use configuration to define glyphs }
    begin
      Glyphs[1]:= '1';
      Glyphs[2]:= '2';
      Glyphs[3]:= '3';
      Glyphs[4]:= '4';
      Glyphs[5]:= '5';
      Glyphs[6]:= '6';
      Glyphs[7]:= '7';
      Glyphs[8]:= '8';
      Glyphs[9]:= '9';
      Glyphs[10]:= '0';
      Glyphs[11]:= '-';
      Glyphs[12]:= '=';
    end;

  function TSignDMSValue.ConvertInput(InputValue: Double): String;
  var
    Degrees: Integer;
    DecDegreesWithoutSign: Double;
    SexagValue: TSexagesimalValue;
  begin
    DefineSigns;
    DefineGlyphs;
    Degrees:= Trunc(InputValue);
    SignIndex:= Trunc(Degrees DIV DEGREES_IN_SIGN) + 1;
    DecDegreesWithoutSign:= InputValue - ((SignIndex - 1) * DEGREES_IN_SIGN);
    SexagValue:= TSexagesimalValue.Create(DecDegreesWithoutSign);
    Result:= SexagValue.GetText;
  end;

{ TFullCelestialObjectValues }

Constructor TFullCelestialObjectValues.Create(PEclipticValues, PEquatorialValues, PDistanceValues,
                                            PHorizontalValues: TPresentableValueArray);
begin
  EclipticValues:= PEclipticValues;
  EquatorialValues:= PEquatorialValues;
  DistanceValues:= PDistanceValues;
  HorizontalValues:= PHorizontalValues;
end;

function TFullCelestialObjectValues.GetEclipticalValues: TPresentableValueArray;
begin
  Result:= EclipticValues;
end;

function TFullCelestialObjectValues.GetEquatorialValues: TPresentableValueArray;
begin
  Result:= EquatorialValues;
end;

function TFullCelestialObjectValues.GetDistanceValues: TPresentableValueArray;
begin
  Result:= DistanceValues;
end;

function TFullCelestialObjectValues.GetHorizontalValues: TPresentableValueArray;
begin
  Result:= HorizontalValues;
end;

{ TFullHouseValues }
Constructor TFullHouseValues.Create(PCusps, PSpecials: TPresentableValueArray);
begin
  Cusps:= PCusps;
  Specials:= PSpecials;
end;

function TFullHouseValues.GetCusps: TPresentableValueArray;
begin
  Result:= Cusps;
end;

function TFullHouseValues.GetSpecials: TPresentableValueArray;
begin
  Result:= Specials;
end;

{  TMetaInfoValues }

Constructor TMetaInfoValues.Create(PName, PLocation, PDateTime, PSettings: String);
begin
  Name := PName;
  Location:= PLocation;
  DateTime:= PDateTime;
  Settings:= PSettings;
end;

function TMetaInfoValues.GetName: String;
begin
  Result:= Name;
end;

function TMetaInfoValues.GetLocation: String;
begin
  Result:= Location;
end;

function TMetaInfoValues.GetDateTime: String;
begin
  Result:= DateTime;
end;

function TMetaInfoValues.GetSettings: String;
begin
  Result:= Settings;
end;

{ TFullChartValues }

Constructor TFullChartValues.Create(PFullHouseValues: TFullHouseValues;
                                    PFullCelestialObjectValues: TFullCelestialObjectValues;
                                    PMetaInfoValues: TMetaInfoValues);
begin
   FullHouseValues:= PFullHouseValues;
   FullCelestialObjectValues:= PFullCelestialObjectValues;
   MetaInfoValues:= PMetaInfoValues;
end;


function TFullChartValues.GetFullHouseValues: TFullHouseValues;
begin
   Result:= FullHouseValues;
end;

function TFullChartValues.GetFullCelestialObjectValues: TFullCelestialObjectValues;
begin
   Result:= FullCelestialObjectValues;
end;

function TFullChartValues.GetMetaInfoValues: TMetaInfoValues;
begin
  Result:= MetaInfoValues;
end;


end.


