unit UiSharedDashboard;
{< First Form that Enigma shows to a user.
Contains button with which the user starts the different parts of the application. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLTranslator, ExtCtrls, Menus, UiChartsMain, UiChartsInput;

type

  { Dashboard that is shown after starting the program. It presents a choice to select one of the main functionalities
    of Enigma.@br
    Created: 2018 @br
    Last change: 2019-03-22 @br }
  TFormDashboard = class(TForm)
    Btn_Charts: TButton;
    Btn_Periods: TButton;
    Btn_Statistics: TButton;
    Btn_Exit: TButton;
    Lbl_Description: TLabel;
    Lbl_Archaeo: TButton;
    Img_Ziggurath: TImage;
    Lbl_SubTitle: TLabel;
    Lbl_Title: TLabel;
    MenuDashboard: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    procedure Btn_ChartsClick(Sender: TObject);


  private

  public

  end;

var
  FormDashboard: TFormDashboard;

implementation

{$R *.lfm}

{ TFormDashboard }


procedure TFormDashboard.Btn_ChartsClick(Sender: TObject);
begin
  FormChartsMain.Show;
end;





end.

