
{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit ChartMetricsImpl;
{< Implementations for ChartMetrics.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, ChartMetrics;

type
  TChartDrawing2DMetrics = class(TInterfacedObject, IChartDrawing2DMetrics)
    strict private
      CurrentSize: Longint;
      Margin, HorizontalMargin, VerticalMargin: Integer;
      CenterX, CenterY, GlyphXOffset, GlyphYOffset: Integer;
      FontSizeGlyphs, FontSizeText: Integer;
      InitialWidth, InitialHeight, CurrentWidth, CurrentHeight: Integer;
      PercAspectsCircle, PercHousesCircle, PercDegreesCircle, PercSignsCircle, PercZodiacCircle, PercPlanetsCircle,
        PercConnectLinesCircle, PercPositionBodyCircle, PercPositionHouseCircle: Integer;
      AspectCircleRadius, HousesCircleRadius, DegreesCircleRadius, SignsCircleRadius, ZodiacCircleRadius,
        PlanetsCircleRadius, ConnectLinesCircleRadius, PositionBodyCircleRadius, PositionHouseCircleRadius: Integer;
      CurrentPercentage: Double;
      procedure DefineAdditionalSizes;
      procedure CreateCurrentCircleSizes;
      procedure CreateCurrentFontSizes;
      function MakeEven(PValue: Integer): Integer;     { TODO : Use MakeEven from UiUtil }

    public
      constructor Create(PInitialWidth, PInitialHeight, PPercZodiacCircle, PPercHousesCircle,
                         PPercDegreesCircle, PPercAspectsCircle, PPercSignsCircle, PPercPlanetsCircle,
                         PPercConnectLinesCircle, PPercPositionBodyCircle, PPercPositionHouseCircle : Integer);
      procedure Resize(PNewWidth, PNewHeight: Integer);
      function GetAspectCircleRadius: Integer;
      function GetHousesCircleRadius: Integer;
      function GetDegreesCircleRadius: Integer;
      function GetSignsCircleRadius: Integer;
      function GetZodiacCircleRadius: Integer;
      function GetPlanetsCircleRadius: Integer;
      function GetConnectLinesCircleRadius: Integer;
      function GetPositionBodyCircleRadius: Integer;
      function GetPositionHouseCircleRadius: Integer;
      function GetCenterX: Integer;
      function GetCenterY: Integer;
      function GetGlyphXOffset: Integer;
      function GetGlyphYOffset: Integer;
      function GetFontSizeGlyphs: Integer;
      function GetFontSizeText: Integer;
  end;



implementation

  uses Math;

  { TChartDrawing2DMetrics }
    constructor TChartDrawing2DMetrics.Create(PInitialWidth, PInitialHeight, PPercZodiacCircle, PPercHousesCircle,
                                              PPercDegreesCircle, PPercAspectsCircle, PPercSignsCircle,
                                              PPercPlanetsCircle, PPercConnectLinesCircle, PPercPositionBodyCircle,
                                              PPercPositionHouseCircle: Integer);
      begin
        InitialWidth:= PInitialWidth;
        CurrentWidth:= InitialWidth;
        InitialHeight:= PInitialHeight;
        CurrentHeight:= InitialHeight;
        CurrentSize:= Min(CurrentWidth, CurrentHeight);
        CurrentPercentage:= 100;
        PercZodiacCircle:= PPercZodiacCircle;           { TODO : Check possibility of combinations of circle sizes }
        PercHousesCircle:= PPercHousesCircle;
        PercDegreesCircle:= PPercDegreesCircle;
        PercAspectsCircle:= PPercAspectsCircle;
        PercSignsCircle:= PPercSignsCircle;
        PercPlanetsCircle:= PPercPlanetsCircle;
        PercConnectLinesCircle:= PPercConnectLinesCircle;
        PercPositionBodyCircle:= PPercPositionBodyCircle;
        PercPositionHouseCircle:= PPercPositionHouseCircle;
        DefineAdditionalSizes;
        CreateCurrentCircleSizes;
        CreateCurrentFontSizes;
      end;

    procedure TChartDrawing2DMetrics.Resize(PNewWidth, PNewHeight: Integer);
      begin
        CurrentWidth:= PNewWidth;
        CurrentHeight:= PNewHeight;
        DefineAdditionalSizes;
        CreateCurrentCircleSizes;
        CreateCurrentFontSizes;
      end;

    procedure TChartDrawing2DMetrics.DefineAdditionalSizes;
      begin
        CenterX:= MakeEven(CurrentWidth div 2);
        CenterY:= MakeEven(CurrentHeight div 2);
        CurrentSize:= Min(CurrentWidth, CurrentHeight);
        Margin:= MakeEven((CurrentSize * 4) div 100);
        If (CurrentHeight >= CurrentWidth) then
          begin
            HorizontalMargin:= Margin;
            VerticalMargin:= MakeEven(Margin + (CurrentHeight - CurrentWidth) div 2);
          end
        else
          begin
            HorizontalMargin:= MakeEven(Margin + (CurrentWidth - CurrentHeight) div 2);
            VerticalMargin:= Margin;
          end;
        GlyphXOffset:= -CurrentSize DIV 60;
        GlyphYOffset:= -CurrentSize DIV 60;
      end;

    procedure TChartDrawing2DMetrics.CreateCurrentCircleSizes;
      begin
        ZodiacCircleRadius:= (CurrentSize * PercZodiacCircle) DIV 200;    // 100 for percentage * 2 for radius
        HousesCircleRadius:= (CurrentSize * PercHousesCircle) DIV 200;
        DegreesCircleRadius:= (CurrentSize * PercDegreesCircle) DIV 200;
        AspectCircleRadius:= (CurrentSize * PercAspectsCircle) DIV 200;
        SignsCircleRadius:= (CurrentSize * PercSignsCircle) DIV 200;
        PlanetsCircleRadius:= (CurrentSize * PercPlanetsCircle) DIV 200;
        ConnectLinesCircleRadius:= (CurrentSize * PercConnectLinesCircle) DIV 200;
        PositionBodyCircleRadius:= (CurrentSize * PercPositionBodyCircle) DIV 200;
        PositionHouseCircleRadius:= (CurrentSize * PercPositionHouseCircle) DIV 200;
      end;

    procedure TChartDrawing2DMetrics.CreateCurrentFontSizes;
      begin
        FontSizeGlyphs:= CurrentSize DIV 24;
        FontSizeText:= CurrentSize DIV 60;
      end;

    function TChartDrawing2DMetrics.GetAspectCircleRadius: Integer;
      begin
        Result:= AspectCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetHousesCircleRadius: Integer;
      begin
        Result:= HousesCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetDegreesCircleRadius: Integer;
      begin
        Result:= DegreesCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetSignsCircleRadius: Integer;
      begin
        Result:= SignsCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetZodiacCircleRadius: Integer;
      begin
        Result:= ZodiacCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetPlanetsCircleRadius: Integer;
      begin
        Result:= PlanetsCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetConnectLinesCircleRadius: Integer;
      begin
        Result:= ConnectLinesCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetPositionBodyCircleRadius: Integer;
      begin
        Result:= PositionBodyCircleRadius;
      end;

    function TChartDrawing2DMetrics.GetPositionHouseCircleRadius: Integer;
      begin
        Result:= PositionHouseCircleRadius;
      end;

    function TChartDrawing2DMetrics.MakeEven(PValue: Integer): Integer;
      begin
        Result:= ((PValue + 1) DIV 2) * 2;
      end;

    function TChartDrawing2DMetrics.GetCenterX: Integer;
      begin
        Result:= CenterX;
      end;

    function TChartDrawing2DMetrics.GetCenterY: Integer;
      begin
        Result:= CenterY;
      end;

    function TChartDrawing2DMetrics.GetGlyphXOffset: Integer;
      begin
        Result:= GlyphXOffset;
      end;

    function TChartDrawing2DMetrics.GetGlyphYOffset: Integer;
      begin
        Result:= GlyphYOffset;
      end;

    function TChartDrawing2DMetrics.GetFontSizeGlyphs: Integer;
      begin
        Result:= FontSizeGlyphs;
      end;

    function TChartDrawing2DMetrics.GetFontSizeText: Integer;
      begin
        Result:= FontSizeText;
      end;

end.

