{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsDataVaultImpl;
{< Implementations for interfaces in UiChartsDataVault. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain, UiChartsDataVault;

type
  TChartsData = class(TInterfacedObject, IChartsData)
    private
      AllCharts: Array of IFullChartResponse;
      NrOfCharts: Integer;
    public
      procedure AddChart(NewChart: IFullChartResponse);
      function GetAllChartIds: TStringArray;
      function GetChart(Id: String):IFullChartResponse;
  end;

implementation

{ TChartsData }

  procedure TChartsData.AddChart(NewChart: IFullChartResponse);
    begin
      NrOfCharts:= Length(AllCharts);
      SetLength(AllCharts, NrOfCharts+1);
      AllCharts[NrOfCharts]:= NewChart;
    end;

  function TChartsData.GetAllChartIds: TStringArray;
    var
      ChartIds: TStringArray;
      i: Integer;
    begin
      NrOfCharts:= Length(AllCharts);
      SetLength(ChartIds, NrOfCharts);
      for i:= 0 to NrOfCharts-1 do ChartIds[i] := AllCharts[i].Name;
      Result:= ChartIds;
    end;

  function TChartsData.GetChart(Id: String): IFullChartResponse;
    var
      i, FoundId: Integer;
    begin
      FoundId:= -1;
      for i:= 0 to NrOfCharts do begin
        if (Id = AllCharts[i].Name) then FoundId := i;
      end;
      if FoundId < 0 then Result:= nil
      else Result:= AllCharts[FoundId];
    end;


end.

