{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit UiChartsDataVault;
{< contains session data related to charts }

{ TODO R_0.6 : create singleton for datavault }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain;
type

  { Collection of calculated charts. @br
  - Created: 2018 @br
  - Last update: 2019-03-20.   }
  IChartsData = Interface ['{BBE2A3B9-52E6-425C-B9C2-2F22D3A54129}']
    procedure AddChart(NewChart: IFullChartResponse);
    function GetAllChartIds: TStringArray;
    function GetChart(Id: String):IFullChartResponse;
    property AllChartIds: TStringArray read GetAllChartIds;
  end;


implementation

end.

