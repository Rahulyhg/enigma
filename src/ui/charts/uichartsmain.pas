{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit UiChartsMain;
{< Main screen for handling and presenting data/graphics of a chart. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Menus, Grids, SharedXchgDomain, XChartsDomain, UiChartsDataVault, ChartsEndpoints;

type

  { TFormChartsMain }

  TFormChartsMain = class(TForm)
    BtnNewChart: TButton;
    BtnSearchChart: TButton;
    ButtonDraw: TButton;
    ButtonChangeCoordinateSystem: TButton;
    ButtonChangeAyanamsha: TButton;
    ButtonChangeBodies: TButton;
    ButtonChangeConfiguration: TButton;
    ButtonChangeHousesystem: TButton;
    ButtonChangePositions: TButton;
    Label1: TLabel;
    Label2: TLabel;
    LabelCoordinateSystemSelected: TLabel;
    LabelCoordinateSystem: TLabel;
    LabelCelestialObjects: TLabel;
    LabelHouseSystem: TLabel;
    LabelAyanamsha: TLabel;
    LabelConfiguration: TLabel;
    LabelSettings: TLabel;
    LabelAyanamshaSelected: TLabel;
    LabelBodiesSelected: TLabel;
    LabelConfigurationSelected: TLabel;
    LabelHouseSystemSelected: TLabel;
    LabelPositionsSelected: TLabel;
    ChartsMenu: TMainMenu;
    MenuItemCharts: TMenuItem;
    MenuItemChartsNew: TMenuItem;
    MenuItemChartsSearch: TMenuItem;
    MenuItemHelp: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItemChartsExit: TMenuItem;
    StringGridHouses: TStringGrid;
    StringGridMeta: TStringGrid;
    StringGridPositions: TStringGrid;
    procedure BtnNewChartClick(Sender: TObject);
    procedure ButtonChangeAyanamshaClick(Sender: TObject);
    procedure ButtonChangeCoordinateSystemClick(Sender: TObject);
    procedure ButtonChangeHousesystemClick(Sender: TObject);
    procedure ButtonChangePositionsClick(Sender: TObject);
    procedure ButtonDrawClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItemChartsExitClick(Sender: TObject);
    procedure MenuItemChartsNewClick(Sender: TObject);
  private
    FullChartResponse: IFullChartResponse;
    ChartsData: IChartsData;
    procedure DefineCelestialObjectNames;
    procedure StartNewChart(Sender: TObject);
    procedure SelectHouseSystem(Sender: TObject);
    procedure SelectAyanamsha(Sender: TObject);
    procedure SelectCoordinateSystem(Sender: TObject);
    procedure SelectPosition(Sender: TObject);
  public

  end;

var
  FormChartsMain: TFormChartsMain;
  GridTextStyle: TTextStyle;
  CelestialObjectNames: Array of String;
  HouseSystemId, AyanamshaId,CoordinateSystemId: Integer;
  //GlyphsForSigns: TGlyphsConfigurationDtoArray;


implementation

{$R *.lfm}

{ TFormChartsMain }
uses UiChartsInput, SelectHouseSystem, SelectAyanamsha, SelectLov, DomainModel, UiDomain, UiChartsDrawing,
  UiChartsDataVaultImpl;

  procedure TFormChartsMain.MenuItemChartsExitClick(Sender: TObject);
    begin
      Close;
    end;


  procedure TFormChartsMain.FormCreate(Sender: TObject);
    begin
      GridTextStyle:= StringGridPositions.DefaultTextStyle;
      GridTextStyle.Alignment:= taRightJustify;
      StringGridPositions.DefaultTextStyle:= GridTextStyle;
      DefineCelestialObjectNames;
      ChartsData := TChartsData.Create;
    end;


  procedure TFormChartsMain.BtnNewChartClick(Sender: TObject);
    begin
      StartNewChart(Sender);
    end;

  procedure TFormChartsMain.MenuItemChartsNewClick(Sender: TObject);
    begin
      StartNewChart(Sender);
    end;

  procedure TFormChartsMain.ButtonChangeHousesystemClick(Sender: TObject);
    begin
      SelectHouseSystem(Sender);
    end;

  procedure TFormChartsMain.ButtonChangePositionsClick(Sender: TObject);
    begin
      SelectPosition(Sender);
    end;

  procedure TFormChartsMain.ButtonDrawClick(Sender: TObject);
    begin
      FormChartsDrawing.SetChart(FullChartResponse);
      FormChartsDrawing.ShowOnTop;
    end;

  procedure TFormChartsMain.ButtonChangeAyanamshaClick(Sender: TObject);
    begin
      SelectAyanamsha(Sender);
    end;

  procedure TFormChartsMain.ButtonChangeCoordinateSystemClick(Sender: TObject);
    begin
      SelectCoordinateSystem(Sender);
    end;

  procedure TFormChartsMain.StartNewChart(Sender: TObject);
    var
      FilledChartRequest: IFullChartRequest;
      FullChartEndpoint: TFullChartEndpoint;
      i, NrOfPlanets, ObjectId: Integer;
      TextRow: Array[0..13] of String;
      TextRowMeta: Array[0..1] of String;
      TextRowHouses: Array[0..6] of String;
      CelObject: ICelestialObjectFull;
      HousePosition: IHousePositionFull;
      HousesResponse: IHousesResponse;
      SignDmsValue: TSignDMSValue;
      SexagValue: TSexagesimalValue;
      DecimalValue: TDecimalValue;
      Location: IValidatedLocation;
      Date: IValidatedDate;
      Time: IValidatedTime;

    begin
      FormChartsInput.ShowModal;
      if (ReadyForCalc) then begin
        FilledChartRequest:= FullChartRequest;   { TODO get from UiChartsInput }
        FullChartEndpoint:= TFullChartEndpoint.Create;
        FullChartResponse:= FullChartEndpoint.HandleRequest(FilledChartRequest);
        ChartsData.AddChart(FullChartResponse);
        TextRowMeta[0]:= 'Data';
        TextRowMeta[1]:= 'Values';
        StringGridMeta.InsertRowWithValues(0, TextRowMeta);
        TextRowMeta[0]:= 'Name';
        TextRowMeta[1]:= FullChartResponse.GetFullChartRequest.GetName;
        StringGridMeta.InsertRowWithValues(1, TextRowMeta);
        TextRowMeta[0]:= 'Location';
        Location:= FullChartResponse.GetFullChartRequest.GetLocation;
        TextRowMeta[1]:= Location.Name + ': ' + FloatToStr(Location.Longitude) + ' / ' + FloatToStr(Location.Latitude);
        StringGridMeta.InsertRowWithValues(2, TextRowMeta);
        TextRowMeta[0]:= 'Date and time';
        Date:= FullChartResponse.GetFullChartRequest.GetDate;
        Time:= FullChartResponse.GetFullChartRequest.GetTime;
        TextRowMeta[1]:= IntToStr(Date.Year) + '-' + IntToStr(Date.Month) + '-' + IntToStr(Date.Day) +
                         ' ' + FloatToStr(Time.DecimalTime);
        StringGridMeta.InsertRowWithValues(3, TextRowMeta);

        NrOfPlanets:= Length(FullChartResponse.GetAllObjects);
        //StringGridPositions.InsertRowWithValues(0,['Object', 'Longitude', 'Speed', 'Latitude', 'Speed', 'RA', 'Speed',
        //                                           'Declination', 'Speed', 'Distance', 'Speed', 'Azimuth', 'Altitude']);
        for i:= 0 to NrOfPlanets-1 do begin
          CelObject:= FullChartResponse.GetAllObjects[i];
          ObjectId:= CelObject.GetObjectId;

          TextRow[0]:= CelestialObjectNames[ObjectId];
          SignDmsValue:= TSignDmsValue.Create(CelObject.EclipticalPos.GetMainPos);
          TextRow[1]:= SignDmsValue.GetText;

          TextRow[2]:= SignDmsValue.GetGlyph;
          SexagValue:= TSexagesimalValue.Create(CelObject.EclipticalPos.GetMainSpeed);
          TextRow[3]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EclipticalPos.GetDeviationPos);
          TextRow[4]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EclipticalPos.GetDeviationSpeed);
          TextRow[5]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EquatorialPos.GetMainPos);
          TextRow[6]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EquatorialPos.GetMainSpeed);
          TextRow[7]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EquatorialPos.GetDeviationPos);
          TextRow[8]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.EquatorialPos.GetDeviationSpeed);
          TextRow[9]:= SexagValue.GetText;
          DecimalValue:= TDecimalValue.Create(CelObject.EclipticalPos.GetDistancePos);
          TextRow[10]:= DecimalValue.GetText;
          DecimalValue:= TDecimalValue.Create(CelObject.EclipticalPos.GetDistanceSpeed);
          TextRow[11]:= DecimalValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.HorizontalPos.GetMainPos);
          TextRow[12]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(CelObject.HorizontalPos.GetDeviationPos);
          TextRow[13]:= SexagValue.GetText;
          StringGridPositions.InsertRowWithValues(i+1, TextRow);
        end;


        // showhouses
        HousesResponse:= FullChartResponse.GetHouses;
        //TextRow[0]:= HousesResponse.HouseSystemSpec.Name;
        //for i:= 1 to 12 do TextRow[i]:= '';
        //StringGridHouses.InsertRowWithValues(StartRowForHouses, TextRow);
        for i:= 1 to HousesResponse.HouseSystemSpec.NumberOfHouses do begin
          TextRowHouses[0]:= IntToStr(i);
          HousePosition:= HousesResponse.GetCusps[i];
          SignDmsValue:= TSignDmsValue.Create(HousePosition.Longitude);
          TextRowHouses[1]:= SignDmsValue.GetText;
          TextRowHouses[2]:= SignDmsValue.GetGlyph;
          SexagValue:= TSexagesimalValue.Create(HousePosition.RightAscension);
          TextRowHouses[3]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(HousePosition.Declination);
          TextRowHouses[4]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(HousePosition.Azimuth);
          TextRowHouses[5]:= SexagValue.GetText;
          SexagValue:= TSexagesimalValue.Create(HousePosition.Altitude);
          TextRowHouses[6]:= SexagValue.GetText;
          StringGridHouses.InsertRowWithValues(i, TextRowHouses);
        end;
      end;
      ButtonDraw.Enabled:= true;
    end;

  procedure TFormChartsMain.SelectHouseSystem(Sender: TObject);
    begin
      FormSelectHouseSystem.SetCurrentIndex(1);
      FormSelectHouseSystem.ShowModal;
      LabelHouseSystemSelected.Caption:= FormSelectHouseSystem.GetSelectedName;
      HouseSystemId:= FormSelectHouseSystem.GetSelectedIndex;
    end;

  procedure TFormChartsMain.SelectAyanamsha(Sender: TObject);
    begin
      FormSelectAyanamsha.ShowModal;
      LabelAyanamshaSelected.Caption:= FormSelectAyanamsha.GetSelectedName;
      AyanamshaId:= FormSelectAyanamsha.GetSelectedIndex;
    end;

  procedure TFormChartsMain.SelectCoordinateSystem(Sender: TObject);
    begin
      FormSelectLov.SetTableName('CoordinateSystems');
      FormSelectLov.LabelValues.Caption:= 'Coordinate Systems';
      FormSelectLov.ShowModal;
      LabelCoordinateSystemSelected.Caption:= FormSelectLov.GetSelectedName;
      CoordinateSystemId:= FormSelectLov.GetSelectedIndex;
    end;

  procedure TFormChartsMain.SelectPosition(Sender: TObject);
    begin
      FormSelectLov.SetTableName('ObserverPositions');
      FormSelectLov.LabelValues.Caption:= 'Observer positions';
      FormSelectLov.ShowModal;
      LabelPositionsSelected.Caption:= FormSelectLov.GetSelectedName;
      CoordinateSystemId:= FormSelectLov.GetSelectedIndex;
    end;


  procedure TFormChartsMain.DefineCelestialObjectNames;
    begin
      { TODO : R_0.6 Replace with values from endpoint for GlyphsCOnfiguration }
      SetLength(CelestialObjectNames, 16);
      CelestialObjectNames[0]:='a';
      CelestialObjectNames[1]:='b';
      CelestialObjectNames[2]:='c';
      CelestialObjectNames[3]:='d';
      CelestialObjectNames[4]:='f';
      CelestialObjectNames[5]:='g';
      CelestialObjectNames[6]:='h';
      CelestialObjectNames[7]:='i';
      CelestialObjectNames[8]:='j';
      CelestialObjectNames[9]:='k';
      CelestialObjectNames[10]:='{';
      CelestialObjectNames[11]:='True Node';
      CelestialObjectNames[12]:='Mean Apogee';
      CelestialObjectNames[13]:='True Apogee';
      CelestialObjectNames[14]:='e';
      CelestialObjectNames[15]:='w';
    end;


end.

