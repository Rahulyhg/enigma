{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseUiDomain;

{TODO create tests for FullChartValues, FullCelestialObjectValues and FullHouseValues }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, UiDomain;

const
     DEGREE_SIGN = '°';     { TODO : Replace degreesign with unicode U+00B0 }
     MINUTE_SIGN = '''';
     SECOND_SIGN = '"';
     SPACE = ' ';
type
   TestDecimalValue = class(TTestCase)
   published
     procedure TestHappyFlow;
     procedure TestShortFraction;
   end;

   TestSexagesimalValue = class(TTestCase)
     published
       procedure TestHappyFlow;
       procedure TestShortFraction;
       procedure TestNegativeValue;
   end;

   //TestSignDMSValue = class(TTestCase)
   //  private
   //    SignDMSValue: TSignDMSValue;
   //  published
   //    procedure SetUp; override;
   //    procedure TestText;
   //    procedure TestGlyph;
   //    procedure TestAbbreviation;
   //    procedure TestShortFraction;
   //end;

   TestMetaInfoValues = class(TTestCase)
     published
       procedure TestHappyFlow;
   end;


implementation
{ TestDecimalValue }
procedure TestDecimalValue.TestHappyFlow;
var
  DecValue : Double;
  Expected, Result: String;
  DecimalValue: TDecimalValue;
begin
  DecValue:= 123.458987654321098;
  Expected:= '123,45898765';
  DecimalValue:= TDecimalValue.Create(DecValue);
  Result:= DecimalValue.GetText;
  CheckEquals(Expected, Result);
end;


procedure TestDecimalValue.TestShortFraction;
var
  DecValue : Double;
  Expected, Result: String;
  DecimalValue: TDecimalValue;
begin
  DecValue:= 123.45;
  Expected:= '123,45000000';
  DecimalValue:= TDecimalValue.Create(DecValue);
  Result:= DecimalValue.GetText;
  CheckEquals(Expected, Result);
end;

{ TestSexagesimalValue }

procedure TestSexagesimalValue.TestHappyFlow;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= 123.458987654321098;
  Expected:= '123'+ DEGREE_SIGN +  '27' + MINUTE_SIGN + '32' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;

procedure TestSexagesimalValue.TestShortFraction;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= 1.1016666666666668;
  Expected:= '  1'+ DEGREE_SIGN + ' 6' + MINUTE_SIGN + ' 6' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;

procedure TestSexagesimalValue.TestNegativeValue;
var
  DecValue : Double;
  Expected, Result: String;
  SexagValue: TSexagesimalValue;
begin
  DecValue:= -123.458987654321098;
  Expected:= '-123'+ DEGREE_SIGN +  '27' + MINUTE_SIGN + '32' + SECOND_SIGN;
  SexagValue:= TSexagesimalValue.Create(DecValue);
  Result:= SexagValue.GetText;
  CheckEquals(Expected, Result);
end;

  //{ TestSignDMSValue }
  //
  //procedure TestSignDMSValue.SetUp;
  //  begin
  //    SignDMSValue:= TSignDMSValue.Create(3.5);
  //  end;
  //
  //procedure TestSignDMSValue.TestText;
  //  begin
  //    CheckEquals(SPACE + SPACE + '3' + DEGREE_SIGN +  '30' + MINUTE_SIGN + ' 0' + SECOND_SIGN, SignDMSValue.GetText);
  //  end;
  //
  //procedure TestSignDMSValue.TestGlyph;
  //  begin
  //    CheckEquals('1', SignDMSValue.GetGlyph);
  //  end;
  //
  //procedure TestSignDMSValue.TestAbbreviation;
  //  begin
  //    CheckEquals('AR', SignDMSValue.GetAbbreviation);
  //  end;
  //
  //procedure TestSignDMSValue.TestShortFraction;
  //  begin
  //    SignDMSValue:= TSignDMSValue.Create(0.0);
  //    CheckEquals(SPACE + SPACE + '0' + DEGREE_SIGN +  ' 0' + MINUTE_SIGN + ' 0' + SECOND_SIGN, SignDMSValue.GetText);
  //  end;



{ TestMetaInfoValues }
procedure TestMetaInfoValues.TestHappyFlow;
var
  Name, Location, DateTime, Settings: String;
  MetaInfoValues: TMetaInfoValues;
begin
  Name:= 'MyName';
  Location:= 'MyLocation';
  DateTime:= 'MyDateTime';
  Settings:= 'MySettings';
  MetaInfoValues:= TMetaInfoValues.Create(Name, Location, DateTime, Settings);
  CheckEquals(Name, MetaInfoValues.GetName);
  CheckEquals(Location, MetaInfoValues.GetLocation);
  CheckEquals(DateTime, MetaInfoValues.GetDateTime);
  CheckEquals(Settings, MetaInfoValues.GetSettings);
end;




initialization

  RegisterTest('UiDomain',TestDecimalValue);
  RegisterTest('UiDomain',TestSexagesimalValue);
  //RegisterTest('UiDomain',TestSignDMSValue);
  RegisterTest('UiDomain', TestMetaInfoValues);

end.

