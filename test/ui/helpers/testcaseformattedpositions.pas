{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseFormattedPositions;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, FormattedPositions;

type

  TestDegreeMinuteSign = class(TTestCase)
    strict private
      DMS: IDegreeMinuteSign;
    published
      procedure TestDegrees;
      procedure TestDegreesNoLeadingZero;
      procedure TestMinutes;
      procedure TestMinutesNoLeadingZero;
      procedure TestMinutesDegrees;
      { TODO : Add test for function GetSign }
  end;


  TestSexagesimalPositions = class(TTestCase)
    published
      procedure TestSexagesimalDegreesHappyFlow;
      procedure TestSexagesimalDegreesHappyFlowLarge;
      procedure TestSexagesimalDegreesHappyFlowZero;
      procedure TestSexagesimalSignsHappyFlow;
      procedure TestSexagesimalSignsHappyFlowMoreSigns;
      procedure TestSexagesimalSignsZeroAries;
  end;

implementation

uses FormattedPositionsImpl;

{ TestDegreeMinuteSign }

  procedure TestDegreeMinuteSign.TestDegrees;
    begin
      DMS:= TDegreeMinuteSign.Create(23.456);
      AssertEquals('23°', DMS.Degrees);
    end;

  procedure TestDegreeMinuteSign.TestDegreesNoLeadingZero;
    begin
      DMS:= TDegreeMinuteSign.Create(2.33333);
      AssertEquals('2°', DMS.Degrees);
    end;

  procedure TestDegreeMinuteSign.TestMinutes;
    begin
      DMS:= TDegreeMinuteSign.Create(22.6666666666666667);
      AssertEquals('40''', DMS.Minutes);
    end;

  procedure TestDegreeMinuteSign.TestMInutesNoLeadingZero;
    begin
      DMS:= TDegreeMinuteSign.Create(22.1);
      AssertEquals('6''', DMS.Minutes);
    end;

  procedure TestDegreeMinuteSign.TestMinutesDegrees;
    begin
      DMS:= TDegreeMinuteSign.Create(15.5);
      AssertEquals('15°30''', DMS.DegreesMinutes);
    end;

{ TestSexagesimalPositions }

procedure TestSexagesimalPositions.TestSexagesimalDegreesHappyFlow;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 3.5;
  Expected:= '  3°30''00"';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalDegrees);
end;

procedure TestSexagesimalPositions.TestSexagesimalDegreesHappyFlowLarge;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 321.9999999999;
  Expected:= '321°59''59"';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalDegrees);
end;

procedure TestSexagesimalPositions.TestSexagesimalDegreesHappyFlowZero;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 0;
  Expected:= '  0°00''00"';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalDegrees);
end;

procedure TestSexagesimalPositions.TestSexagesimalSignsHappyFlow;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 3.5;
  Expected:= ' 3°30''00" AR';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalSigns);
end;

procedure TestSexagesimalPositions.TestSexagesimalSignsHappyFlowMoreSigns;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 263.66666667;
  Expected:= '23°40''00" SA';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalSigns);
end;

procedure TestSexagesimalPositions.TestSexagesimalSignsZeroAries;
var
  TestValue: Double;
  Expected: String;
  SexagPos: TSexagesimalPositions;
begin
  TestValue := 0;
  Expected:= ' 0°00''00" AR';
  SexagPos:= TSexagesimalPositions.Create(TestValue);
  CheckEquals(Expected, SexagPos.SexagesimalSigns);
end;


initialization
  RegisterTest('FormattedPositions',TestDegreeMinuteSign);
  RegisterTest('FormattedPositions',TestSexagesimalPositions);

end.

