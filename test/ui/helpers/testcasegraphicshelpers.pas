{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseGraphicsHelpers;

{$mode objfpc}{$H+}

interface

  uses
    Classes, SysUtils, fpcunit, testutils, testregistry, GraphicsHelpers;

  type
    TestRectTriangle = class(TTestCase)
      strict private
        CenterX, CenterY, Hypothenusa: Integer;
        Angle: Double;
        RectTriangle: IRectTriangle;
      published
        procedure SetUp; override;
        procedure TestXYPointSmall;
        procedure TestXYPointLarge;
    end;

implementation

  uses
    GraphicsHelpersImpl;

  { TestRectTriangle }
  procedure TestRectTriangle.SetUp;
    begin
      CenterX:= 200;
      CenterY:= 400;
      Hypothenusa:= 100;
    end;

  procedure TestRectTriangle.TestXYPointSmall;
    var
      ExpectedX, ExpectedY: Integer;
    begin
      Angle:= 30.0;
      ExpectedX:= 114;
      ExpectedY:= 350;
      RectTriangle:= TRectTriangle.Create(CenterX, CenterY, Hypothenusa, Angle);
      AssertEquals(ExpectedX, RectTriangle.XYPoint.x);
      AssertEquals(ExpectedY, RectTriangle.XYPoint.y);
    end;

  procedure TestRectTriangle.TestXYPointLarge;
    var
      ExpectedX, ExpectedY: Integer;
    begin
      Angle:= 211.0;
      ExpectedX:= 286;
      ExpectedY:= 452;
      RectTriangle:= TRectTriangle.Create(CenterX, CenterY, Hypothenusa, Angle);
      AssertEquals(ExpectedX, RectTriangle.XYPoint.x);
      AssertEquals(ExpectedY, RectTriangle.XYPoint.y);
    end;



initialization

  RegisterTest('GraphicsHelpers',TestRectTriangle);


end.

