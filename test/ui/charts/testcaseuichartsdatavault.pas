{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }

unit TestCaseUiChartsDataVault;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, UiChartsDataVault;

type

  TestUiChartsDataVault= class(TTestCase)
    protected
      ChartsData: IChartsData;
      procedure SetUp; override;
    published
      procedure TestAddGetChart;
      procedure TestAllChartIds;
    end;

implementation

uses UiChartsDataVaultImpl, FakesXChartsDomain;

  procedure TestUiChartsDataVault.SetUp;
    begin
      ChartsData:= TChartsData.Create;
      ChartsData.AddChart(FakeFullChartResponse.Create);
      ChartsData.AddChart(FakeFullChartResponse2.Create);
    end;

  procedure TestUiChartsDataVault.TestAddGetChart;
    begin
      AssertEquals('Full Chart Name', ChartsData.GetChart('Full Chart Name').Name);
    end;

  procedure TestUiChartsDataVault.TestAllChartIds;
    var
      AllIds: TStringArray;
    begin
      AllIds:= ChartsData.AllChartIds;
      AssertEquals(2, Length(AllIds));
      AssertEquals('Full Chart Name', AllIds[0]);
      AssertEquals('Full Chart 2 Name', AllIds[1]);
    end;

initialization

  RegisterTest('UiChartsDataVault', TestUiChartsDataVault);

end.

