{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseChartMetrics;


{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, ChartMetrics;

type

  TestChartDrawing2DMetrics = class(TTestCase)
    private
      Metrics: IChartDrawing2DMetrics;
    published
      procedure SetUp; override;
      procedure TestAspectCircleRadius;
      procedure TestHousesCircleRadius;
      procedure TestSignsCircleRadius;
      procedure TestZodiacCircleRadius;
      procedure TestPlanetsCircleRadius;
      procedure TestConnectLinesCircleRadius;
      procedure TestPositionBodyCircleRadius;
      procedure TestPositionHouseCircleRadius;
      procedure TestCenterX;
      procedure TestCenterY;
      procedure TestGlyphXOffset;
      procedure TestGlyphYOffset;
      procedure TestFontSizeGlyphs;
      procedure TestFontSizeText;
      procedure Resize;
  end;

implementation
  uses
    Graphics, ChartMetricsImpl, FakesChartMetrics;
  const
    InitialWidth = 400;
    InitialHeight = 200;
    PercZodiacCircle = 90;
    PercSignsCircle = 80;
    PercPositionHouseCircle =  84;
    PercHousesCircle = 70;
    PercDegreesCircle = 68;
    PercPositionBodyCircle = 60;
    PercPlanetsCircle = 50;
    PercConnectLinesCircle = 44;
    PercAspectsCircle = 40;


  { TestChartDrawing2DMetrics }
     procedure TestChartDrawing2DMetrics.SetUp;
       begin
         Metrics:= TChartDrawing2DMetrics.Create(InitialWidth, InitialHeight, PercZodiacCircle, PercHousesCircle,
                                                 PercDegreesCircle, PercAspectsCircle, PercSignsCircle,
                                                 PercPlanetsCircle, PercConnectLinesCircle, PercPositionBodyCircle,
                                                 PercPositionHouseCircle);
       end;

     procedure TestChartDrawing2DMetrics.TestAspectCircleRadius;
       begin
         AssertEquals(40, Metrics.AspectCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestHousesCircleRadius;
       begin
         AssertEquals(70, Metrics.HousesCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestSignsCircleRadius;
       begin
         AssertEquals(80, Metrics.SignsCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestZodiacCircleRadius;
       begin
         AssertEquals(90, Metrics.ZodiacCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestPlanetsCircleRadius;
       begin
         AssertEquals(50, Metrics.PlanetscircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestConnectLinesCircleRadius;
       begin
         AssertEquals(44, Metrics.ConnectLinesCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestPositionBodyCircleRadius;
       begin
         AssertEquals(60, Metrics.PositionBodyCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestPositionHouseCircleRadius;
       begin
         AssertEquals(84, Metrics.PositionHouseCircleRadius);
       end;

     procedure TestChartDrawing2DMetrics.TestCenterX;
       begin
         AssertEquals(200, Metrics.CenterX);
       end;

     procedure TestChartDrawing2DMetrics.TestCenterY;
       begin
         AssertEquals(100, Metrics.CenterY);
       end;

     procedure TestChartDrawing2DMetrics.TestGlyphXOffset;
       begin
         AssertEquals(-3, Metrics.GlyphXOfset);
       end;

     procedure TestChartDrawing2DMetrics.TestGlyphYOffset;
       begin
         AssertEquals(-3, Metrics.GlyphYOfset);
       end;

     procedure TestChartDrawing2DMetrics.TestFontSizeGlyphs;
       begin
         AssertEquals(8, Metrics.FontSizeGlyphs);
       end;

     procedure TestChartDrawing2DMetrics.TestFontSizeText;
       begin
         AssertEquals(3, Metrics.FontSizeText);
       end;

     procedure TestChartDrawing2DMetrics.Resize;
       begin
         Metrics.Resize(800, 600);
         AssertEquals(270, Metrics.ZodiacCircleRadius);
       end;


initialization
  RegisterTest('ChartMetrics',TestChartDrawing2DMetrics);

end.

