{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseDao;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, db, sqldb, Dao, Dto, DbTestUtils;


type
  TestLookupValueDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

  TestVersionDao = class(TTestCase)
    published
      procedure SetUp; Override;
      procedure WriteAndRead;
      procedure ReadAll;
  end;

  TestHouseSystemsDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

  TestAyanamshasDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

  TestBodiesDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

  TestAspectsDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

  TestConfigurationsDao = class(TTestCase)
    published
      procedure SetUp; override;
      procedure WriteAndRead;
      procedure ReadAll;
      procedure Update;
      procedure Delete;
  end;

{ TODO : Testcases voor lezen glyphs
 }



implementation

  uses
    sqlite3conn, DaoImpl;

  const
    MARGIN = 0.000000001;

  var
    DbInitialized: Boolean = false;
    Greenfield: TDataBaseGreenfield;

  procedure InitializeDb;
    begin
      if (DbInitialized = false) then
      begin
        Greenfield:= TDataBaseGreenfield.Create;
        DbInitialized:= true;
      end;
    end;


  { TestLookupValueDao }

  procedure TestLookupValueDao.SetUp;
    begin
      InitializeDb;
    end;

  procedure TestLookupValueDao.WriteAndRead;
    var
      Id: integer = 2001;
      Name: String = 'TopoCentric';
      Description: String = 'The location of the observer as the center.';
      Dao: ILookupValueDao;
      ResultArray: TLookUpValueDtoArray;
    begin
      Dao:= TFactoryLookUpValueDao.Create('ObserverPositions').GetInstance;
      Dao.Insert(Id, Name, Description);
      ResultArray := Dao.Read(Id);
      AssertEquals(Name, ResultArray[0].Name);
      AssertEquals(Description, ResultArray[0].Description);
      Dao.Delete(Id);
    end;

  procedure TestLookupValueDao.ReadAll;
    var
      Dao: ILookupValueDao;
      SortColumn: String = 'Name';
      ResultArray: TLookUpValueDtoArray;
    begin
      Dao:= TFactoryLookUpValueDao.Create('ObserverPositions').GetInstance;
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 3);
    end;

  procedure TestLookupValueDao.Update;
    var
      Dao: ILookupValueDao;
      ResultArray: TLookUpValueDtoArray;
      Id: integer = 2002;
      Name: String = 'TopoCentric';
      Description: String = 'The location of the observer as the center.';
    begin
      // Write
      Dao:= TFactoryLookUpValueDao.Create('ObserverPositions').GetInstance;
      Dao.Insert(Id, Name, Description);
      // Update
      Dao.Update(Id, 'NewName', 'NewDescription');
      // Read
      ResultArray := Dao.Read(Id);
      // Compare
      AssertEquals('NewName', ResultArray[0].Name);
      AssertEquals('NewDescription', ResultArray[0].Description);
      // CleanUp
      Dao.Delete(Id);
    end;

  procedure TestLookupValueDao.Delete;
    var
      Dao: ILookupValueDao;
      ResultArray: TLookUpValueDtoArray;
      Id: integer = 2002;
      Name: String = 'TopoCentric';
      Description: String = 'The location of the observer as the center.';
    begin
      // Write
      Dao:= TFactoryLookUpValueDao.Create('ObserverPositions').GetInstance;
      Dao.Insert(Id, Name, Description);
      ResultArray := Dao.Read(Id);
      AssertEquals(Id, ResultArray[0].Id);
      // Delete;
      Dao.Delete(Id);
      ResultArray := Dao.Read(Id);
      AssertTrue(Length(ResultArray) = 0);
    end;


  { TestVersionDao }
  procedure TestVersionDao.SetUp;
    begin
      InitializeDb;
    end;

  procedure TestVersionDao.WriteAndRead;
      var
      Id: integer = 2005;
      Major: Integer = 12;
      Minor: Integer = 7;
      Micro: Integer = 360;
      Date: String = '2000-01-01';
      Dao: IVersionsDao;
      ResultArray: TVersionDtoArray;
    begin
      Dao:= TFactoryVersionsDao.Create.GetInstance('ApplicationVersions');
      Dao.Insert(Id, Major, Minor, Micro, Date);
      ResultArray := Dao.Read(Id);
      AssertEquals(Major, ResultArray[0].Major);
      AssertEquals(Minor, ResultArray[0].Minor);
      AssertEquals(Micro, ResultArray[0].Micro);
      AssertEquals(Date, ResultArray[0].Date);
    end;

  procedure TestVersionDao.ReadAll;
    var
      Dao: IVersionsDao;
      SortColumn: String = 'Date';
      ResultArray: TVersionDtoArray;
    begin
      Dao:= TFactoryVersionsDao.Create.GetInstance('ApplicationVersions');
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 1);
    end;

  { TestHouseSystemsDao }

  procedure TestHouseSystemsDao.SetUp;
    begin
      InitializeDb;
    end;

  procedure TestHouseSystemsDao.WriteAndRead;
    var
      Id: integer = 219;
      Name: String = 'Placidus';
      Description: String = 'Time based, each cusp is on a specific part of its own semi-arc.';
      NrOfHouses: Integer = 12;
      ClockWise: Boolean = true;
      QuadrantSystem: Boolean = true;
      CuspIsStart: Boolean = true;
      Dao: IHouseSystemsDao;
      ResultArray: THouseSystemDtoArray;
      ResultText: String;
    begin
      Dao:= TFactoryHouseSystemsDao.Create.GetInstance;
      ResultText:= Dao.Insert(Id, Name, Description, NrOfHouses, ClockWise, QuadrantSystem, CuspIsStart);
      ResultArray := Dao.Read(Id);
      AssertEquals('OK', ResultText);
      AssertEquals(Name, ResultArray[0].Name);
      AssertEquals(Description, ResultArray[0].Description);
      AssertEquals(NrOfHouses, ResultArray[0].NrOfHouses);
      AssertEquals(ClockWise, ResultArray[0].CounterClockWise);
      AssertEquals(CuspIsStart, ResultArray[0].CuspIsStart);
      // cleanup
      Dao.Delete(Id);
    end;

  procedure TestHouseSystemsDao.ReadAll;
    var
      Dao: IHouseSystemsDao;
      SortColumn: String = 'Name';
      ResultArray: THouseSystemDtoArray;
    begin
      Dao:= TFactoryHouseSystemsDao.Create.GetInstance;
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 2);
    end;

  procedure TestHouseSystemsDao.Update;
    var
      Id: integer = 319;
      Name: String = 'Placidus';
      Description: String = 'Time based, each cusp is on a specific part of its own semi-arc.';
      NrOfHouses: Integer = 12;
      CounterClockWise: Boolean = true;
      QuadrantSystem: Boolean = true;
      CuspIsStart: Boolean = true;
      Dao: IHouseSystemsDao;
      ResultArray: THouseSystemDtoArray;
    begin
      // Write
      Dao:= TFactoryHouseSystemsDao.Create.GetInstance;
      Dao.Insert(Id, Name, Description, NrOfHouses, CounterClockWise, QuadrantSystem, CuspIsStart);
      // Update
      Dao.Update(Id, 'NewName', 'NewDescription', 16, true, false, true);
      // Read
      ResultArray := Dao.Read(Id);
      // Compare
      AssertEquals('NewName', ResultArray[0].Name);
      AssertEquals('NewDescription', ResultArray[0].Description);
      AssertEquals(16, ResultArray[0].NrOfHouses);
      AssertTrue(ResultArray[0].CounterClockWise);
      AssertFalse(ResultArray[0].QuadrantSystem);
      AssertTrue(ResultArray[0].CuspIsStart);
      // CleanUp
      Dao.Delete(Id);
    end;

  procedure TestHouseSystemsDao.Delete;
    var
      Id: integer = 419;
      Name: String = 'Placidus';
      Description: String = 'Time based, each cusp is on a specific part of its own semi-arc.';
      NrOfHouses: Integer = 12;
      CounterClockWise: Boolean = true;
      QuadrantSystem: Boolean = true;
      CuspIsStart: Boolean = true;
      Dao: IHouseSystemsDao;
      ResultArray: THouseSystemDtoArray;
    begin
      // Write
      Dao:= TFactoryHouseSystemsDao.Create.GetInstance;
      Dao.Insert(Id, Name, Description, NrOfHouses, CounterClockWise, QuadrantSystem, CuspIsStart);
      ResultArray := Dao.Read(Id);
      AssertEquals(Id, ResultArray[0].Id);
      // Delete;
      Dao.Delete(Id);
      ResultArray := Dao.Read(Id);
      AssertTrue(Length(ResultArray) = 0);
    end;


  { TestAyanamshasDao }

  procedure TestAyanamshasDao.SetUp;
    begin
      InitializeDb;
    end;

  procedure TestAyanamshasDao.WriteAndRead;
    var
      Id: integer = 219;
      Name: String = 'AyanamshaName';
      Description: String = 'Ayanamsha description.';
      Offset2000: Double = 28.5;
      Dao: IAyanamshasDao;
      ResultArray: TAyanamshaDtoArray;
      ResultText: String;
    begin
      Dao:= TFactoryAyanamshasDao.Create.GetInstance;
      ResultText:= Dao.Insert(Id, Name, Description, Offset2000);
      ResultArray := Dao.Read(Id);
      AssertEquals('OK', ResultText);
      AssertEquals(Name, ResultArray[0].Name);
      AssertEquals(Description, ResultArray[0].Description);
      AssertEquals(Offset2000, ResultArray[0].Offset2000, MARGIN);
      // cleanup
      Dao.Delete(Id);
    end;

  procedure TestAyanamshasDao.ReadAll;
    var
      Dao: IAyanamshasDao;
      ResultArray: TAyanamshaDtoArray;
      SortColumn: String = 'Name';
    begin
      Dao:= TFactoryAyanamshasDao.Create.GetInstance;
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 2);
    end;

  procedure TestAyanamshasDao.Update;
    var
      Id: integer = 600;
      Name: String = 'AyanamshaName1';
      Description: String = 'AyanamshaDescription1.';
      OffSet2000: Double = 29.2;
      Dao: IAyanamshasDao;
      ResultArray: TAyanamshaDtoArray;
    begin
      // Write
      Dao:= TFactoryAyanamshasDao.Create.GetInstance;
      Dao.Insert(Id, Name, Description, Offset2000);
      // Update
      Dao.Update(Id, 'AyanamshaName2', 'AyanamshaDescription2', 22.2);
      // Read
      ResultArray := Dao.Read(Id);
      // Compare
      AssertEquals('AyanamshaName2', ResultArray[0].Name);
      AssertEquals('AyanamshaDescription2', ResultArray[0].Description);
      AssertEquals(22.2, ResultArray[0].Offset2000, MARGIN);
      // CleanUp
      Dao.Delete(Id);
    end;

  procedure TestAyanamshasDao.Delete;
    var
      Id: integer = 660;
      Name: String = 'AyanamshaName';
      Description: String = 'AyanamshaDescription.';
      Offset2000: Double = 27.77;
      Dao: IAyanamshasDao;
      ResultArray: TAyanamshaDtoArray;
    begin
      // Write
      Dao:= TFactoryAyanamshasDao.Create.GetInstance;
      Dao.Insert(Id, Name, Description, Offset2000);
      ResultArray := Dao.Read(Id);
      AssertEquals(Id, ResultArray[0].Id);
      // Delete;
      Dao.Delete(Id);
      ResultArray := Dao.Read(Id);
      AssertTrue(Length(ResultArray) = 0);
    end;

  { TestBodiesDao }

  procedure  TestBodiesDao.SetUp;
    begin
      InitializeDb;
    end;

  procedure  TestBodiesDao.WriteAndRead;
    var
      Id: integer = 219;
      Name: String = 'BodyName';
      BodyCategory: Integer = 6;
      Dao: IBodiesDao;
      ResultArray: TBodyDtoArray;
      ResultText: String;
    begin
      Dao:= TFactoryBodiesDao.Create.GetInstance;
      ResultText:= Dao.Insert(Id, Name, BodyCategory);
      ResultArray := Dao.Read(Id);
      AssertEquals('OK', ResultText);
      AssertEquals(Name, ResultArray[0].Name);
      AssertEquals(BodyCategory, ResultArray[0].BodyCategory);
      // cleanup
      Dao.Delete(Id);
    end;

  procedure TestBodiesDao.ReadAll;
    var
      Dao: IBodiesDao;
      ResultArray: TBodyDtoArray;
      SortColumn: String = 'Name';
    begin
      Dao:= TFactoryBodiesDao.Create.GetInstance;
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 2);
    end;

  procedure TestBodiesDao.Update;
    var
      Id: integer = 600;
      Name: String = 'BodyName1';
      BodyCategory: Integer = 1;
      Dao: IBodiesDao;
      ResultArray: TBodyDtoArray;
    begin
      // Write
      Dao:= TFactoryBodiesDao.Create.GetInstance;
      Dao.Insert(Id, Name, BodyCategory);
      // Update
      Dao.Update(Id, 'BodyName2', 2);
      // Read
      ResultArray := Dao.Read(Id);
      // Compare
      AssertEquals('BodyName2', ResultArray[0].Name);
      AssertEquals(2, ResultArray[0].BodyCategory);
      // CleanUp
      Dao.Delete(Id);
    end;

  procedure TestBodiesDao.Delete;
    var
      Id: integer = 660;
      Name: String = 'BodyName';
      BodyCategory: Integer = 3;
      Dao: IBodiesDao;
      ResultArray: TBodyDtoArray;
    begin
      // Write
      Dao:= TFactoryBodiesDao.Create.GetInstance;
      Dao.Insert(Id, Name, BodyCategory);
      ResultArray := Dao.Read(Id);
      AssertEquals(Id, ResultArray[0].Id);
      // Delete;
      Dao.Delete(Id);
      ResultArray := Dao.Read(Id);
      AssertTrue(Length(ResultArray) = 0);
    end;

  { TestAspectsDao }

  procedure TestAspectsDao.SetUp;
    begin
     InitializeDb;
    end;

  procedure TestAspectsDao.WriteAndRead;
    var
      Id: integer = 333;
      Name: String = 'AspectName';
      AspectCategory: Integer = 2;
      Angle: Double = 44.44;
      Dao: IAspectsDao;
      ResultArray: TAspectDtoArray;
      ResultText: String;
    begin
      Dao:= TFactoryAspectsDao.Create.GetInstance;
      ResultText:= Dao.Insert(Id, Name, Angle, AspectCategory);
      ResultArray := Dao.Read(Id);
      AssertEquals('OK', ResultText);
      AssertEquals(Name, ResultArray[0].Name);
      AssertEquals(Angle, ResultArray[0].Angle);
      AssertEquals(AspectCategory, ResultArray[0].AspectCategory);
      // cleanup
      Dao.Delete(Id);
    end;

  procedure TestAspectsDao.ReadAll;
    var
      Dao: IAspectsDao;
      ResultArray: TAspectDtoArray;
      SortColumn: String = 'Name';
    begin
      Dao:= TFactoryAspectsDao.Create.GetInstance;
      ResultArray:= Dao.ReadAll(SortColumn);
      AssertTrue(Length(ResultArray) >= 2);
    end;

  procedure TestAspectsDao.Update;
    var
      Id: integer = 99;
      Name: String = 'AspectName1';
      Angle: Double = 12.34;
      AspectCategory: Integer = 1;
      Dao: IAspectsDao;
      ResultArray: TAspectDtoArray;
    begin
      // Write
      Dao:= TFactoryAspectsDao.Create.GetInstance;
      Dao.Insert(Id, Name, Angle, AspectCategory);
      // Update
      Dao.Update(Id, 'AspectName2', 19.2, 2);
      // Read
      ResultArray := Dao.Read(Id);
      // Compare
      AssertEquals('AspectName2', ResultArray[0].Name);
      AssertEquals(19.2, ResultArray[0].Angle);
      AssertEquals(2, ResultArray[0].AspectCategory);
      // CleanUp
      Dao.Delete(Id);
    end;

  procedure TestAspectsDao.Delete;
    var
      Id: integer = 888;
      Name: String = 'AspectName';
      Angle: Double = 77.54;
      AspectCategory: Integer = 33;
      Dao: IAspectsDao;
      ResultArray: TAspectDtoArray;
    begin
      // Write
      Dao:= TFactoryAspectsDao.Create.GetInstance;
      Dao.Insert(Id, Name, Angle, AspectCategory);
      ResultArray := Dao.Read(Id);
      AssertEquals(Id, ResultArray[0].Id);
      // Delete;
      Dao.Delete(Id);
      ResultArray := Dao.Read(Id);
      AssertTrue(Length(ResultArray) = 0);
    end;

 { TestConfigurationsDao }
   procedure TestConfigurationsDao.SetUp;
     begin
       InitializeDb;
     end;

   procedure TestConfigurationsDao.WriteAndRead;
     var
       Id: integer = 333;
       Name: String = 'ConfigName';
       Description: String = 'ConfigDescription';
       BaseOrbAspects: Double = 6.5;
       BaseOrbMidpoints: Double = 1.2;
       CoordinateSystem: Integer = 0;
       Ayanamsha: Integer = 1;
       HouseSystem: Integer = 2;
       ObserverPosition: Integer = 3;
       ProjectionType: Integer = 4;
       Dao: IConfigurationsDao;
       ResultArray: TConfigurationDtoArray;
       ResultText: String;
     begin
       Dao:= TFactoryConfigurationsDao.Create.GetInstance;
       ResultText:= Dao.Insert(Id, Name, Description, BaseOrbAspects, BaseOrbMidpoints, CoordinateSystem, Ayanamsha,
                               HouseSystem, ObserverPosition, ProjectionType);
       ResultArray := Dao.Read(Id);
       AssertEquals('OK', ResultText);
       AssertEquals(Name, ResultArray[0].Name);
       AssertEquals(Description, ResultArray[0].Description);
       AssertEquals(BaseOrbAspects, ResultArray[0].BaseOrbAspects);
       AssertEquals(BaseOrbMidpoints, ResultArray[0].BaseOrbMidpoints);
       AssertEquals(CoordinateSystem, ResultArray[0].CoordinateSystem);
       AssertEquals(Ayanamsha, ResultArray[0].Ayanamsha);
       AssertEquals(HouseSystem, ResultArray[0].HouseSystem);
       AssertEquals(ObserverPosition, ResultArray[0].ObserverPosition);
       AssertEquals(ProjectionType, ResultArray[0].ProjectionType);
       // cleanup
       Dao.Delete(Id);

     end;

   procedure TestConfigurationsDao.ReadAll;
     var
       Dao: IConfigurationsDao;
       ResultArray: TConfigurationDtoArray;
       SortColumn: String = 'Name';
     begin
       Dao:= TFactoryConfigurationsDao.Create.GetInstance;
       ResultArray:= Dao.ReadAll(SortColumn);
       AssertTrue(Length(ResultArray) >= 1);
     end;


   procedure TestConfigurationsDao.Update;
     var
       Id: integer = 330;
       Name: String = 'ConfigName';
       Description: String = 'ConfigDescription';
       BaseOrbAspects: Double = 6.5;
       BaseOrbMidpoints: Double = 1.2;
       CoordinateSystem: Integer = 0;
       Ayanamsha: Integer = 1;
       HouseSystem: Integer = 2;
       ObserverPosition: Integer = 3;
       ProjectionType: Integer = 4;
       Dao: IConfigurationsDao;
       ResultArray: TConfigurationDtoArray;
       ResultText: String;
     begin
       // Write
       Dao:= TFactoryConfigurationsDao.Create.GetInstance;
       ResultText:= Dao.Insert(Id, Name, Description, BaseOrbAspects, BaseOrbMidpoints, CoordinateSystem, Ayanamsha,
                               HouseSystem, ObserverPosition, ProjectionType);
       // Update
       Dao.Update(Id, 'ConfigName2', 'ConfigDescription2', 6.9, 1.5, 4, 3, 2, 1 , 0);
       // Read
       ResultArray := Dao.Read(Id);
       // Compare
       AssertEquals('ConfigName2', ResultArray[0].Name);
       AssertEquals('ConfigDescription2', ResultArray[0].Description);
       AssertEquals(6.9, ResultArray[0].BaseOrbAspects);
       AssertEquals(1.5, ResultArray[0].BaseOrbMidpoints);
       AssertEquals(4, ResultArray[0].CoordinateSystem);
       AssertEquals(3, ResultArray[0].Ayanamsha);
       AssertEquals(2, ResultArray[0].HouseSystem);
       AssertEquals(1, ResultArray[0].ObserverPosition);
       AssertEquals(0, ResultArray[0].ProjectionType);
       // CleanUp
       Dao.Delete(Id);
     end;


   procedure TestConfigurationsDao.Delete;
     var
       Id: integer = 864;
       Name: String = 'ConfigName';
       Description: String = 'ConfigDescription';
       BaseOrbAspects: Double = 6.5;
       BaseOrbMidpoints: Double = 1.2;
       CoordinateSystem: Integer = 0;
       Ayanamsha: Integer = 1;
       HouseSystem: Integer = 2;
       ObserverPosition: Integer = 3;
       ProjectionType: Integer = 4;
       Dao: IConfigurationsDao;
       ResultArray: TConfigurationDtoArray;
     begin
       // Write
       Dao:= TFactoryConfigurationsDao.Create.GetInstance;
       Dao.Insert(Id, Name, Description, BaseOrbAspects, BaseOrbMidpoints, CoordinateSystem, Ayanamsha, HouseSystem,
                  ObserverPosition, ProjectionType);
       ResultArray := Dao.Read(Id);
       AssertEquals(Id, ResultArray[0].Id);
       // Delete;
       Dao.Delete(Id);
       ResultArray := Dao.Read(Id);
       AssertTrue(Length(ResultArray) = 0);
     end;


initialization
  RegisterTest('Daos',TestLookupValueDao);
  RegisterTest('Daos',TestVersionDao);
  RegisterTest('Daos',TestHouseSystemsDao);
  RegisterTest('Daos',TestAyanamshasDao);
  RegisterTest('Daos',TestBodiesDao);
  RegisterTest('Daos',TestAspectsDao);
  RegisterTest('Daos',TestConfigurationsDao);
end.

