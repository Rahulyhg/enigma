{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit DbTestUtils;
{ Utilities to support tests that use the database. }
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, sqldb, sqlite3conn, DbFrontend, DbUpdater;

const
  TestDatabase: String = 'enigma.db';

type

  TDataBaseGreenfield = class
    strict private
      DatabaseUpdater: IDatabaseUpdater;
      function RemoveDatabase: Boolean;
    public
      constructor Create;
  end;


implementation

uses DbUpdaterImpl, DbFrontendImpl;
const
  NewVersion = '0.4';


{ TDataBaseGreenfield }

{ Should be called once at the start of DAO tests. }
constructor TDataBaseGreenfield.Create;
begin
  if (RemoveDatabase) then
  begin
    DatabaseUpdater:= TDatabaseUpdater.Create(NewVersion);
    DatabaseUpdater.CreateDbVersion;
  end
  else
  begin
    raise Exception.Create('Could not create GreenField.');
  end;
end;

function TDataBaseGreenfield.RemoveDatabase: Boolean;
  begin
    if (FileExists(TestDatabase)) then
      begin
        Result:= DeleteFile(TestDatabase);
      end;
    Result:= true;
  end;

end.

