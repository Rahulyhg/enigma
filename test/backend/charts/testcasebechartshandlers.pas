unit TestCaseBeChartsHandlers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type
TestFullChartHandler = class(TTestCase) // Only an empty placeholder for now, Real unit testing would require
                                        // either mocks or fakes and they are quite combersome in FP. Maybe I should
                                        // use an integration test instead of a unit test.
published
  procedure TestHandleRequest;
end;

implementation

{ TestFullChartHandler }
procedure TestFullChartHandler.TestHandleRequest;
begin
      // function HandleRequest(PFullChartRequest: IFullChartRequest): IFullChartResponse;
end;


initialization

RegisterTest('ChartsHandlers',TestFullChartHandler);

end.

