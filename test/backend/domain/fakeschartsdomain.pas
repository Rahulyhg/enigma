{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit FakesChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ChartsDomain;

type
  FakeHorizontalPosition = class(TInterfacedObject, IHorizontalPosition)
    private
      function GetAzimuth: Double;
      function GetAltitude: Double;
    public
      property Altitude: Double read GetAltitude;
      property Azimuth: Double read GetAzimuth;
  end;

implementation

{ FakeHorizontalPosition }
function FakeHorizontalPosition.GetAzimuth: Double;
begin
  Result:= 100.0;
end;

function FakeHorizontalPosition.GetAltitude: Double;
begin
  Result:= 30.0;
end;

end.

