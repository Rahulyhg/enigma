{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseSharedXchgDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, SharedXchgDomain, SharedXchgDomainImpl;

type
TestValidatedLocation = class(TTestCase)
  published
    procedure TestConstructor;
end;

TestValidatedDate = class(TTestCase)
  published
    procedure TestConstructor;
  end;

TestValidatedTime = class(TTestCase)
  published
    procedure TestConstructor;
  end;

TestValidatedDouble = class(TTestCase)
published
  procedure TestConstructor;
end;

TestReferenceFrame = class(TTestCase)
published
  procedure TestConstructor;
  procedure TestFlagsCombined;
end;





implementation

const
  MARGIN = 0.000000001;

{ TestValidatedLocation }
procedure TestValidatedLocation.TestConstructor;
  var
    Name: String;
    Longitude, Latitude: Double;
    Location: IValidatedLocation;
  begin
    Name:='LocationName';
    Longitude:= 13.45;
    Latitude:= -22.33;
    Location:= TValidatedLocation.Create(Name, Longitude, Latitude);
    CheckEquals(Name, Location.Name);
    CheckEquals(Longitude, Location.Longitude, MARGIN);
    CheckEquals(Latitude, Location.Latitude, MARGIN);
  end;

  
{ TestValidatedDate }

procedure TestValidatedDate.TestConstructor;
  var
    Date: IValidatedDate;
    Year, Month, Day: Integer;
    Calendar: String;
    Valid: Boolean;
  begin
    Year:=1953;
    Month:=1;
    Day:=29;
    Calendar:='g';
    Valid:=True;
    Date:=TValidatedDate.Create(Year, Month, Day, Calendar, Valid);
    CheckEquals(Year, Date.Year, 'Year incorrect.');
    CheckEquals(Month, Date.Month, 'Month incorrect.');
    CheckEquals(Day, Date.Day, 'Date incorrect.');
    CheckEquals(Calendar, Date.Calendar, 'Calendar incorrect.');
    CheckEquals(Valid, Date.Valid, 'Valid incorrect.');
  end;

{ TestValidatedTime }

procedure TestValidatedTime.TestConstructor;
  var
    Time: IValidatedTime;
  begin
    Time:=TValidatedTime.Create(7.625, true);
    CheckEquals(7.625, Time.DecimalTime, 'Decimal time incorrect.');
    CheckTrue(Time.Valid, 'Valid incorrect.');
  end;

{ TestValidatedDouble }

procedure TestValidatedDouble.TestConstructor;
var
  Value2Test: IValidatedDouble;
  Value: Double;
  Valid: Boolean;
begin
  Value:=123.4567;
  Valid:=True;
  Value2Test:=TValidatedDouble.Create(Value, Valid);
  CheckEquals(Value, Value2Test.Value, MARGIN, 'Value incorrect.');
  CheckEquals(Valid, Value2Test.Valid, 'Valid incorrect.');
end;

{ TestReferenceFrame }

procedure TestReferenceFrame.TestConstructor;
var
  Frame: IReferenceFrame;
  Equatorial, Topocentric, Heliocentric, Sidereal: Boolean;
  Expected: LongInt;
begin
  Equatorial:= true;
  Topocentric:= false;
  Heliocentric:= false;
  Sidereal:= false;
  Expected:= 2048; //SEFLG_EQUATORIAL: Longint = (2 * 1024);
  Frame:= TReferenceFrame.Create(Equatorial, Topocentric, Heliocentric, Sidereal);
  AssertEquals(Expected, Frame.Flags);
end;

procedure TestReferenceFrame.TestFlagsCombined;
var
  Frame: IReferenceFrame;
  Equatorial, Topocentric, Heliocentric, Sidereal: Boolean;
  Expected: LongInt;
begin
  Equatorial:= false;
  Topocentric:= false;
  Heliocentric:= true;   // SEFLG_HELCTR: Longint     = 8;
  Sidereal:= true; //SEFLG_SIDEREAL: Longint = (64 * 1024);
  Expected:= 65544; // 8 or 65536;
  Frame:= TReferenceFrame.Create(Equatorial, Topocentric, Heliocentric, Sidereal);
  AssertEquals(Expected, Frame.Flags);
end;



initialization
  RegisterTest('SharedXchgDomain',TestValidatedLocation);
  RegisterTest('SharedXchgDomain',TestValidatedDate);
  RegisterTest('SharedXchgDomain',TestValidatedTime);
  RegisterTest('SharedXchgDomain',TestValidatedDouble);
  RegisterTest('SharedXchgDomain',TestReferenceFrame);
end.

