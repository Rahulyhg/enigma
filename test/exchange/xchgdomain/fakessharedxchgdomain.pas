unit FakesSharedXchgDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SharedXchgDomain;
type
FakeValidatedLocation = class(TInterfacedObject, IValidatedLocation)
  function GetName: String;
  function GetLongitude: Double;
  function GetLatitude: Double;
  property Name: String read GetName;
  property Longitude: Double read GetLongitude;
  property Latitude: Double read GetLatitude;
end;

FakeReferenceFrame = class(TInterfacedObject, IReferenceFrame)
  function GetFlags: LongInt;
end;

implementation

{ FakeValidatedLocation }
function FakeValidatedLocation.GetName: String;
begin
  Result:= 'MyLocation';
end;

function FakeValidatedLocation.GetLongitude: Double;
begin
  Result:= 5.0;
end;

function FakeValidatedLocation.GetLatitude: Double;
begin
  Result:= 50.0;
end;


{ FakeReferenceFrame }
function FakeReferenceFrame.GetFlags: LongInt;
begin
  Result:= 2048;
end;

end.

