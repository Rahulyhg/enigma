unit fakesdto;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dto;

type
  FakeLookUpValueDto = class (TInterfacedObject, ILookUpValueDto)
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
  end;

  FakeLookUpValueDto2 = class (TInterfacedObject, ILookUpValueDto)
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
  end;

  FakeVersionDto = class (TInterfacedObject, IVersionDto)
    function GetId: Integer;
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
  end;

  FakeVersionDto2 = class (TInterfacedObject, IVersionDto)
    function GetId: Integer;
    function GetMajor: Integer;
    function GetMinor: Integer;
    function GetMicro: Integer;
    function GetDate: String;
  end;

  FakeHouseSystemDto = class(TInterfacedObject, IHouseSystemDto)
     function Getid: Integer;
     function GetName: String;
     function GetDescription: String;
     function GetNrOfHouses: Integer;
     function IsCounterClockWise: Boolean;
     function IsQuadrantSystem: Boolean;
     function IsCuspIsStart: Boolean;
  end;

  FakeHouseSystemDto2 = class(TInterfacedObject, IHouseSystemDto)
     function Getid: Integer;
     function GetName: String;
     function GetDescription: String;
     function GetNrOfHouses: Integer;
     function IsCounterClockWise: Boolean;
     function IsQuadrantSystem: Boolean;
     function IsCuspIsStart: Boolean;
  end;

  FakeAyanamshaDto = class(TInterfacedObject, IAyanamshaDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Real;
  end;

  FakeAyanamshaDto2 = class(TInterfacedObject, IAyanamshaDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetOffset2000: Real;
  end;

  FakeBodyDto = class(TInterfacedObject, IBodyDto)
    function Getid: Integer;
    function GetName: String;
    function GetBodyCategory: Integer;
  end;

  FakeBodyDto2 = class(TInterfacedObject, IBodyDto)
    function Getid: Integer;
    function GetName: String;
    function GetBodyCategory: Integer;
  end;

  FakeAspectDto = class(TInterfacedObject, IAspectDto)
    function Getid: Integer;
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
  end;

  FakeAspectDto2 = class(TInterfacedObject, IAspectDto)
    function Getid: Integer;
    function GetName: String;
    function GetAngle: Double;
    function GetAspectCategory: Integer;
  end;

  FakeConfigurationDto = class(TInterfacedObject, IConfigurationDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
  end;

  FakeConfigurationDto2 = class(TInterfacedObject, IConfigurationDto)
    function Getid: Integer;
    function GetName: String;
    function GetDescription: String;
    function GetBaseOrbAspects: Double;
    function GetBaseOrbMidpoints: Double;
    function GetCoordinateSystem: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystem: integer;
    function GetObserverPosition: Integer;
    function GetProjectionType: Integer;
  end;

  FakeGlyphsBodiesDto = class(TInterfacedObject, IGlyphsBodiesDto)
    function GetId: LongInt;
    function GetGlyph: String;
  end;

  FakeGlyphsBodiesDto2 = class(TInterfacedObject, IGlyphsBodiesDto)
    function GetId: LongInt;
    function GetGlyph: String;
  end;

  FakeGlyphsConfigurationDto = class(TInterfacedObject, IGlyphsConfigurationDto)
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
  end;

  FakeGlyphsConfigurationDto2 = class(TInterfacedObject, IGlyphsConfigurationDto)
    function GetConfigId: LongInt;
    function GetItemId: LongInt;
    function GetCategory: String;
    function GetGlyph: String;
  end;

implementation

  { FakeLookUpValueDto }
  function FakeLookUpValueDto.GetId: Integer;
    begin
      Result:= 21;
    end;

  function FakeLookUpValueDto.GetName: String;
    begin
      Result:= 'MyName';
    end;

  function FakeLookUpValueDto.GetDescription: String;
    begin
      Result:= 'MyDescription';
    end;


  { FakeLookUpValueDto2 }
  function FakeLookUpValueDto2.GetId: Integer;
    begin
      Result:= 22;
    end;

  function FakeLookUpValueDto2.GetName: String;
    begin
      Result:= 'MyName2';
    end;

  function FakeLookUpValueDto2.GetDescription: String;
    begin
      Result:= 'MyDescription2';
    end;


  { FakeVersionDto }

  function FakeVersionDto.GetId: Integer;
    begin
      Result:= 1;
    end;

  function FakeVersionDto.GetMajor: Integer;
    begin
      Result:= 3;
    end;

  function FakeVersionDto.GetMinor: Integer;
    begin
      Result:= 2;
    end;

  function FakeVersionDto.GetMicro: Integer;
    begin
      Result:= 1;
    end;

  function FakeVersionDto.GetDate: String;
    begin
      Result:= '2000-01-01';
    end;


  { FakeVersionDto2 }

  function FakeVersionDto2.GetId: Integer;
    begin
      Result:= 12;
    end;

  function FakeVersionDto2.GetMajor: Integer;
    begin
      Result:= 32;
    end;

  function FakeVersionDto2.GetMinor: Integer;
    begin
      Result:= 22;
    end;

  function FakeVersionDto2.GetMicro: Integer;
    begin
      Result:= 12;
    end;

  function FakeVersionDto2.GetDate: String;
    begin
      Result:= '2000-01-02';
    end;

  { FakeHouseSystemDto }
  function FakeHouseSystemDto.Getid: Integer;
    begin
      Result:= 100;
    end;

  function FakeHouseSystemDto.GetName: String;
    begin
      Result:= 'HouseSystemName';
    end;

  function FakeHouseSystemDto.GetDescription: String;
    begin
      Result:= 'HouseSystemDescription';
    end;

  function FakeHouseSystemDto.GetNrOfHouses: Integer;
    begin
      Result:= 12;
    end;

  function FakeHouseSystemDto.IsCounterClockWise: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto.IsQuadrantSystem: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;


  { FakeHouseSystemDto2 }
  function FakeHouseSystemDto2.Getid: Integer;
    begin
      Result:= 102;
    end;

  function FakeHouseSystemDto2.GetName: String;
    begin
      Result:= 'HouseSystemName2';
    end;

  function FakeHouseSystemDto2.GetDescription: String;
    begin
      Result:= 'HouseSystemDescription2';
    end;

  function FakeHouseSystemDto2.GetNrOfHouses: Integer;
    begin
      Result:= 22;
    end;

  function FakeHouseSystemDto2.IsCounterClockWise: Boolean;
    begin
      Result:= true;
    end;

  function FakeHouseSystemDto2.IsQuadrantSystem: Boolean;
    begin
      Result:= false;
    end;

  function FakeHouseSystemDto2.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;

  { FakeAyanamshaDto }
  function FakeAyanamshaDto.Getid: Integer;
    begin
      Result:= 400;
    end;

  function FakeAyanamshaDto.GetName: String;
    begin
      Result:= 'AyanamshaName';
    end;

  function FakeAyanamshaDto.GetDescription: String;
    begin
      Result:= 'AyanamshaDescription';
    end;

  function FakeAyanamshaDto.GetOffset2000: Real;
    begin
      Result:= 29.123;
    end;

  { FakeAyanamshaDto2 }
  function FakeAyanamshaDto2.Getid: Integer;
    begin
      Result:= 402;
    end;

  function FakeAyanamshaDto2.GetName: String;
    begin
      Result:= 'AyanamshaName2';
    end;

  function FakeAyanamshaDto2.GetDescription: String;
    begin
      Result:= 'AyanamshaDescription2';
    end;

  function FakeAyanamshaDto2.GetOffset2000: Real;
    begin
      Result:= 30.123;
    end;

  { FakeBodyDto }
  function FakeBodyDto.Getid: Integer;
    begin
      Result:= 777;
    end;

  function FakeBodyDto.GetName: String;
    begin
      Result:= 'BodyName';
    end;

  function FakeBodyDto.GetBodyCategory: Integer;
    begin
      Result:= 5;
    end;

  { FakeBodyDto2 }
  function FakeBodyDto2.Getid: Integer;
    begin
      Result:= 772;
    end;

  function FakeBodyDto2.GetName: String;
    begin
      Result:= 'BodyName2';
    end;

  function FakeBodyDto2.GetBodyCategory: Integer;
    begin
      Result:= 2;
    end;

  { FakeAspectDto }
  function FakeAspectDto.Getid: Integer;
    begin
      Result:= 799;
    end;

  function FakeAspectDto.GetName: String;
    begin
      Result:= 'AspectName';
    end;

  function FakeAspectDto.GetAngle: Double;
    begin
      Result:= 22.33;
    end;

  function FakeAspectDto.GetAspectCategory: Integer;
    begin
      Result:= 1;
    end;

  { FakeAspectDto2 }
  function FakeAspectDto2.Getid: Integer;
    begin
      Result:= 792;
    end;

  function FakeAspectDto2.GetName: String;
    begin
      Result:= 'AspectName2';
    end;

  function FakeAspectDto2.GetAngle: Double;
    begin
      Result:= 33.22;
    end;

  function FakeAspectDto2.GetAspectCategory: Integer;
    begin
      Result:= 2;
    end;

  { FakeConfigurationDto }
  function FakeConfigurationDto.Getid: Integer;
    begin
      Result:= 2;
    end;

  function FakeConfigurationDto.GetName: String;
    begin
      Result:= 'ConfigName1';
    end;

  function FakeConfigurationDto.GetDescription: String;
    begin
      Result:= 'ConfigDescription1';
    end;

  function FakeConfigurationDto.GetBaseOrbAspects: Double;
    begin
      Result:= 7.1;
    end;

  function FakeConfigurationDto.GetBaseOrbMidpoints: Double;
    begin
      Result:= 1.1;
    end;

  function FakeConfigurationDto.GetCoordinateSystem: Integer;
    begin
      Result:= 1;
    end;

  function FakeConfigurationDto.GetAyanamsha: Integer;
    begin
      Result:= 11;
    end;

  function FakeConfigurationDto.GetHouseSystem: integer;
    begin
      Result:= 21;
    end;

  function FakeConfigurationDto.GetObserverPosition: Integer;
    begin
      Result:= 3;
    end;

  function FakeConfigurationDto.GetProjectionType: Integer;
    begin
      Result:= 4;
    end;

  { FakeConfigurationDto2 }
   function FakeConfigurationDto2.Getid: Integer;
     begin
       Result:= 3;
     end;

   function FakeConfigurationDto2.GetName: String;
     begin
       Result:= 'ConfigName2';
     end;

   function FakeConfigurationDto2.GetDescription: String;
     begin
       Result:= 'ConfigDescription2';
     end;

   function FakeConfigurationDto2.GetBaseOrbAspects: Double;
     begin
       Result:= 7.2;
     end;

   function FakeConfigurationDto2.GetBaseOrbMidpoints: Double;
     begin
       Result:= 1.2;
     end;

   function FakeConfigurationDto2.GetCoordinateSystem: Integer;
     begin
       Result:= 2;
     end;

   function FakeConfigurationDto2.GetAyanamsha: Integer;
     begin
       Result:= 12;
     end;

   function FakeConfigurationDto2.GetHouseSystem: integer;
     begin
       Result:= 22;
     end;

   function FakeConfigurationDto2.GetObserverPosition: Integer;
     begin
       Result:= 1;
     end;

   function FakeConfigurationDto2.GetProjectionType: Integer;
     begin
       Result:= 2;
     end;

  { FakeGlyphsBodiesDto }
    function FakeGlyphsBodiesDto.GetId: LongInt;
      begin
        Result:= 3;
      end;

    function FakeGlyphsBodiesDto.GetGlyph: String;
      begin
        Result:= 'z';
      end;

  { FakeGlyphsBodiesDto2 }
    function FakeGlyphsBodiesDto2.GetId: LongInt;
      begin
        Result:= 8;
      end;

    function FakeGlyphsBodiesDto2.GetGlyph: String;
      begin
        Result:= 'y';
      end;

  { FakeGlyphsConfigurationDto }
    function FakeGlyphsConfigurationDto.GetConfigId: LongInt;
      begin
        Result:= 0;
      end;

    function FakeGlyphsConfigurationDto.GetItemId: LongInt;
      begin
        Result:= 3;
      end;

    function FakeGlyphsConfigurationDto.GetCategory: String;
      begin
        Result:= 'A';
      end;

    function FakeGlyphsConfigurationDto.GetGlyph: String;
      begin
        Result:= 'W';
      end;

  { FakeGlyphsConfigurationDto2 }
    function FakeGlyphsConfigurationDto2.GetConfigId: LongInt;
      begin
        Result:= 1;
      end;

    function FakeGlyphsConfigurationDto2.GetItemId: LongInt;
      begin
        Result:= 4;
      end;

    function FakeGlyphsConfigurationDto2.GetCategory: String;
      begin
        Result:= 'S';
      end;

    function FakeGlyphsConfigurationDto2.GetGlyph: String;
      begin
        Result:= 'P';
      end;


end.

