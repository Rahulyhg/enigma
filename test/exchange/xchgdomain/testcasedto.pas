unit TestCaseDto;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, TestDecorator, Dto;

type

  TestLookUpValueDto = class(TTestCase)
    strict private
      Id: Integer;
      Name, Description: String;
      Dto: ILookUpValueDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestDescription;
    end;

  TestVersionDto = class(TTestCase)
    strict private
      Id, Major, Minor, Micro: Integer;
      Date: String;
      Dto: IVersionDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestMajor;
      procedure TestMinor;
      procedure TestMicro;
      procedure TestDate;
  end;

  TestHouseSystemDto = class(TTestCase)
    strict private
      Id, CounterClockWise, CuspIsStart, QuadrantSystem, NrOfHouses: Integer;
      Name, Description: String;
      Dto: IHouseSystemDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestDescription;
      procedure TestNrOfHouses;
      procedure TestCounterClockWise;
      procedure TestQuadrantSystem;
      procedure TestCuspIsStart;
  end;

  TestAyanamshaDto = class(TTestCase)
    strict private
      Id: Integer;
      Name, Description: String;
      Offset2000: Real;
      Dto: IAyanamshaDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestDescription;
      procedure TestOffset2000;
  end;

  TestBodyDto = class(TTestCase)
    strict private
      Id, BodyCategory: Integer;
      Name: String;
      Dto: IBodyDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestBodyCategory;
  end;

  TestAspectDto = class(TTestCase)
    strict private
      Id, AspectCategory: Integer;
      Name: String;
      Angle: Double;
      Dto: IAspectDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestAngle;
      procedure TestAspectCategory;
  end;

  TestConfigurationDto = class(TTestCase)
    strict private
      Id, CoordinateSystem, Ayanamsha, HouseSystem, ObserverPosition, ProjectionType: Integer;
      Name, Description: String;
      BaseOrbAspects, BaseOrbMidpoints: Double;
      Dto: IConfigurationDto;
    published
      procedure SetUp; override;
      procedure TestId;
      procedure TestName;
      procedure TestDescription;
      procedure TestBaseOrbAspects;
      procedure TestBaseOrbMidpoints;
      procedure TestCoordinateSystem;
      procedure TestAyanamsha;
      procedure TestHouseSystem;
      procedure TestObserverPosition;
      procedure TestProjectionType;
  end;

  TestGlyphsBodiesDto = class(TTestCase)
    strict private
      BodyId: Integer;
      Glyph: String;
      Dto: IGlyphsBodiesDto;
    published
      procedure SetUp; override;
      procedure TestBodyId;
      procedure TestGlyph;
  end;

  TestGlyphsConfigurationDto = class(TTestCase)
    strict private
      ConfigId, ItemId: LongInt;
      Glyph, Category: String;
      Dto: IGlyphsConfigurationDto;
    published
      procedure SetUp; override;
      procedure TestConfigId;
      procedure TestItemId;
      procedure TestCategory;
      procedure TestGlyph;
  end;


implementation

uses
  DtoImpl, FakesDto;

const
  MARGIN = 0.000000001;


  { TestLookUpValueDto }

  procedure TestLookUpValueDto.Setup;
    begin
      Id:= 22;
      Name:= 'Name';
      Description:= 'LookUpValueDescription';
      Dto:= TLookUpValueDto.Create(Id, Name, Description);
    end;

  procedure TestLookUpValueDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestLookUpValueDto.TestName;
    begin
      AssertEquals(Name, Dto.Name);
    end;

  procedure TestLookUpValueDto.TestDescription;
    begin
      AssertEquals(Description, Dto.Description);
    end;

  { TestVersionDto }

  procedure TestVersionDto.Setup;
    begin
      Id:= 100;
      Major:= 10;
      Minor:= 3;
      Micro:= 1;
      Date:= '2020-01-01';
      Dto:= TVersionDto.Create(Id, Major, Minor, Micro, Date);
    end;

  procedure TestVersionDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestVersionDto.TestMajor;
    begin
      AssertEquals(Major, Dto.Major);
    end;

  procedure TestVersionDto.TestMinor;
    begin
      AssertEquals(Minor, Dto.Minor);
    end;

  procedure TestVersionDto.TestMicro;
    begin
      AssertEquals(Micro, Dto.Micro);
    end;

  procedure TestVersionDto.TestDate;
    begin
      AssertEquals(Date, Dto.Date);
    end;

  { TestHouseSystemDto }

  procedure TestHouseSystemDto.SetUp;
    begin
      Id:= 33;
      Name:= 'Name';
      Description:= 'Description';
      NrOfHouses:= 16;
      CounterClockWise:= 1;
      CuspIsStart:= 1;
      QuadrantSystem:= 0;
      Dto:= THouseSystemDto.Create(Id, Name, Description, NrOfHouses, CounterClockWise, QuadrantSystem, CuspIsStart);
    end;

  procedure TestHouseSystemDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestHouseSystemDto.TestName;
    begin
       AssertEquals(Name, Dto.Name);
    end;

  procedure TestHouseSystemDto.TestDescription;
    begin
       AssertEquals(Description, Dto.Description);
    end;

  procedure TestHouseSystemDto.TestNrOfHouses;
    begin
       AssertEquals(NrOfHouses, Dto.NrOfHouses);
    end;

  procedure TestHouseSystemDto.TestCounterClockWise;
    begin
       AssertTrue(Dto.CounterClockWise);
    end;

  procedure TestHouseSystemDto.TestQuadrantSystem;
    begin
       AssertFalse(Dto.QuadrantSystem);
    end;

  procedure TestHouseSystemDto.TestCuspIsStart;
    begin
       AssertTrue(Dto.CuspIsStart);
    end;

  { TestAyanamshaDto }

  procedure TestAyanamshaDto.SetUp;
    begin
      Id:= 200;
      Name:= 'Name';
      Description:= 'Description';
      Offset2000:= 25.1234;
      Dto:= TAyanamshaDto.Create(Id, Name, Description, Offset2000);
    end;

  procedure TestAyanamshaDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestAyanamshaDto.TestName;
    begin
      AssertEquals(Name, Dto.Name);
    end;

  procedure TestAyanamshaDto.TestDescription;
    begin
      AssertEquals(Description, Dto.Description);
    end;

  procedure TestAyanamshaDto.TestOffset2000;
    begin
      AssertEquals(Offset2000, Dto.Offset2000, MARGIN);
    end;

  { TestBodyDto }
  procedure TestBodyDto.SetUp;
    begin
      Id:= 400;
      Name:= 'Name';
      BodyCategory:= 3;
      Dto:= TBodyDto.Create(Id, Name, BodyCategory);
    end;

  procedure TestBodyDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestBodyDto.TestName;
    begin
      AssertEquals(Name, Dto.Name);
    end;

  procedure TestBodyDto.TestBodyCategory;
    begin
      AssertEquals(BodyCategory, Dto.BodyCategory);
    end;

  { TestAspectDto }

  procedure TestAspectDto.SetUp;
    begin
      Id:= 500;
      Name:= 'Name';
      Angle:= 72.72;
      AspectCategory:= 5;
      Dto:= TAspectDto.Create(Id, Name, Angle, AspectCategory);
    end;

  procedure TestAspectDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestAspectDto.TestName;
    begin
      AssertEquals(Name, Dto.Name);
    end;

  procedure TestAspectDto.TestAngle;
    begin
      AssertEquals(Angle, Dto.Angle, MARGIN);
    end;

  procedure TestAspectDto.TestAspectCategory;
    begin
      AssertEquals(AspectCategory, Dto.AspectCategory);
    end;

  { TestConfigurationDto }

  procedure TestConfigurationDto.SetUp;
    begin
      Id:= 3;
      Name:= 'Name';
      Description:= 'Description';
      BaseOrbAspects:= 7.5;
      BaseOrbMidpoints:= 1.6;
      CoordinateSystem:= 1;
      Ayanamsha:= 0;
      HouseSystem:= 4;
      ObserverPosition:= 3;
      ProjectionType:= 2;
      Dto:= TConfigurationDto.Create(Id, Name, Description, BaseOrbAspects, BaseOrbMidpoints,
                                     CoordinateSystem, Ayanamsha, HouseSystem, ObserverPosition, ProjectionType);
    end;

  procedure TestConfigurationDto.TestId;
    begin
      AssertEquals(Id, Dto.Id);
    end;

  procedure TestConfigurationDto.TestName;
    begin
      AssertEquals(Name, Dto.Name);
    end;

  procedure TestConfigurationDto.TestDescription;
    begin
      AssertEquals(Description, Dto.Description);
    end;

  procedure TestConfigurationDto.TestBaseOrbAspects;
    begin
      AssertEquals(BaseOrbAspects, Dto.BaseOrbAspects, MARGIN);
    end;

  procedure TestConfigurationDto.TestBaseOrbMidpoints;
    begin
      AssertEquals(BaseOrbMidpoints, Dto.BaseOrbMidpoints);
    end;

  procedure TestConfigurationDto.TestCoordinateSystem;
    begin
      AssertEquals(CoordinateSystem, Dto.CoordinateSystem);
    end;

  procedure TestConfigurationDto.TestAyanamsha;
    begin
      AssertEquals(Ayanamsha, Dto.Ayanamsha);
    end;

  procedure TestConfigurationDto.TestHouseSystem;
    begin
      AssertEquals(HouseSystem, Dto.HouseSystem);
    end;

  procedure TestConfigurationDto.TestObserverPosition;
    begin
      AssertEquals(ObserverPosition, Dto.ObserverPosition);
    end;

  procedure TestConfigurationDto.TestProjectionType;
    begin
      AssertEquals(ProjectionType, Dto.ProjectionType);
    end;

  { TestGlyphsBodiesDto }

  procedure TestGlyphsBodiesDto.SetUp;
    begin
      BodyId:= 3;
      Glyph:= 'X';
      Dto:= TGlyphsBodiesDto.Create(BodyId, Glyph);
    end;

  procedure TestGlyphsBodiesDto.TestBodyId;
    begin
      AssertEquals(BodyId, Dto.BodyId);
    end;

  procedure TestGlyphsBodiesDto.TestGlyph;
    begin
      AssertEquals(Glyph, Dto.Glyph);
    end;

  { TestGlyphsConfigurationDto }
  procedure TestGlyphsConfigurationDto.SetUp;
    begin
      ConfigId:= 0;
      ItemId:= 5;
      Category:= 'O';
      Glyph:= 'e';
      Dto:= TGlyphsConfigurationDto.Create(ConfigId, ItemId, Category, Glyph);
    end;

  procedure TestGlyphsConfigurationDto.TestConfigId;
    begin
      AssertEquals(ConfigId, Dto.ConfigId);
    end;

  procedure TestGlyphsConfigurationDto.TestItemId;
    begin
      AssertEquals(ItemId, Dto.ItemId);
    end;

  procedure TestGlyphsConfigurationDto.TestCategory;
    begin
      AssertEquals(Category, Dto.Category);
    end;

  procedure TestGlyphsConfigurationDto.TestGlyph;
    begin
      AssertEquals(Glyph, Dto.Glyph);
    end;

initialization
  RegisterTest('Exchange-Dto',TestLookUpValueDto);
  RegisterTest('Exchange-Dto',TestVersionDto);
  RegisterTest('Exchange-Dto',TestHouseSystemDto);
  RegisterTest('Exchange-Dto',TestAyanamshaDto);
  RegisterTest('Exchange-Dto',TestBodyDto);
  RegisterTest('Exchange-Dto',TestAspectDto);
  RegisterTest('Exchange-Dto',TestConfigurationDto);
  RegisterTest('Exchange-Dto',TestGlyphsBodiesDto);
  RegisterTest('Exchange-Dto',TestGlyphsConfigurationDto);

end.

