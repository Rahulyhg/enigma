unit FakesXChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XChartsDomain, SharedXchgDomain;

type
  FakeCelestialObjectSimple = class(TInterfacedObject, ICelestialObjectSimple)
    function GetMainPos: Double;
    function GetDeviationPos: Double;
    function GetDistancePos: Double;
    function GetMainSpeed: Double;
    function GetDeviationSpeed: Double;
    function GetDistanceSpeed: Double;
  end;

  FakeCelestialObjectFull = class (TInterfacedObject, ICelestialObjectFull)
    function GetObjectId: Integer;
    function GetEclipticalPos: ICelestialObjectSimple;
    function GetEquatorialPos: ICelestialObjectSimple;
    function GetHorizontalPos: ICelestialObjectSimple;
 end;

  FakeHouseSystemSpec = class(TInterfacedObject, IHouseSystemSpec)
    function GetName: String;
    function GetSeId: String;
    function GetDescription: String;
    function GetId: Integer;
    function GetNumberOfHouses: Integer;
    function IsQuadrantSystem: Boolean;
    function IsCuspIsStart: Boolean;
  end;

  FakeHousePositionFull = class(TInterfacedObject, IHousePositionFull)
    function GetLongitude: Double;
    function GetRightAscension: Double;
    function GetDeclination: Double;
    function GetAzimuth: Double;
    function GetAltitude: Double;
  end;

  FakeCalculationSettings = class(TInterfacedObject, ICalculationSettings)
    function GetPosition: Integer;
    function GetAyanamsha: Integer;
    function GetHouseSystemSpec: IHouseSystemSpec;
    function GetObjects: TIntArray;
    function GetReferenceFrame: IReferenceFrame;
  end;

  FakeHousesResponse = class(TInterfacedObject, IHousesResponse)
    function GetMc: IHousePositionFull;
    function GetAsc: IHousePositionFull;
    function GetCusps: THousePositionFullArray;
    function GetHouseSystemSpec: IHouseSystemSpec;
  end;

  FakeFullChartRequest = class(TInterfacedObject, IFullChartRequest)
    function GetName: String;
    function GetLocation: IValidatedLocation;
    function GetDate: IValidatedDate;
    function GetTime: IValidatedTime;
    function GetCalculationSettings: ICalculationSettings;
  end;

  FakeFullChartResponse = class(TInterfacedObject, IFullChartResponse)
    function GetName: String;
    function GetAllObjects: TCelestialObjectFullArray;
    function GetHouses: IHousesResponse;
    function GetFullChartRequest: IFullChartRequest;
  end;

  FakeFullChartResponse2 = class(TInterfacedObject, IFullChartResponse)
    function GetName: String;
    function GetAllObjects: TCelestialObjectFullArray;
    function GetHouses: IHousesResponse;
    function GetFullChartRequest: IFullChartRequest;
  end;

implementation

uses FakesSharedXchgDomain, SharedXchgDomainImpl;

{ FakeCelestialObjectSimple }

  function FakeCelestialObjectSimple.GetMainPos: Double;
    begin
      Result:= 60.0;
    end;

  function FakeCelestialObjectSimple.GetDeviationPos: Double;
    begin
      Result:= 1.0;
    end;

  function FakeCelestialObjectSimple.GetDistancePos: Double;
    begin
      Result:= 5.0;
    end;

  function FakeCelestialObjectSimple.GetMainSpeed: Double;
    begin
      Result:= 0.5;
    end;

  function FakeCelestialObjectSimple.GetDeviationSpeed: Double;
    begin
      Result:= -0.04;
    end;

  function FakeCelestialObjectSimple.GetDistanceSpeed: Double;
    begin
      Result:= 0.01;
    end;

{ FakeCelestialObjectFull }
  function FakeCelestialObjectFull.GetObjectId: Integer;
    begin
      Result:= 6;
    end;

  function FakeCelestialObjectFull.GetEclipticalPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;

  function FakeCelestialObjectFull.GetEquatorialPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;

  function FakeCelestialObjectFull.GetHorizontalPos: ICelestialObjectSimple;
    begin
      Result:= FakeCelestialObjectSimple.Create;
    end;



{ FakeHouseSystemSpec }
  function FakeHouseSystemSpec.GetName: String;
    begin
      Result:= 'MySystem';
    end;

  function FakeHouseSystemSpec.GetSeId: String;
    begin
      Result:= 'M';
    end;

  function FakeHouseSystemSpec.GetDescription: String;
    begin
      Result:= 'MyDescription';
    end;

  function FakeHouseSystemSpec.GetId: Integer;
    begin
      Result:= 12;
    end;

  function FakeHouseSystemSpec.GetNumberOfHouses: Integer;
    begin
      Result:= 16;
    end;

  function FakeHouseSystemSpec.IsQuadrantSystem: Boolean;
    begin
      Result:= false;
    end;

  function FakeHouseSystemSpec.IsCuspIsStart: Boolean;
    begin
      Result:= true;
    end;


{ FakeHousePositionFull }
  function FakeHousePositionFull.GetLongitude: Double;
    begin
      Result:= 111.22;
    end;

  function FakeHousePositionFull.GetRightAscension: Double;
    begin
      Result:= 112.33;
    end;

  function FakeHousePositionFull.GetDeclination: Double;
    begin
      Result:= -1.44;
    end;

  function FakeHousePositionFull.GetAzimuth: Double;
    begin
      Result:= 88.88;
    end;


  function FakeHousePositionFull.GetAltitude: Double;
    begin
      Result:= 13.46;
    end;


{ FakeCalculationSettings }
  function FakeCalculationSettings.GetPosition: Integer;
    begin
      Result:= 1;
    end;

  function FakeCalculationSettings.GetAyanamsha: Integer;
    begin
      Result:= 0;
    end;

  function FakeCalculationSettings.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= FakeHouseSystemSpec.Create;
    end;

  function FakeCalculationSettings.GetObjects: TIntArray;
    var
      Objects: TIntArray;
    begin
      SetLength(Objects, 2);
      Objects[0]:= 3;
      Objects[1]:= 4;
      Result:= Objects;
    end;

  function FakeCalculationSettings.GetReferenceFrame: IReferenceFrame;
    begin
      Result:= FakeReferenceFrame.Create;
    end;

{ FakeHousesResponse }
  function FakeHousesResponse.GetMc: IHousePositionFull;
    begin
      Result:= FakeHousePositionFull.Create;
    end;

  function FakeHousesResponse.GetAsc: IHousePositionFull;
    begin
      Result:= FakeHousePositionFull.Create;
    end;

  function FakeHousesResponse.GetCusps: THousePositionFullArray;
    var
      HousePositions: THousePositionFullArray;
    begin
      SetLength(HousePositions, 1);
      HousePositions[0] := FakeHousePositionFull.Create;
      Result:= HousePositions;
    end;

  function FakeHousesResponse.GetHouseSystemSpec: IHouseSystemSpec;
    begin
      Result:= FakeHouseSystemSpec.Create;;
    end;

{ FakeFullChartRequest }
  function FakeFullChartRequest.GetName: String;
    begin
      Result:= 'TheName';
    end;

  function FakeFullChartRequest.GetLocation: IValidatedLocation;
    begin
      Result:= FakeValidatedLocation.Create;
    end;

  function FakeFullChartRequest.GetDate: IValidatedDate;
    begin
      Result:= TValidatedDate.Create(2018, 9, 6, 'g', true);
    end;

  function FakeFullChartRequest.GetTime: IValidatedTime;
    begin
      Result:= TValidatedTime.Create(2.25, true);
    end;

  function FakeFullChartRequest.GetCalculationSettings: ICalculationSettings;
    begin
      Result:= FakeCalculationSettings.Create;
    end;

{ FakeFullChartResponse }
  function FakeFullChartResponse.GetName: String;
    begin
      Result:= 'Full Chart Name';
    end;

  function FakeFullChartResponse.GetAllObjects: TCelestialObjectFullArray;
    var
      ObjectArray: TCelestialObjectFullArray;
    begin
      SetLength(ObjectArray, 1);
      ObjectArray[0]:= FakeCelestialObjectFull.Create;
      Result:= ObjectArray;
    end;

  function FakeFullChartResponse.GetHouses: IHousesResponse;
    begin
      Result:= FakeHousesResponse.Create;
    end;

  function FakeFullChartResponse.GetFullChartRequest: IFullChartRequest;
    begin
      Result:= FakeFullChartRequest.Create;
    end;

  { FakeFullChartResponse2 }
    function FakeFullChartResponse2.GetName: String;
      begin
        Result:= 'Full Chart 2 Name';
      end;

    function FakeFullChartResponse2.GetAllObjects: TCelestialObjectFullArray;
      var
        ObjectArray: TCelestialObjectFullArray;
      begin
        SetLength(ObjectArray, 1);
        ObjectArray[0]:= FakeCelestialObjectFull.Create;
        Result:= ObjectArray;
      end;

    function FakeFullChartResponse2.GetHouses: IHousesResponse;
      begin
        Result:= FakeHousesResponse.Create;
      end;

    function FakeFullChartResponse2.GetFullChartRequest: IFullChartRequest;
      begin
        Result:= FakeFullChartRequest.Create;
      end;

end.

