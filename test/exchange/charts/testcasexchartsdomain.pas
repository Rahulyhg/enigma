{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit testcaseXChartsDomain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, XChartsDomain, SharedXchgDomain, SharedXchgDomainImpl;

type

  TestHouseSystemSpec = class(TTestCase)
    procedure SetUp; override;
    procedure TestName;
    procedure TestSeId;
    procedure TestDescription;
    procedure TestId;
    procedure TestNumberOfHouses;
    procedure TestQuadrantSystem;
    procedure TestCuspIsStart;
  end;

  TestHousePositionFull = class(TTestCase)
    published
      procedure SetUp; override;
      procedure TestLongitude;
      procedure TestRightAscension;
      procedure TestDeclination;
      procedure TestAzimuth;
      procedure TestAltitude;
    end;

  TestCelestialObjectSimple = class(TTestCase)
    published
      procedure SetUp; override;
      procedure TestMainPos;
      procedure TestDeviationPos;
      procedure TestDistancePos;
      procedure TestMainSpeed;
      procedure TestDeviationSpeed;
      procedure TestDistanceSpeed;
  end;

  TestCelestialObjectFull = class(TTestCase)
    published
      procedure SetUp; override;
      procedure TestObjectId;
      procedure TestEclipticPositions;
      procedure TestEquatorialPositions;
      procedure TestHorizontalPositions;
    end;

  TestHousesResponse = class(TTestCase)
    published
      procedure SetUp; override;
      procedure GetMc;
      procedure GetAsc;
      procedure GetCusps;
      procedure GetHouseSystemSpec;
    end;

  TestCalculationSettings = class(TTestCase)
    published
      procedure SetUp; override;
      procedure TestPosition;
      procedure TestAyanamsha;
      procedure TestSeId;
      procedure TestReferenceFrame;
      procedure TestObjects;
      procedure TestFlags;
    end;

  TestFullChartRequest = class(TTestCase)
    published
      procedure SetUp; override;
      procedure GetName;
      procedure GetLocation;
      procedure GetDate;
      procedure GetTime;
      procedure GetCalculationSettings;
    end;

  TestFullChartResponse = class(TTestCase)
    published
      procedure SetUp; override;
      procedure GetName;
      procedure GetAllObjects;
      procedure GetHouses;
      procedure GetFullChartRequest;
    end;


implementation

uses
  XChartsDomainImpl, FakesSharedXchgDomain, FakesXChartsDomain;

const
  MARGIN = 0.000000001;

var
  HouseSystemSpec: IHouseSystemSpec;
  HouseSystemSpecName: String = 'MyName';
  HouseSystemSpecSeId: String = 'MySeId';
  HouseSystemSpecDescription: String = 'MyDescrription';
  HouseSystemSpecId: Integer = 1;
  HouseSystemSpecNumberOfHouses: Integer = 8;
  HouseSystemSpecQuadrantSystem: Boolean = false;
  HouseSystemSpecCuspIsStart: Boolean = true;

  HousePositionFull: IHousePositionFull;
  HousePositionFullLongitude: Double = 303.03;
  HousePositionFullRightAscension: Double = 304.4;
  HousePositionFullDeclination: Double = -12.12;
  HousePositionFullAzimuth: Double = 200.20;
  HousePositionFullAltitude: Double = -40.40;

  CelestialObjectSimple: ICelestialObjectSimple;
  CelestialObjectSimpleMainPos: Double = 123.456;
  CelestialObjectSimpleDeviationPos: Double = -2.2222;
  CelestialObjectSimpleDistancePos: Double = 12.121212;
  CelestialObjectSimpleMainSpeed: Double = 0.218765;
  CelestialObjectSimpleDeviationSpeed: Double = -0.00123;
  CelestialObjectSimpleDistanceSpeed: Double = 0.000876;

  CelestialObjectFull: ICelestialObjectFull;
  CelestialObjectFullObjectId: Integer = 42;
  CelestialObjectFullEclipticalPos: ICelestialObjectSimple;
  CelestialObjectFullEquatorialPos: ICelestialObjectSimple;
  CelestialObjectFullHorizontalPos: ICelestialObjectSimple;

  Calculationsettings: ICalculationSettings;
  CalculationSettingsPosition: Integer = 22;
  CalculationSettingsAyanamsha: Integer = 3;
  CalculationSettingsObjects: TIntArray;
  CalculationSettingsReferenceFrame: IReferenceFrame;
  CalculationSettingsHouseSystem: IHouseSystemSpec;

  HousesResponse: IHousesResponse;
  FullChartRequest: IFullChartRequest;
  FullChartResponse: IFullChartResponse;


  function CreateCelestialObjectFull: TCelestialObjectFull;
    var
      EclObject, EquObject, HorObject: TCelestialObjectSimple;
      ObjectId: Integer;
      CelObject: TCelestialObjectFull;
      Positions: Array of Double;
    begin
      SetLength(Positions, 6);
      Positions[0]:= 100.0;
      Positions[1]:= 1.1;
      Positions[2]:= 22.22;
      Positions[3]:= 0.9;
      Positions[4]:= 0.5;
      Positions[5]:= -0.2;
      EclObject:= TCelestialObjectSimple.Create(Positions);
      Positions[0]:= 200.0;
      Positions[1]:= 2.1;
      Positions[2]:= 32.22;
      Positions[3]:= 2.9;
      Positions[4]:= 2.5;
      Positions[5]:= -2.2;
      EquObject:= TCelestialObjectSimple.Create(Positions);
      Positions[0]:= 300.0;
      Positions[1]:= 3.1;
      Positions[2]:= 33.22;
      Positions[3]:= 3.9;
      Positions[4]:= 3.5;
      Positions[5]:= -3.2;
      HorObject:= TCelestialObjectSimple.Create(Positions);
      ObjectId:= 42;
      Result:= TCelestialObjectFull.Create(ObjectId, EclObject, EquObject, HorObject);
    end;


{ TestHouseSystemSpec }

  procedure TestHouseSystemSpec.SetUp;
    begin
      HouseSystemSpec:= THouseSystemSpec.Create(HouseSystemSpecName, HouseSystemSpecSeId, HouseSystemSpecDescription,
                                                HouseSystemSpecId, HouseSystemSpecNumberOfHouses,
                                                HouseSystemSpecQuadrantSystem, HouseSystemSpecCuspIsStart);
    end;

  procedure TestHouseSystemSpec.TestName;
    begin
      AssertEquals(HouseSystemSpecName, HouseSystemSpec.Name);
    end;

  procedure TestHouseSystemSpec.TestSeId;
    begin
      AssertEquals(HouseSystemSpecSeId, HouseSystemSpec.SeId);
    end;

  procedure TestHouseSystemSpec.TestDescription;
    begin
      AssertEquals(HouseSystemSpecDescription, HouseSystemSpec.Description);
    end;

  procedure TestHouseSystemSpec.TestId;
    begin
      AssertEquals(HouseSystemSpecId, HouseSystemSpec.Id);
    end;

  procedure TestHouseSystemSpec.TestNumberOfHouses;
    begin
      AssertEquals(HouseSystemSpecNumberOfHouses, HouseSystemSpec.NumberOfHouses);
    end;

  procedure TestHouseSystemSpec.TestQuadrantSystem;
    begin
      AssertEquals(HouseSystemSpecQuadrantSystem, HouseSystemSpec.QuadrantSystem);
    end;

  procedure TestHouseSystemSpec.TestCuspIsStart;
    begin
      AssertEquals(HouseSystemSpecCuspIsStart, HouseSystemSpec.CuspIsStart);
    end;


{ TestHousePositionFull }
  procedure TestHousePositionFull.SetUp;
    begin
      HousePositionFull:= THousePositionFull.Create(HousePositionFullLongitude, HousePositionFullRightAscension,
                                                    HousePositionFullDeclination, HousePositionFullAzimuth,
                                                    HousePositionFullAltitude);
    end;

  procedure TestHousePositionFull.TestLongitude;
    begin
      AssertEquals(HousePositionFullLongitude, HousePositionFull.Longitude);
    end;

  procedure TestHousePositionFull.TestRightAscension;
    begin
       AssertEquals(HousePositionFullRightAscension, HousePositionFull.RightAscension);
    end;

  procedure TestHousePositionFull.TestDeclination;
    begin
       AssertEquals(HousePositionFullDeclination, HousePositionFull.Declination);
    end;

  procedure TestHousePositionFull.TestAzimuth;
    begin
       AssertEquals(HousePositionFullAzimuth, HousePositionFull.Azimuth);
    end;

  procedure TestHousePositionFull.TestAltitude;
    begin
       AssertEquals(HousePositionFullAltitude, HousePositionFull.Altitude);
    end;


{ TestCelestialObjectSimple }

  procedure TestCelestialObjectSimple.SetUp;
    var
      Values: Array of Double;
    begin
      SetLength(Values, 6);
      Values[0]:= CelestialObjectSimpleMainPos;
      Values[1]:= CelestialObjectSimpleDeviationPos;
      Values[2]:= CelestialObjectSimpleDistancePos;
      Values[3]:= CelestialObjectSimpleMainSpeed;
      Values[4]:= CelestialObjectSimpleDeviationSpeed;
      Values[5]:= CelestialObjectSimpleDistanceSpeed;
      CelestialObjectSimple:= TCelestialObjectSimple.Create(Values);
    end;

  procedure TestCelestialObjectSimple.TestMainPos;
    begin
      AssertEquals(CelestialObjectSimpleMainPos, CelestialObjectSimple.MainPos, MARGIN);
    end;

  procedure TestCelestialObjectSimple.TestDeviationPos;
    begin
      AssertEquals(CelestialObjectSimpleDeviationPos, CelestialObjectSimple.DeviationPos, MARGIN);
    end;

  procedure TestCelestialObjectSimple.TestDistancePos;
    begin
      AssertEquals(CelestialObjectSimpleDistancePos, CelestialObjectSimple.DistancePos, MARGIN);
    end;

  procedure TestCelestialObjectSimple.TestMainSpeed;
    begin
      AssertEquals(CelestialObjectSimpleMainSpeed, CelestialObjectSimple.MainSpeed, MARGIN);
    end;

  procedure TestCelestialObjectSimple.TestDeviationSpeed;
    begin
      AssertEquals(CelestialObjectSimpleDeviationSpeed, CelestialObjectSimple.DeviationSpeed, MARGIN);
    end;

  procedure TestCelestialObjectSimple.TestDistanceSpeed;
    begin
      AssertEquals(CelestialObjectSimpleDistanceSpeed, CelestialObjectSimple.DistanceSpeed, MARGIN);
    end;

{ TestCelestialObjectFull }

  procedure TestCelestialObjectFull.SetUp;
    begin
      CelestialObjectFullEclipticalPos:= FakeCelestialObjectSimple.Create;
      CelestialObjectFullEquatorialPos:= FakeCelestialObjectSimple.Create;
      CelestialObjectFullHorizontalPos:= FakeCelestialObjectSimple.Create;
      CelestialObjectFull:= TCelestialObjectFull.Create(CelestialObjectFullObjectId, CelestialObjectFullEclipticalPos,
                                                        CelestialObjectFullEquatorialPos, CelestialObjectFullHorizontalPos);
    end;

  procedure TestCelestialObjectFull.TestObjectId;
    begin
      CheckEquals(CelestialObjectFullObjectId, CelestialObjectFull.ObjectId);
    end;

  procedure TestCelestialObjectFull.TestEclipticPositions;
    begin
      CheckTrue(CelestialObjectFull.EclipticalPos is ICelestialObjectSimple);
    end;

  procedure TestCelestialObjectFull.TestEquatorialPositions;
    begin
      CheckTrue(CelestialObjectFull.EquatorialPos is ICelestialObjectSimple);
    end;

  procedure TestCelestialObjectFull.TestHorizontalPositions;
    begin
      CheckTrue(CelestialObjectFull.HorizontalPos is ICelestialObjectSimple);
    end;



{ TestHousesResponse }
  procedure TestHousesresponse.SetUp;
    var
      Mc: IHousePositionFull;
      Asc: IHousePositionFull;
      Cusps: THousePositionFullArray;
      HouseSpec: IHouseSystemSpec;
    begin
      Mc:= FakeHousePositionFull.Create;
      Asc:= FakeHousePositionFull.Create;
      HouseSpec:= FakeHouseSystemSpec.Create;
      SetLength(Cusps, 13);
      HousesResponse:= THousesResponse.Create(Mc, Asc, Cusps, HouseSpec);
    end;

  procedure TestHousesresponse.GetMc;
    begin
      AssertTrue(HousesResponse.Mc is IHousePositionFull);
    end;

  procedure TestHousesresponse.GetAsc;
    begin
      AssertTrue(HousesResponse.Asc is HousePositionFull);
    end;

  procedure TestHousesresponse.GetCusps;
    begin
      AssertTrue(13 = Length(HousesResponse.Cusps));
    end;

  procedure TestHousesresponse.GetHouseSystemSpec;
    begin
      AssertTrue(HousesResponse.HouseSystemSpec is IHouseSystemSpec);
    end;


{ TestCalculationSettings}

  procedure TestCalculationSettings.SetUp;
    var
      i: Integer;
    begin
      SetLength(CalculationSettingsObjects, 11);
      for i := 0 to 10 do CalculationSettingsObjects[i]:= i;
      CalculationSettingsReferenceFrame:= FakeReferenceFrame.Create;
      CalculationSettingsHouseSystem:= FakeHouseSystemSpec.Create;
      Calculationsettings := TCalculationSettings.Create(CalculationSettingsPosition, CalculationSettingsAyanamsha,
                                                         CalculationSettingsObjects, CalculationSettingsReferenceFrame,
                                                         CalculationSettingsHouseSystem);
    end;

  procedure TestCalculationSettings.TestPosition;
    begin
      CheckEquals(CalculationSettingsPosition, CalculationSettings.Position);
    end;

  procedure TestCalculationSettings.TestAyanamsha;
    begin
      CheckEquals(CalculationSettingsAyanamsha, CalculationSettings.Ayanamsha);
    end;

  procedure TestCalculationSettings.TestSeId;
    begin
      CheckEquals(CalculationSettingsHouseSystem.GetSeId, CalculationSettings.HouseSystemSpec.SeId);
    end;

  procedure TestCalculationSettings.TestReferenceFrame;
    begin
      CheckEquals(CalculationSettingsReferenceFrame.Flags, CalculationSettings.ReferenceFrame.Flags);
    end;

  procedure TestCalculationSettings.TestObjects;
    var
      i: Integer;
    begin
      for i := 0 to 10 do CheckEquals(CalculationSettingsObjects[i], Calculationsettings.GetObjects[i]);
    end;

  procedure TestCalculationSettings.TestFlags;
    begin
      CheckEquals(2048, Calculationsettings.GetReferenceFrame.GetFlags);
    end;


{ TestFullChartRequest }

  procedure TestFullChartRequest.SetUp;
    var
      Date: IValidatedDate;
      Time: IValidatedTime;
    begin
      Date:= TValidatedDate.Create(2018, 9, 6, 'g', true);
      Time:= TValidatedTime.Create(2.25, true);
      FullChartRequest:= TFullChartRequest.Create('NameForChart', FakeValidatedLocation.Create, Date, Time, FakeCalculationSettings.Create);
    end;

    procedure TestFullChartRequest.GetName;
      begin
        CheckEquals('NameForChart', FullChartRequest.Name);
      end;

    procedure TestFullChartRequest.GetLocation;
      begin
        AssertTrue(FullChartRequest.Location is IValidatedLocation);
      end;

    procedure TestFullChartRequest.GetDate;
      begin
        AssertTrue(FullChartRequest.Date is IValidatedDate);
      end;

    procedure TestFullChartRequest.GetTime;
      begin
        AssertTrue(FullChartRequest.Time is IValidatedTime);
      end;

    procedure TestFullChartRequest.GetCalculationSettings;
      begin
        AssertTrue(FullChartRequest.CalculationSettings is ICalculationSettings);
      end;

{ TestFullChartResponse }

  procedure TestFullChartResponse.SetUp;
    var
      ObjectArray: TCelestialObjectFullArray;
    begin
      SetLength(ObjectArray, 1);
      ObjectArray[0]:= FakeCelestialObjectFull.Create;
      FullChartResponse:= TFullChartResponse.Create('Full Chart Response Name',
                                                    ObjectArray,
                                                    FakeHousesResponse.Create,
                                                    FakeFullChartRequest.Create);
    end;

  procedure TestFullChartResponse.GetName;
    begin
      AssertEquals('Full Chart Response Name', FullChartResponse.Name);
    end;

  procedure TestFullChartResponse.GetAllObjects;
    begin
      AssertEquals(1, Length(FullChartResponse.AllObjects));
    end;

  procedure TestFullChartResponse.GetHouses;
    begin
      AssertTrue(FullChartResponse.Houses is IHousesResponse);
    end;

  procedure TestFullChartResponse.GetFullChartRequest;
    begin
      AssertTrue(FullChartResponse.FullChartRequest is IFullChartRequest);
    end;



initialization
  RegisterTest('XChartsDomain',TestHouseSystemSpec);
  RegisterTest('XChartsDomain',TestHousePositionFull);
  RegisterTest('XChartsDomain',TestCelestialObjectSimple);
  RegisterTest('XChartsDomain',TestCelestialObjectFull);
  RegisterTest('XChartsDomain',TestHousesResponse);
  RegisterTest('XChartsDomain',TestCalculationSettings);
  RegisterTest('XChartsDomain',TestFullChartRequest);
  RegisterTest('XChartsDomain',TestFullChartResponse);

end.

