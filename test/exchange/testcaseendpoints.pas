unit TestCaseEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, SharedXchgDomain, SharedEndpoints;

type

  TestFullChartEndpoint = class(TTestCase)
  published
    procedure TestHandleRequest;
  end;


implementation

const
  MARGIN = 0.000000001;






{ TestFullChartEndpoint }
procedure TestFullChartEndpoint.TestHandleRequest;
{ TODO : Create test for FullChartEndpoint after Fakes are in place. }
begin
  CheckTrue(True);
end;

initialization



  RegisterTest('Endpoints',TestFullChartEndpoint);

end.

