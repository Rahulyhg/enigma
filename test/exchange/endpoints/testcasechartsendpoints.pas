{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseChartsEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, ChartsEndpoints, ChartsHandlers, SharedXchgDomain;

type

// Not implemented yet, as this would require either fakes or mocks. Maybe replace with an integration test?
TestFullChartEndpoint = class(TTestCase)
published
  procedure TestHandleRequest;
end;

implementation



{ TestFullChartEndpoint }
procedure TestFullChartEndpoint.TestHandleRequest;

begin


end;

initialization



end.

