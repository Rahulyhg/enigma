{ Enigma is open source.
  Please check the file copyright.txt in the root of this application for further details. }
unit TestCaseDataEndpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, DataEndpoints, Dao, Dto;

type
  TestHouseSystemDataEndpoint = class(TTestCase)
    strict private
      Endpoint: IHouseSystemDataEndpoint;
    published
      procedure SetUp; override;
      procedure GetSystem;
      procedure GetAllSystems;
  end;

  TestAyanamshaDataEndpoint = class(TTestCase)
    strict private
       Endpoint: IAyanamshaDataEndpoint;
    published
      procedure SetUp; override;
      procedure GetAyanamsha;
      procedure GetAllAyanamshas;
  end;

  TestCoordinateSystemDataEndpoint = class(TTestCase)
    strict private
       Endpoint: ICoordinateSystemDataEndpoint;
    published
      procedure SetUp; override;
      procedure GetName;
      procedure GetAllItems;
    end;

implementation

  uses
    FakesDao, DataEndpointsImpl;

  { TestHouseSystemDataEndpoint }

  procedure TestHouseSystemDataEndpoint.SetUp;
    begin
      Endpoint:= THouseSystemDataEndpoint.Create(FakeHouseSystemsDao.Create);
    end;

  procedure TestHouseSystemDataEndpoint.GetSystem;
    var
      Dto: IHouseSystemDto;
    begin
      Dto:= Endpoint.GetSystem(1);
      AssertNotNull(Dto);
    end;

  procedure TestHouseSystemDataEndpoint.GetAllSystems;
    var
      Dto: IHouseSystemDto;
    begin
      Dto:= Endpoint.AllSystems[0];
      AssertNotNull(Dto);
    end;

  { TestAyanamshaDataEndpoint }
  procedure TestAyanamshaDataEndpoint.SetUp;
    begin
      Endpoint:= TAyanamshaDataEndpoint.Create(FakeAyanamshasDao.Create);
    end;

  procedure TestAyanamshaDataEndpoint.GetAyanamsha;
    var
      Dto: IAyanamshaDto;
    begin
      Dto:= Endpoint.GetAyanamsha(1);
      AssertNotNull(Dto);
    end;

  procedure TestAyanamshaDataEndpoint.GetAllAyanamshas;
    var
      Dto: IAyanamshaDto;
    begin
      Dto:= Endpoint.AllAyanamshas[0];
      AssertNotNull(Dto);
    end;

  { TestCoordinateSystemDataEndpoint }

  procedure TestCoordinateSystemDataEndpoint.SetUp;
    begin
      Endpoint:= TCoordinateSystemDataEndpoint.Create(FakeLookupValueDao.Create);
    end;

  procedure TestCoordinateSystemDataEndpoint.GetName;
    var
      Dto: ILookupValueDto;
    begin
      Dto:= Endpoint.GetName(1);
      AssertNotNull(Dto);
    end;

  procedure TestCoordinateSystemDataEndpoint.GetAllItems;
     var
      Dto: ILookupValueDto;
    begin
      Dto:= Endpoint.AllItems[0];
      AssertNotNull(Dto);
    end;



initialization
  RegisterTest('DatabaseEndpoints',TestHouseSystemDataEndpoint);
  RegisterTest('DatabaseEndpoints',TestAyanamshaDataEndpoint);
  RegisterTest('DatabaseEndpoints',TestCoordinateSystemDataEndpoint);

end.

